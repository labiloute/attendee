-- phpMyAdmin SQL Dump
-- version 5.0.4deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 22 juin 2023 à 13:52
-- Version du serveur :  10.5.15-MariaDB-0+deb11u1
-- Version de PHP : 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `attendee`
--

CREATE DATABASE IF NOT EXISTS `attendee` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `attendee`;

-- --------------------------------------------------------

--
-- Structure de la table `attendees`
--

CREATE TABLE `attendees` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `hide` tinyint(4) DEFAULT NULL,
  `confirmkey` varchar(50) NOT NULL,
  `state` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `letters_addresses`
--

CREATE TABLE `letters_addresses` (
  `address_id` int(11) NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `disabled` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `letters_groups`
--

CREATE TABLE `letters_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `letters_groups_to_users`
--

CREATE TABLE `letters_groups_to_users` (
  `link_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Store links between addresses and groups';

-- --------------------------------------------------------

--
-- Structure de la table `letters_letters`
--

CREATE TABLE `letters_letters` (
  `letter_id` int(11) NOT NULL,
  `letter_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `letter_width` int(11) DEFAULT NULL,
  `letter_bgcolor` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter_pagecolor` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letter_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `letter_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `letter_enabled` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `letters_shipping`
--

CREATE TABLE `letters_shipping` (
  `shipping_id` int(11) NOT NULL,
  `letter_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `letters_to_groups`
--

CREATE TABLE `letters_to_groups` (
  `link_id` int(11) NOT NULL,
  `letter_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `disabled` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `places`
--

INSERT INTO `places` (`id`, `title`, `description`, `disabled`) VALUES
(1, 'Your place', '<p>Default place.</p><p>HTML content allowed !</p>', 0);

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `max_attendee` int(11) DEFAULT NULL,
  `price` int(5) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `place` int(11) NOT NULL,
  `quickbook` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Available for quick booking',
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `notice` tinyint(4) NOT NULL DEFAULT 0,
  `canceled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `skills`
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `cat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcat` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `starred` tinyint(4) NOT NULL DEFAULT 0,
  `disabled` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Déchargement des données de la table `skills`
--

INSERT INTO `skills` (`id`, `cat`, `subcat`, `title`, `starred`, `disabled`) VALUES
(1, 'Utiliser', NULL, 'Créer et paramétrer un compte Google', 0, 0),
(2, 'Utiliser', NULL, 'Gérer ses favoris de navigation', 0, 0),
(3, 'Utiliser', NULL, 'Poste informatique Windows', 0, 0),
(4, 'Utiliser', NULL, 'Poste informatique MacOS', 0, 0),
(5, 'Utiliser', NULL, 'Poste informatique Linux', 0, 0),
(6, 'Utiliser', NULL, 'Internet : paramétrer un réseau Wifi', 0, 0),
(7, 'Utiliser', NULL, 'Internet : paramétrer et gérer ses emails', 0, 0),
(8, 'Utiliser', NULL, 'Transférer des fichiers volumineux', 0, 0),
(9, 'Utiliser', NULL, 'Organiser, explorer et partager des contenus numériques', 0, 0),
(10, 'Utiliser', NULL, 'Gérer ses données: sauvegardes en ligne (cloud)', 0, 0),
(11, 'Utiliser', NULL, 'Gérer ses données : sauvegardes locales (disques dur externes, clés USB, etc)', 0, 0),
(12, 'Utiliser', NULL, 'Photo numérique: usage courant (prise de vue, réglages)', 0, 0),
(13, 'Utiliser', NULL, 'Classer, gérer et partager ses photos', 0, 0),
(14, 'Utiliser', NULL, 'Images: gérer ses photos en ligne', 0, 0),
(15, 'Utiliser', NULL, 'Facebook: découverte', 0, 0),
(16, 'Utiliser', NULL, 'Facebook: approfondissement', 0, 0),
(17, 'Utiliser', NULL, 'Twitter: découverte', 0, 0),
(18, 'Utiliser', NULL, 'Twitter: approfondissement', 0, 0),
(19, 'Utiliser', NULL, 'Instagram: découverte', 0, 0),
(20, 'Utiliser', NULL, 'Instagram: approfondissement', 0, 0),
(21, 'Utiliser', NULL, 'Pinterest: découverte', 0, 0),
(22, 'Utiliser', NULL, 'Pinterest : approfondissement', 0, 0),
(23, 'Utiliser', NULL, 'Snapchat', 0, 0),
(24, 'Utiliser', NULL, 'Skype', 0, 0),
(25, 'Utiliser', NULL, 'Traitement de texte: découverte', 0, 0),
(26, 'Utiliser', NULL, 'Traitement de texte: utilisation de base', 0, 0),
(27, 'Utiliser', NULL, 'Traitement de texte : utilisation avancée', 0, 0),
(28, 'Utiliser', NULL, 'Tableur: découverte', 0, 0),
(29, 'Utiliser', NULL, 'Tableur : utilisation de base', 0, 0),
(30, 'Utiliser', NULL, 'Tableur : Utilisation avancée', 0, 0),
(31, 'Utiliser', NULL, 'Signer numériquement des documents administratifs', 0, 0),
(32, 'Utiliser', NULL, 'Marchés publics : signer numériquement ses réponses', 0, 0),
(33, 'Utiliser', 'Smartphone', 'Android', 0, 0),
(34, 'Utiliser', 'Smartphone', 'Environnement IOS', 0, 0),
(35, 'Utiliser', 'Smartphone', 'Les application clés', 0, 0),
(36, 'Utiliser', 'Smartphone', 'Les principaux gestes pour l\'écran tactile', 0, 0),
(37, 'Utiliser', 'Smartphone', 'Les fonctionnalités de base', 0, 0),
(38, 'Utiliser', 'Smartphone', 'Télécharger une application sur le \"Store\" (magasin d\'application)', 0, 0),
(39, 'Utiliser', 'Smartphone', 'Réglages et configuration de l\'appareil', 0, 0),
(40, 'Utiliser', NULL, 'Outils numériques pour maîtriser sa consommation énergétique', 0, 0),
(41, 'Utiliser', NULL, 'Suivre la scolarité de mon enfant', 0, 0),
(42, 'Utiliser', NULL, 'Découvrir des alternatives libres et gratuits aux logiciels', 0, 0),
(43, 'Faire ses démarches en ligne', '', 'Pôle emploi: faire ses démarches en ligne', 0, 0),
(44, 'Faire ses démarches en ligne', NULL, 'Déclarer ses revenus en ligne', 0, 0),
(45, 'Faire ses démarches en ligne', NULL, 'Accéder à ses droits sociaux et les gérer en ligne (RSA, ...)', 0, 0),
(46, 'Faire ses démarches en ligne', NULL, 'Ouvrir et gérer son dossier de retraite en ligne (CNAF/CARSAT/...)', 0, 0),
(47, 'Faire ses démarches en ligne', NULL, 'Gérer son abonnement et ses factures d’électricité/gaz en ligne', 0, 0),
(48, 'Faire ses démarches en ligne', NULL, 'Gérer ses droits d\'assuré social en ligne', 0, 0),
(49, 'Faire ses démarches en ligne', NULL, 'Gérer ses droits et allocation en ligne (CAF,...)', 0, 0),
(50, 'Faire ses démarches en ligne', NULL, 'Plateforme la Poste : envoyer et recevoir des courriers', 0, 0),
(51, 'Faire ses démarches en ligne', NULL, 'Plateforme la Poste : utiliser le coffre-fort en ligne', 0, 0),
(52, 'Faire ses démarches en ligne', NULL, 'Plate-forme Ameli.fr : la sécurité sociale en ligne', 0, 0),
(53, 'Faire ses démarches en ligne', NULL, 'Découvrir les services en ligne de l\'enfance de votre commune', 0, 0),
(54, 'Faire ses démarches en ligne', NULL, 'Plateforme France Connect', 0, 0),
(55, 'Identité numérique', NULL, 'Connaître et gérer son identité numérique', 0, 0),
(56, 'Identité numérique', NULL, 'Nettoyer son identité numérique', 0, 0),
(57, 'Identité numérique', NULL, 'Effacer ses traces sur le web, protéger ses données personnelles', 0, 0),
(58, 'Contribuer', NULL, 'Utiliser et contribuer à Wikipedia', 0, 0),
(59, 'Contribuer', NULL, 'Découvrir et contribuer à OpenStreetMap', 0, 0),
(60, 'Contribuer', NULL, 'Utiliser des outils de cartographie', 0, 0),
(61, 'Contribuer', NULL, 'Utiliser et partager sa veille', 0, 0),
(62, 'Contrôler ses données', NULL, 'Les solutions de bureautique libres', 0, 0),
(63, 'Contrôler ses données', NULL, 'Les outils libres pour la navigation Internet', 0, 0),
(64, 'Contrôler ses données', NULL, 'Les outils libres pour gérer ses emails', 0, 0),
(65, 'Contrôler ses données', NULL, 'Les alternatives à Google Drive', 0, 0),
(66, 'Contrôler ses données', NULL, 'Les alternatives à Facebook', 0, 0),
(67, 'Contrôler ses données', NULL, 'Les outils disponibles pour sécurises ses usages numériques', 0, 0),
(68, 'Explorer de nouvelles possibilités', NULL, 'Images: retouches ses photos', 0, 0),
(69, 'Explorer de nouvelles possibilités', NULL, 'Découverte et utilisation des imprimantes 3D', 0, 0),
(70, 'Explorer de nouvelles possibilités', NULL, 'Vidéo: découvrir le montage vidéo', 0, 0),
(71, 'Explorer de nouvelles possibilités', NULL, 'PAO: faire des présentations (diaporamas)', 0, 0),
(72, 'Explorer de nouvelles possibilités', NULL, 'PAO : publier des présentations en ligne', 0, 0),
(73, 'Explorer de nouvelles possibilités', NULL, 'Découverte et utilisation d\'une découpeuse numérique', 0, 0),
(74, 'Explorer de nouvelles possibilités', NULL, 'Savoir \"pitcher\" son projet en 5 minutes', 0, 0),
(75, 'Explorer de nouvelles possibilités', NULL, 'Créer un site web avec WordPress', 0, 0),
(76, 'Explorer de nouvelles possibilités', NULL, 'Créer, paramétrer et utiliser une liste de diffusion', 0, 0),
(77, 'Explorer de nouvelles possibilités', NULL, 'Modélisation 3D', 0, 0),
(78, 'Explorer de nouvelles possibilités', NULL, 'Internet: déposer un annonce sur LeBonCoin (ou autre)', 0, 0),
(79, 'Explorer de nouvelles possibilités', NULL, 'Services et plateformes de démocratie participative', 0, 0),
(80, 'Explorer de nouvelles possibilités', NULL, 'Découvrir et expérimenter la programmation informatique (code)', 0, 0),
(81, 'Explorer de nouvelles possibilités', NULL, 'Utiliser des cartes, capteurs et outils interactifs', 0, 0),
(82, 'Explorer de nouvelles possibilités', NULL, 'Découvrir et participer à des MOOCs', 0, 0),
(83, 'S\'insérer professionnellement', NULL, 'Réalisation de CV', 0, 0),
(84, 'S\'insérer professionnellement', NULL, 'Diffuser son CV en ligne', 0, 0),
(85, 'S\'insérer professionnellement', NULL, 'Utilisation d\'un espace de co-working', 0, 0),
(86, 'S\'insérer professionnellement', NULL, 'Organiser sa recherche d\'emploi', 0, 0),
(87, 'S\'insérer professionnellement', NULL, 'Découverte et usage de l\'\"emploi store\"', 0, 0),
(88, 'S\'insérer professionnellement', NULL, 'Découvrir les métiers du numérique', 0, 0),
(89, 'S\'informer', NULL, 'Composantes et facettes de l\'identité numérique', 0, 0),
(90, 'S\'informer', NULL, 'Les conduites à risque et les bons usages du numérique', 0, 0),
(91, 'S\'informer', NULL, 'Être parent à l\'ère numérique : connaître les usages, jouer son rôle de parent', 0, 0),
(92, 'S\'informer', NULL, 'Panorama des usages numériques des adolescents', 0, 0),
(93, 'S\'informer', NULL, 'Découvrir les réseaux sociaux : définition, fonctionnement', 0, 0),
(94, 'S\'informer', NULL, 'Utiliser les réseaux sociaux pour sa recherche d\'emploi', 0, 0),
(95, 'S\'informer', NULL, 'Panorama d\'usages créatifs du numérique', 0, 0),
(96, 'S\'informer', NULL, 'Panorama de la création artistique numérique', 0, 0),
(97, 'S\'informer', NULL, 'Fablab : charte, valeurs, et panorama des outils numériques', 0, 0),
(98, 'S\'informer', NULL, 'Panorama des objets connectés (Domotique, Sécurité, Santé)', 0, 0),
(99, 'S\'informer', NULL, 'Internet: fonctionnement et outils de navigation web', 0, 0),
(100, 'S\'informer', NULL, 'Internet: fonctionnement des emails', 0, 0),
(101, 'S\'informer', NULL, 'Internet: fonctionnement des clients webmail', 0, 0),
(102, 'S\'informer', NULL, 'Découverte des outils de messagerie instantanée (tchat)', 0, 0),
(103, 'S\'informer', NULL, 'Panorama des outils de webconférence', 0, 0),
(104, 'S\'informer', NULL, 'Panorama des outils du cloud', 0, 0),
(105, 'S\'informer', NULL, 'Panorama des outils de travail collaboratif', 0, 0),
(106, 'S\'informer', NULL, 'Marchés publics : panorama des plateformes', 0, 0),
(107, 'S\'informer', NULL, 'Panorama des plateformes d\'économie collaborative', 0, 0),
(108, 'S\'informer', NULL, 'Panorama des plateformes de recherche d\'emploi', 0, 0),
(109, 'S\'informer', NULL, 'Fonctionnement des plateformes de musique et de films en ligne', 0, 0),
(110, 'S\'informer', NULL, 'Découvrir l\'univers des jeux vidéo', 0, 0),
(111, 'S\'informer', NULL, 'Les mécanismes addictifs ou excessifs liés au numérique', 0, 0),
(112, 'Comprendre', NULL, 'Principes et fonctionnement du cloud', 0, 0),
(113, 'Comprendre', NULL, 'Google Drive et solutions alternatives', 0, 0),
(114, 'Comprendre', NULL, 'Les outils de protection de l\'enfance', 0, 0),
(115, 'Comprendre', NULL, 'Téléphonie: comprendre et comparer les offres mobiles', 0, 0),
(116, 'Comprendre', NULL, 'Le paiement en ligne', 0, 0),
(117, 'Comprendre', NULL, 'les biens communs: principes et enjeux', 12, 0),
(118, 'Comprendre', NULL, 'Les licences libres', 11, 0),
(119, 'Comprendre', NULL, 'Les monnaies virtuelles', 10, 0),
(120, 'Comprendre', NULL, 'Fonctionnement d\'une box internet', 9, 0),
(121, 'Comprendre', NULL, 'Internet: comprendre une offre internet', 8, 0),
(122, 'Comprendre', NULL, 'Internet: comprendre les principes de fonctionnement', 7, 0),
(123, 'Comprendre', NULL, 'Internet: comprendre un réseau Wifi', 6, 0),
(124, 'Comprendre', NULL, 'Big Data / Open Data : comprendre les données', 5, 0),
(125, 'Comprendre', NULL, 'Le smartphone : principes de fonctionnement', 4, 0),
(126, 'Comprendre', NULL, 'Techniques de vérification de l\'information', 3, 0),
(127, 'Comprendre', NULL, 'Comprendre les cultures numériques : les jeux vidéos', 2, 0),
(128, 'Comprendre', NULL, 'Neutralité du net : de quoi parle-t-on ?', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) UNSIGNED NOT NULL,
  `label` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `price` float DEFAULT NULL,
  `image` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin DEFAULT NULL,
  `category` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `ref` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `weight` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `templates`
--

CREATE TABLE `templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `max_attendee` int(11) DEFAULT NULL,
  `price` int(5) DEFAULT 0,
  `username` varchar(100) DEFAULT NULL,
  `place` int(11) NOT NULL,
  `quickbook` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Available for quick booking',
  `notice` tinyint(4) NOT NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `todolist`
--

CREATE TABLE `todolist` (
  `id` int(8) UNSIGNED NOT NULL,
  `position` int(8) UNSIGNED NOT NULL DEFAULT 0,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dt_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `todolistup`
--

CREATE TABLE `todolistup` (
  `id` int(11) NOT NULL,
  `todo_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `user_first_name` varchar(50) NOT NULL,
  `user_last_name` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `profile` varchar(50) NOT NULL,
  `user_comment` text DEFAULT NULL,
  `disabled` int(11) DEFAULT NULL,
  `ldap` tinyint(4) DEFAULT NULL,
  `mail` varchar(100) NOT NULL,
  `skillsid` varchar(500) DEFAULT NULL,
  `trainingrqstid` varchar(500) DEFAULT NULL,
  `external_id` int(11) DEFAULT NULL,
  `confirmkey` varchar(50) DEFAULT NULL,
  `last_pwd_update` datetime DEFAULT NULL,
  `contact_agreement` tinyint(4) NOT NULL DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lon` varchar(20) DEFAULT NULL,
  `city` varchar(120) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `zip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `user_first_name`, `user_last_name`, `password`, `profile`, `user_comment`, `disabled`, `ldap`, `mail`, `skillsid`, `trainingrqstid`, `external_id`, `confirmkey`, `last_pwd_update`, `contact_agreement`, `last_login`, `lat`, `lon`, `city`, `address`, `zip`) VALUES
(1, 'admin', 'Administrator', 'YourAdmin', '$2y$10$ppyR/BdUkcBfSh4wKpjsgOQTdFuDk4U525AdoFZy6/Gl7EmDDgvsm', 'ADMIN', 'test', NULL, NULL, 'youruser@yourdomain.tld', ';127;', '', 0, NULL, '2022-01-14 15:43:37', 1, '2023-02-21 11:16:26', '49.17914', '-0.35193', '', '', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `attendees`
--
ALTER TABLE `attendees`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `letters_addresses`
--
ALTER TABLE `letters_addresses`
  ADD PRIMARY KEY (`address_id`),
  ADD UNIQUE KEY `address` (`address`);

--
-- Index pour la table `letters_groups`
--
ALTER TABLE `letters_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Index pour la table `letters_groups_to_users`
--
ALTER TABLE `letters_groups_to_users`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `address_id` (`address_id`);

--
-- Index pour la table `letters_letters`
--
ALTER TABLE `letters_letters`
  ADD PRIMARY KEY (`letter_id`);

--
-- Index pour la table `letters_shipping`
--
ALTER TABLE `letters_shipping`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Index pour la table `letters_to_groups`
--
ALTER TABLE `letters_to_groups`
  ADD PRIMARY KEY (`link_id`);

--
-- Index pour la table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `skills`
--
ALTER TABLE `skills`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `todolist`
--
ALTER TABLE `todolist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `position` (`position`);

--
-- Index pour la table `todolistup`
--
ALTER TABLE `todolistup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `todo_id` (`todo_id`,`username`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `attendees`
--
ALTER TABLE `attendees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_addresses`
--
ALTER TABLE `letters_addresses`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_groups`
--
ALTER TABLE `letters_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_groups_to_users`
--
ALTER TABLE `letters_groups_to_users`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_letters`
--
ALTER TABLE `letters_letters`
  MODIFY `letter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_shipping`
--
ALTER TABLE `letters_shipping`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `letters_to_groups`
--
ALTER TABLE `letters_to_groups`
  MODIFY `link_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT pour la table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `todolist`
--
ALTER TABLE `todolist`
  MODIFY `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `todolistup`
--
ALTER TABLE `todolistup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
