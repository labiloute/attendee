<?php
/*
include_once('egulias/EmailValidator/Validation/EmailValidation.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/RFCValidation.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/DNSCheckValidation.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/InvalidEmail.php');

include_once('../swiftmailer/egulias/EmailValidator/Parser/Parser.php');
include_once('../swiftmailer/egulias/EmailValidator/Warning/Warning.php');
include_once('../swiftmailer/egulias/EmailValidator/Warning/LocalTooLong.php');
include_once('../swiftmailer/egulias/EmailValidator/Parser/LocalPart.php');
include_once('../swiftmailer/egulias/EmailValidator/Parser/DomainPart.php');

include_once('../swiftmailer/egulias/EmailValidator/Validation/MultipleValidationWithAnd.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/NoRFCWarningsValidation.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/SpoofCheckValidation.php');

include_once('../swiftmailer/egulias/EmailValidator/EmailValidator.php');
include_once('../swiftmailer/egulias/EmailValidator/EmailParser.php');
include_once('../swiftmailer/egulias/EmailValidator/Validation/MultipleErrors.php');

include_once('../swiftmailer/egulias/EmailValidator/EmailLexer.php');
*/

//include_once('mysql.php');
class Mail {

	public $CONFIG;
	public $msg;
	public $transport;
	public $mailer;

	function __construct($CONFIG) {
		$this->CONFIG = $CONFIG;

		if ($this->CONFIG['enable_mail_confirmation']) {
			if (file_exists($this->CONFIG['swiftmailer_path'])) {
				//require_once('/var/www/html/vendor/autoload.php');
				require_once($this->CONFIG['swiftmailer_path']);
				return True;
				}
			else {
				$this->msg = 'Unable to load SwiftMailer on this path : '.$this->CONFIG['swiftmailer_path'];
				return False;
				}
			}


		else {
			$this->msg = 'SwiftMailer disabled in configuration. No notification will be sent';
			return False;
			}
		}

// Send method
function Send($dest,$subject,$message)
	{

	if ($this->CONFIG['enable_mail_confirmation']) {
		//echo 'Creation de la Classe';

		//echo "Encryption : ".$this->CONFIG['swiftmailer_encryption'].PHP_EOL;
		//echo "Host : ".$this->CONFIG['swiftmailer_host'].PHP_EOL;
		//echo "Port : ".$this->CONFIG['swiftmailer_port'].PHP_EOL;

		//print_r(stream_get_transports());

		try {
			if (! $this->CONFIG['swiftmailer_encryption'] && $this->CONFIG['swiftmailer_username'])
				$this->transport = (new Swift_SmtpTransport($this->CONFIG['swiftmailer_host'], $this->CONFIG['swiftmailer_port']))
					->setUsername($this->CONFIG['swiftmailer_username'])
					->setPassword($this->CONFIG['swiftmailer_password'])
				;
			else if ($this->CONFIG['swiftmailer_encryption'] && $this->CONFIG['swiftmailer_username'])
				$this->transport = (new Swift_SmtpTransport($this->CONFIG['swiftmailer_host'], $this->CONFIG['swiftmailer_port'],$this->CONFIG['swiftmailer_encryption']))
					->setUsername($this->CONFIG['swiftmailer_username'])
					->setPassword($this->CONFIG['swiftmailer_password'])
				;
			else {
				$this->transport = (new Swift_SmtpTransport($this->CONFIG['swiftmailer_host'], $this->CONFIG['swiftmailer_port']));}
			}
		catch (\Exception $e) {
    			 $this->msg='Problème de connexion au serveur de smtp : '.$e->getMessage();
    			 echo $this->msg;
    			 return False;
		}
		//echo "OK";
		//var_dump($this->transport);
		$this->mailer = new Swift_Mailer($this->transport);
		//var_dump($this->transport);
		}
	else {
		$this->msg = 'Please enable SwiftMailer to send notifications';
		return False;
		}

	// If dest are given as an array
	if (is_array($dest)) $setto = $dest; else $setto = [$dest];

	// Construct message
	$message = (new Swift_Message($subject))
  			->setFrom([$this->CONFIG['swiftmailer_sender_email'] => $this->CONFIG['swiftmailer_sender_name']])
  			->setTo($setto)
			->setEncoder( new Swift_Mime_ContentEncoder_PlainContentEncoder('8bit'))
 			->setBody($message)
			;
	// Allow HTML
	$type = $message->getHeaders()->get('Content-Type');
	$type->setValue('text/html');
	$type->setParameter('charset', 'utf-8');

	// Send
	try {
		$result = $this->mailer->send($message);
	}
	// Catch result
	catch (Exception $e) {
    		$this->msg=_('Mail sending error').' : '.$e->getMessage();
    		return False;
		}
	//echo "Resultat : ";
	//var_dump($result);
	if ($result==1) {
		$this->msg=_('Sending email OK');
		return True;
		}
	/*
	else {
		if (! $txt_error) $txt_error.=_('Error send email : ')' :'.$e->getMessage();
		$this->msg = $txt_error;
		return False;
		}
	*/
	}
}
?>
