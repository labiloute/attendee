<?php
// Version 10 : ajout d'une fonction pour chercher un groupe par son ID en cherchant par la fin de son SID (lent). Utilisé pour retrouver le PRIMARY GROUP d'un utilisateur qui n'était jusqu'à lors pas disponible
// Version 9 : modification de la fonction getAllGroups pour filtrer mais ajouter des exceptions au filtre
// Version 8 : ajout de la fonction getAllGroups qui parcours l'ad pour tous les noms de groupes (avec possibilité de filtrer)
// Version 7 : ajout de la fonction CountPeople pour compter le nombre de personnes ayant un prenom ou un nom particulier
// Version 6 : amélioration de la fonction getDN et ajout de la fonction getGroups
// version 5 : ajout de la fonction getAttribute et getAttributes pour rechercher un attribut specifique à un utilisateur ou plusieurs attributs
// Version 4 : correction dans la fonction ckusergroup_ad et ajout de getUsername
// Version 3 : ajout de la fonction ldap_bind
// Version 2 : ajout de la function qui renvoie dans un tableau la liste des utilisateurs d'un groupe
//  Et sous forme de classe avec les parametres de l'AD fournis dans $CONFIG

class Ldap {

	function __construct($CONFIG) {
		$this->CONFIG = $CONFIG;
		//echo $_SERVER['REMOTE_USER'];
	}

	function connect() {
//		$this->ad = ldap_connect("ldap://{$this->CONFIG['ad_host']}.{$this->CONFIG['ad_domain']}") or die('<br />Impossible de joindre le serveur ldap.<br />');
//		$this->ad = ldap_connect("ldap://{$this->CONFIG['ldap_host']}") or die('<br />Impossible de joindre le serveur ldap.<br />');
		if ($this->CONFIG['ldap_host']!='' || $this->CONFIG['ldap_host']!=False) {
			$this->ad=@ldap_connect($this->CONFIG['ldap_host']) or die("Unable to connect anonymously to the Ldap Server ".$this->CONFIG['ldap_host']." anonymously. Please double check config.");
		}
		//@ldap_bind($this->ad, $this->CONFIG['ad_bind_user']."@".$this->CONFIG['ad_domain'], $this->CONFIG['ad_bind_passwd']) or die('<br />You requested login with a Ldap account but we are unable to join your Ldap server :  '.$this->CONFIG['ad_host'].'. Please check configuration.<br />');
		ldap_set_option($this->ad, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($this->ad, LDAP_OPT_REFERRALS, 0);
		ldap_set_option($this->ad, LDAP_OPT_DEBUG_LEVEL, 7);//DEBUG
		return true;
	}

	function bind($username,$password) {
		// Concat AD domain if not given
		if (!strpos($username, '@') && $this->CONFIG['ldap_domain']!='' && $this->CONFIG['ldap_domain'])
			$username = $username.'@'.$this->CONFIG['ldap_domain'];
		$ldapbind=ldap_bind($this->ad,$username, $password);
		if ($ldapbind) return True;
		else {  //echo ldap_error($this->ad); 
			return False;}
		}


	function ckusergroup_ad($user,$group) {
		//check if user contain an @ char, then split and keep only the username :
		if (strpos($user, '@')!=false)
			{
			$user_splited=explode('@',$user);
			$user=$user_splited[0];
			}
		//echo $user;
		//ad bind
		$userdn = $this->getDN($user);
		if ($this->checkGroupEx($userdn, $this->getDN($group))) {
			//if ($this->checkGroup($userdn, $this->getDN($group)))
		    		//echo "You're authorized as ".$this->getCN($userdn);
			return true;
		} else {
		    //echo 'Authorization failed';
			return false;
		}

	}
	/*
	* This function searchs in LDAP tree ($ad -LDAP link identifier)
	* entry specified by samaccountname and returns its DN or epmty
	* string on failure.
	*/
        function getDN($username,$field='uid',$attributes = array('dn')) {
                if ($this->CONFIG['ldap_user_identifier']) $field=$this->CONFIG['ldap_user_identifier'];
                $attribute=$attributes[0];
                //check if user contain an @ char, then split and keep only the username :
                    if (strpos($username, '@')!=false)
                        {
                        $user_splited=explode('@',$username);
                        $username=$user_splited[0];
                        }

                $result = ldap_search($this->ad, $this->CONFIG['ldap_base_dn'],"({$field}={$username})", $attributes);
                //echo "Coucou2 : ".$field.$username;

                if ($result === FALSE) { echo "Erreur LDAP : ".ldap_error($this->ad); return False; }
                $entries = ldap_get_entries($this->ad, $result);
                //var_dump($entries);
                if ($entries['count']>0) { return $entries[0][$attribute]; }
                else { return False; };
                }


        /*
        * This function retrieves and returns Organisational Units from a given base DN
        */
        function getOU($basedn=False,$FirstLevelOnly=True) {
                // Define returned array
                $oulist=array();
                // Define base search DN
                if ($basedn) $searchdn=$basedn;
                else $searchdn=$this->CONFIG['ldap_base_dn'];
                // Define search filter (only OU)
                $filter="(structuralObjectClass=organizationalUnit)";
                // Get attributes (ou is the name, DN serve to know if it is a first level, to exclude second level eventually)
                $attributes=array('ou','dn');
                //var_dump($this->CONFIG['ldap_base_dn']);
                //var_dump($attributes);
                $result = ldap_search($this->ad, $searchdn, $filter, $attributes);
                //var_dump($result);
                if ($result === FALSE) { echo "Erreur LDAP : ".ldap_error($this->ad); return False; }
                $entries = ldap_get_entries($this->ad, $result);

                // Loop truh results and store them into array
                foreach ($entries as $ou) {
                        //var_dump($ou);
                        //echo '<br />';
                        // Define a combination OU and baseDN, which will be our reference for first level
                        $firstlevel='ou='.$ou['ou'][0].','.$this->CONFIG['ldap_base_dn'];
                        //echo $firstlevel.'<br />';
                        //var_dump($ou['dn']);
                        // If DN is equal to first level, then it is an OU of firstlevel, so we store it
                        if ($ou['dn']==$firstlevel && $FirstLevelOnly && $ou['ou'][0]) {
                                $oulist[]=$ou['ou'][0];
                                //echo '<br />';
                                }
                        // Else if all OU are requested, we return any OU since it is not empty
                        else if ($FirstLevelOnly == False && $ou['ou'][0]) {
                                $oulist[]=$ou['ou'][0];
                                }
                        }
                return $oulist;
                }


        /*
        * This function retrieves and returns DATA and USERS limit for an OU (specific to Marmule)
        */
        function getOUDescription($ou) {
                // Define search filter
                $filter="(ObjectClass=organizationalUnit)";
                // Get attributes (ou is the name, DN serve to know if it is a first level, to exclude second level eventually)
                $attributes=array('description');
                //var_dump($this->CONFIG['ldap_base_dn']);
                $searchdn='ou='.$ou.','.$this->CONFIG['ldap_base_dn'];
                //var_dump($attributes);
                $result = ldap_search($this->ad, $searchdn, $filter, $attributes);
                //var_dump($result);
                if ($result === FALSE) { echo "Erreur LDAP : ".ldap_error($this->ad); return False; }
                $entries = ldap_get_entries($this->ad, $result);
                $text=$entries[0]['description'][0];
                $stext=explode("\n",$text);
                //var_dump($stext);
                // Cut LDAP Description Field in a PAIR of KEY/VALUE
                foreach ($stext as $pair) {
                        $spair=explode(":",$pair);
                        // Eventually TRIM wrong ending spaces
                        $spair[1]=trim($spair[1]);
                        //var_dump($this->CONFIG['ldap_allowed_description_metadata']);
                        if (in_array($spair[0],$this->CONFIG['ldap_allowed_description_metadata'])) {
                                $values[$spair[0]]=$spair[1];
                                //echo $spair[0].' has value '.$spair[1].'.<br />';
                                // IF IT STARTS WITH 'KEY-', Store it in a specific SUBARRAY
                                if (substr($spair[0],0,4)=='KEY-') {
                                        // Remove pattern from KEY NAME
                                        $keyindex=str_replace('KEY-','',$spair[0]);
                                        //echo $keyindex.' has value '.$spair[1].'.<br />';
                                        $values['KEY'][$keyindex]=$spair[1];
                                        }
                                }
                        }
                return $values;
                }


        /*
        * This function retrieves and returns users from a given base DN
        */
        function getUsers($basedn=False,$disabled=False) {
                // Define returned array
                $oulist=array();
                // Define base search DN
                if ($basedn) $searchdn=$basedn;
                else $searchdn=$this->CONFIG['ldap_base_dn'];
                // Eventually add filter for disabled users
                if ( $disabled==False && $this->CONFIG['ldap_disabled_field'] && $this->CONFIG['ldap_disabled_pattern'])
                        $disabled_filter='(!('.$this->CONFIG['ldap_disabled_field'].'~='.$this->CONFIG['ldap_disabled_pattern'].'))';
                elseif ($disabled=='ONLY' && $this->CONFIG['ldap_disabled_field'] && $this->CONFIG['ldap_disabled_pattern'])
                        $disabled_filter='('.$this->CONFIG['ldap_disabled_field'].'~='.$this->CONFIG['ldap_disabled_pattern'].')';
                else $disabled_filter='';
                // Define search filter (only OU)
                $filter="(&(ObjectClass=inetOrgPerson)".$disabled_filter.")";
                //var_dump($filter);
                // Get attributes (ou is the name, DN serve to know if it is a first level, to exclude second level eventually)
                $attributes=array('uid','dn','sn','cn','givenName','mail','businessCategory');
                //var_dump($this->CONFIG['ldap_base_dn']);
                //var_dump($attributes);
                $result = ldap_search($this->ad, $searchdn, $filter, $attributes);
                //var_dump($result);
                if ($result === FALSE) { echo "Erreur LDAP : ".ldap_error($this->ad); return False; }
                $entries = ldap_get_entries($this->ad, $result);
                return $entries;
                }



	/*
	* This function retrieves and returns CN from given DN
	*/
	function getCN($dn) {
		preg_match('/[^,]*/', $dn, $matchs, PREG_OFFSET_CAPTURE, 3);
		return $matchs[0][0];
		}

	/*
	* This function return the username without the domain name 
	*/
	function getUsername($fqu) {
		if (strpos($fqu, '@')!=false)
			{
			$user_splited=explode('@',$fqu);
			$user=$user_splited[0];
			return $user;
			}	
		}
	/*
	* This function checks group membership of the user, searching only
	* in specified group (not recursively).
	*/
	function checkGroup($userdn, $groupdn) {
	    $attributes = array('members');
	    $result = ldap_read($this->ad, $userdn, "(memberof={$groupdn})", $attributes);
	    if ($result === FALSE) { echo "Lap Error"; return FALSE; };
	    $entries = ldap_get_entries($this->ad, $result);
		var_dump($entries);
	    return ($entries['count'] > 0);
	}


	/*
	* This function checks group membership of the user, searching
	* in specified group and groups which is its members (recursively).
	*/
	function checkGroupEx($userdn, $groupdn) {
	    $attributes = array('memberOf');
		//echo $ad.' '.$userdn;
	    $result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attributes);
	    if ($result === FALSE) { return FALSE; };
	    $entries = ldap_get_entries($this->ad, $result);
	    if ($entries['count'] <= 0) { echo "<br />Erreur -1 dans checkGroupEx</br />";return FALSE; };
	    if (empty($entries[0]['memberof'])) {  return FALSE; } else {
		for ($i = 0; $i < $entries[0]['memberof']['count']; $i++) {
		    if ($entries[0]['memberof'][$i] == $groupdn) { return TRUE; }
		    elseif ($this->checkGroupEx($entries[0]['memberof'][$i], $groupdn)) { return TRUE; };
		};
	    };
	    //-debug-echo "<br />Erreur chelou dans checkGroupEx</br />";
	    return FALSE;
	}


	function getMembers($group) {
		//echo "Searching $group";
		$query = ldap_search($this->ad, $this->CONFIG['ldap_base_dn'],"cn=$group");
		// Read all results from search
		$data = ldap_get_entries($this->ad, $query);
		//var_dump($data);
		$dirty = 0;
		foreach($data[0]['memberuid'] as $member) {
			if($dirty == 0) {
				$dirty = 1;
			} else {
				$members[] = $member;
			}
		}
		return $members;
	}

	function getAccountExpirationDate($username) {
		$attributes = array('accountExpires');
		$userdn = $this->getDN($username);
		//echo $username.' '.$ad_basedn.' = '.$userdn.'<br />';
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attributes);
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		//-debug-echo "Windows timestamp : ".$entries[0]['accountexpires'][0]."<br />";
		if (empty($entries[0]['accountexpires'][0])) 
			{return FALSE; } 
		else if ($entries[0]['accountexpires'][0]=='9223372036854775807' OR $entries[0]['accountexpires'][0]=='0')
			{return 0;}
		else
			{return $this->ldaptimestamp_convert($entries[0]['accountexpires'][0]);}

	}

	// Get a specific attribute of a user DN - /!\ return an array with attributes names in lowercase !
	function getAttribute($username,$attribute,$userdn=NULL) {
		//echo $username.$attribute;
		if ($userdn==NULL) $userdn = $this->getDN($username);
		//echo "UserDn : ".$userdn;
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', array($attribute));
		if ($result === FALSE) { return FALSE; }
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries[0]);
                // Return first match if size is only one
                if ($entries[0][$attribute]['count'] == 1)
                        return $entries[0][$attribute][0];
                // Else return full array
                else
                        return $entries[0][$attribute];
	}


	//*********
	// This function returns all groups, eventually filtered by pattern $filter if given
	//*********
	// /!\ Warning : there is an intermediary function in howto.php
	function getAllGroups($filter=NULL,$additional_groups=NULL) {
	$fields=array('cn');
	// A filter as been given
	if ($filter) {
		// But some additional groups can be added this way. They bypass the filter.
		if (!empty($additional_groups)) {
			$txt_or='(|';$txt_or_end=')';
			foreach ($additional_groups as $group) {
				//$a[]='Ce groupe :'.$group;
				$txt_additional_groups.='(cn='.$group.')';
				}
			}
		$ldap_query='(&(objectclass=group)'.$txt_or.'(cn=*'.$filter.'*)'.$txt_additional_groups.')'.$txt_or_end;
		//$a[]=$ldap_query;
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'],$ldap_query,$fields);

		}
	else
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'], '(objectclass=group)',$fields);
	if ($result === FALSE) { return FALSE; }
	$entries = ldap_get_entries($this->ad, $result);
	foreach ($entries as $entry) {
		if ($entry['cn'][0]!=NULL)
			$a[]=$entry['cn'][0];
		}

	//print_r($entries);
	return $a;

	}

	// Get a an array of attributes of a user DN -  /!\ return an array with attributes names in lowercase !
	function getAttributes($username,$attribute) {
		$userdn = $this->getDN($username);
		//echo $userdn;
		$result = ldap_read($this->ad, $userdn, '(objectclass=*)', $attribute);
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		return $entries[0];
		
	}


	function getEmailList() {
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'], '(mail=*)', array('mail','sn','givenname'));
		if ($result === FALSE) { return FALSE; };
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries);
		return $entries;
		
	}

	function explode_dn($dn, $with_attributes=0)
	{
	    $result = ldap_explode_dn($dn, $with_attributes);
	    //translate hex code into ascii again
	    if ($result) {
			foreach($result as $key => $value) $result[$key] = preg_replace("/\\\([0-9A-Fa-f]{2})/e", "''.chr(hexdec('\\1')).''", $value);
			return $result;
			}
	    return false;
	}

	function ldaptimestamp_convert($ldaptimestamp) {
		$winSecs       = (int)($ldaptimestamp / 10000000); // divide by 10 000 000 to get seconds
		$unixTimestamp = $winSecs - ((1970-1601) * 365.242190) * 86400; // 1.1.1600 -> 1.1.1970 difference in seconds
		return intval($unixTimestamp);
	}

	function unbind() {
		ldap_unbind($this->ad);
	}

	// ***************
	// Function to retrieve group name by GID - Need to browse all groups. Be carefull ! IT IS SLOW !
        function getGroupFromid($id,$restrict_buildin=False) {
		if ($this->CONFIG['ad_basedn_buildingroups'] && $restrict_buildin)
			$basedn=$this->CONFIG['ad_basedn_buildingroups'];
		else
			$basedn=$this->basedn;
		$ldapattributes = array("objectSid","samaccountname");
		$filter="(ObjectClass=group)";
		//echo $basedn;
		$q = ldap_search($this->ad, $basedn,$filter,$ldapattributes);
		$r = ldap_get_entries($this->ad, $q);
		//print_r($r).'<br /><br />';
		foreach ($r as $g) {
			//print_r($g).'<br /><br />';
			$arr_gid=explode("-",$this->bin_to_str_sid($g['objectsid'][0]));
			//echo $arr_gid[7].'compared to '.$id.PHP_EOL;
			if($arr_gid[7]==$id) {return $g['samaccountname'][0];}
			}
		

		//var_dump($entry);
	}
	//***********
	// Function to get groups the user belongs, with an option to get PRIMARY GROUP (which takes more time)
	// **********
	function getGroups($username,$GetPrimary=False) {
		$raw=$this->getAttributes($username,array('memberOf','primaryGroupID'));
		$primary=($raw[primarygroupid][0]);
		//echo $primary;
		$allgroups=array();
		foreach ($raw['memberof'] as $group) 
			{
			//$group=explode('')
			//echo $group;
			$explodedngroup=$this->explode_dn($group);
//print_r($explodedngroup);
			$splitgroup=explode("=",$explodedngroup[0]);
			if ($splitgroup[1]!="" && $splitgroup[1]!=NULL)
				$allgroups[]=$splitgroup[1];
			}
		// Append primary Group
		if ($GetPrimary) {
			$allgroups[]=$this->getGroupFromid($primary,True);
			}
		//print_r($allgroups);
		return $allgroups;
		}

        // **************
        // Write in LDAP server
        // **************
        function SetValue($username,$value_name,$value_index=0,$value)
                {
                if ($valuename=='mobile')
                        $userdata[$value_name][$value_index]=$value;
                else
                        $userdata[$value_name]=$value;
                $userdn = $this->getDN($username);
                $mod=ldap_modify($this->ad, $userdn, $userdata);
                return $mod;
                }


	function CountPeople($firstname,$lastname=NULL)
		{
		if ($firstname && $lastname)
			$filter="(|(sn=$lastname*)(givenName=$firstname*))";
		else {
			$filter="(givenName=*$firstname*)";
			//echo $filter;
			}
			
		$result = ldap_search($this->ad, $this->CONFIG['ad_basedn'],$filter,array('sn'));
		$entries = ldap_get_entries($this->ad, $result);
		//print_r($entries);
		return $entries['count'];
		}

	// ******************
	// Function to convert AD Binary HEx to string and invert
	// ******************

	// Returns the textual SID
	function bin_to_str_sid($binsid) 
	{
	    $hex_sid = bin2hex($binsid);
	    $rev = hexdec(substr($hex_sid, 0, 2));
	    $subcount = hexdec(substr($hex_sid, 2, 2));
	    $auth = hexdec(substr($hex_sid, 4, 12));
	    $result    = "$rev-$auth";

	    for ($x=0;$x < $subcount; $x++) {
		$subauth[$x] = 
		    hexdec($this->little_endian(substr($hex_sid, 16 + ($x * 8), 8)));
		$result .= "-" . $subauth[$x];
	    }

	    // Cheat by tacking on the S-
	    return 'S-' . $result;
	}

	// Converts a little-endian hex-number to one, that 'hexdec' can convert
	function little_endian($hex) 
	{
	    $result = "";

	    for ($x = strlen($hex) - 2; $x >= 0; $x = $x - 2) 
	    {
		$result .= substr($hex, $x, 2);
	    }
	    return $result;
	}


	// This function will convert a binary value guid into a valid string.
	function bin_to_str_guid($object_guid) 
	{
	    $hex_guid = bin2hex($object_guid);
	    $hex_guid_to_guid_str = '';
	    for($k = 1; $k <= 4; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 8 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-';
	    for($k = 1; $k <= 2; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 12 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-';
	    for($k = 1; $k <= 2; ++$k) {
		$hex_guid_to_guid_str .= substr($hex_guid, 16 - 2 * $k, 2);
	    }
	    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 16, 4);
	    $hex_guid_to_guid_str .= '-' . substr($hex_guid, 20);

	    return strtoupper($hex_guid_to_guid_str);
	}

}
?>
