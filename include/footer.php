<?php
echo '
<!-- Jquery first -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

<!-- From Bootstrap -->
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="ckeditor5-35.3.0/build/ckeditor.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script src="js/jquery-te/jquery-te-1.4.0.min.js" type="text/javascript">
<script type="text/javascript" src="js/Chart.js"></script>
<!--<script type="text/javascript" src="js/jquery-validation.js"></script>-->
<script src="addtocalendarbutton/assets/js/atcb.min.js" defer></script>

';
if ($_GET['page']=='calendar') {
	echo "
	<script src='fullcalendar/lib/moment.min.js'></script>
	<script src='fullcalendar/fullcalendar.min.js'></script>
	<script src='fullcalendar/locale/fr.js'></script><!-- TODO : ability to load more locale if needed, but then specify which one is loaded then in fullcalendar config -->
	";
}


if ($CONFIG['display-footer']) {
	echo '<div id="attendeefooter">
			<a href="https://framagit.org/labiloute/attendee" target="new">Attendee</a> 
			is a free software licensed under GNU GPLv3+, 
			created by the <a href="https://artifaille.fr" target="new">Artifaille</a> Team</div>';
}
echo '
</body>
<!--<link rel="stylesheet" href="css/ck-content-styles.css" type="text/css">-->
<!--<link rel="stylesheet" href="css/ck-custom.css" type="text/css">-->
</html>
';
?>
