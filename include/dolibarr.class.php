<?php
// To test Dolibarr API : http://yourdolibarr/api/index.php/explorer/
class Dolibarr
{
	public $CONFIG;
	public $error;
	public $my;

	/**
	* Constructor
	*/
	function __construct($CONFIG,$my=False)
	{
        	$this->CONFIG=$CONFIG;
		$this->error=False;
		$this->my=$my;
	}

	/* *********************************** */
	// READ ONLY
	/* *********************************** */

	function count_members($email) {
		$ret=False;
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			$SubscriptionsParam = ['limit'=>'2','sortorder'=>'ASC','sortfield'=>'t.rowid','sqlfilters'=>'(t.email:=:\''.$email.'\')']; // means : (t.email:like:'user@provider.com')
			$SubscriptionsResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members", $SubscriptionsParam);
			$response = json_decode($SubscriptionsResult, true); //because of true, it's in an array

			//var_dump($response);
                        if ($response['error']['code']=='404') {
                                $this->error=_('Dolibarr-member-unknown');
                                $ret=0;
                                }
                        else if ($response['error']['code']=='401') {
                                $this->error=_('Dolibarr-unauthorized');
                                $ret=-1;
                                }
			else
				$ret = count($response);
			}
		return $ret;
		}


	function GetMemberInfo($email) {
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			$SubscriptionsParam = ['limit'=>'2','sortorder'=>'ASC','sortfield'=>'t.rowid','sqlfilters'=>'(t.email:=:\''.$email.'\')']; // means : (t.email:like:'user@provider.com')
			$SubscriptionsResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members", $SubscriptionsParam);
			$response = json_decode($SubscriptionsResult, true); //because of true, it's in an array

			//var_dump($response);
                        if ($response['error']['code']=='404') {
                                $this->error=_('Dolibarr-member-unknown');
                                }
			elseif (count($response) > 1) {
				$this->error=_('Dolibarr-two-or-more-members-in-db');
				}
			else
				return $response[0];
			}
		return False;
		}

	function get_last_subscription_date($email) {
		// If link to dolibarr members API is setup, display members information from Dolibarr
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			($date==NULL)?$date=date('Ymd'):NULL;
			$Subscriptions = [];
			$SubscriptionsParam = ['limit'=>'2','sortorder'=>'ASC','sortfield'=>'t.rowid','sqlfilters'=>'(t.email:=:\''.$email.'\')']; // means : (t.email:like:'user@provider.com')
			$SubscriptionsResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members", $SubscriptionsParam);
			$response = json_decode($SubscriptionsResult, true); // because of true, it's in an array
			$nb_dolibarr_users = count($response);
			//var_dump($response);
			if ( ! isset($response['error']['code']) && $nb_dolibarr_users>1) {
				//echo $nb_dolibarr_users;
				$this->error=_('Dolibarr-two-or-more-members-in-db');
				return False;
				}
			else if ($response['error']['code']=='404') {
				$this->error=_('Dolibarr-member-unknown');
				return False;
				}
			else {
				//var_dump($response[0]);
				$last_subscription_date = $response[0]['last_subscription_date_end'];
				$this->last_user_id=$response[0]['id'];
				return $last_subscription_date;
				}
			}
		return False;
		}


	function get_list_of_term_subscriptions($send_email=False,$date=NULL) {
		$list=array();
		// If link to dolibarr members API is setup, display members information from Dolibarr
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			($date==NULL)?$date=date('Ymd'):NULL;
			$SubscriptionsParam = ['limit'=>'1000','sortorder'=>'ASC','sortfield'=>'dateadh','sqlfilters'=>'(t.datef:=:\''.$date.'\')']; // , "sqlfilters" => '(t.volume_units:>:0)'
			$SubscriptionsResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."subscriptions/", $SubscriptionsParam);
			$SubscriptionsResult = json_decode($SubscriptionsResult, true);
			if ($SubscriptionsResult['error'] and $SubscriptionsResult['error']['code']=='404') {
				//echo "[ERROR] Error looking for terminated subscriptions (or no terminated subscription).".PHP_EOL;
				return False;
				}
			foreach ($SubscriptionsResult as $r) {
				//$list[]=$r;
				//echo "Adhérent : ".$r['fk_adherent'];
				//var_dump($r);
				$list[]=$this->get_adherent_email($r['fk_adherent']);
				}
			//var_dump($list);
			return $list;
			}
		return False;
		}


	function get_adherent_email($adherent_id) {

		// If link to dolibarr members API is setup, display members information from Dolibarr
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='' && $adherent_id) {
			// Retrieve user
			$Member = [];
			$MemberParam = []; // , "sqlfilters" => '(t.volume_units:>:0)'
			$MemberResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members/".$adherent_id, $MemberParam);
			$MemberResult = json_decode($MemberResult, true);
			$nb_term = count($MemberResult);
			return $MemberResult;
			}
		return False;
		}


	/*
		Verify validity of subscription
	*/
	function is_valid($sub_date) {
		$now = time(); // or your date as well
		//echo $now.'<br />';
		//$last_subscription_time = strtotime(date('Y-m-d',$sub_date));
		//echo $last_subscription_time.'<br />';
		//$datediff = $now - $last_subscription_time;
		//$datediff = $now;
		//$diff=floor($datediff / (60 * 60 * 24));
		//echo "Diff ".$diff;
		//if ($diff >= $this->CONFIG['members-validity']) return false;
		if ($sub_date > $now) return true;
		else return false;
		}

	function format_date($date) {
		$dt = new DateTime;
		$date_formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE);
		$date = $date_formatter->format(strtotime(date('Y-m-d',$date)));
		return $date;
		}

	function getStock($json=False) {
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			// Retrieve products list
			$listProduits = [];
			// mode=1 avoir que les produits ; mode=2 seulement les services
			$produitParam = ["limit" => 1000, "sortfield" => "t.label", 'sortorder' => 'DESC', "mode" => "1"];
			$listProduitsResult = $this->CallAPI("GET", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."products", $produitParam);
			$listProduitsResult = json_decode($listProduitsResult, true);
			if ($json) return $listProduitsResult;
			if (isset($listProduitsResult["error"]) && $listProduitsResult["error"]["code"] >= "300") {
				echo "[ERROR] Error !!";
			} 
			else {
				foreach ($listProduitsResult as $produit) {
					/*var_dump($produit);
					echo '<br /><br />';*/
					$listProduits[intval($produit["id"])]['label'] = html_entity_decode($produit["label"], ENT_QUOTES);
					$listProduits[intval($produit["id"])]['nombre'] = html_entity_decode($produit["volume_units"], ENT_QUOTES);
					}
				}
			return $listProduits;
			}
		return False;
		}

	function getProductInfo($id,$json=False) {
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			// Retrieve products list
			//$Produit = [];
			// mode=1 avoir que les produits ; mode=2 seulement les services
			$produitParam = []; // , "sqlfilters" => '(t.volume_units:>:0)'
			$ProduitResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."products/".$id, $produitParam);
			if ($json) return $ProduitResult;
			$ProduitResult = json_decode($ProduitResult, true);

			if (isset($ProduitResult["error"]) && $ProduitResult["error"]["code"] >= "300") {
				echo "[ERROR] Error !!";
			}
			return $ProduitResult;
			}
		return False;
		}
	function getProductCategory($id,$json=False) {
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			// Retrieve products list
			// mode=1 avoir que les produits ; mode=2 seulement les services
			$CatParam = []; // , "sqlfilters" => '(t.volume_units:>:0)'
			$CatResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."products/".$id."/categories", $CatParam);
			// Eventually return raw json
			if ($json) return $CatResult;
			$CatResult = json_decode($CatResult, true);
			//var_dump($CatResult["error"]);
			if (isset($CatResult["error"]) && $CatResult["error"]["code"] >= 300) {
				//echo _('No category found (or error occured)').PHP_EOL;
				return False;
				}
			return $CatResult;
			}
		return False;
		}
//
	function getProductImage($id,$json=False) {
		if ($this->CONFIG['dolibarr']['apikey'] && $this->CONFIG['dolibarr']['api']!='') {
			// Retrieve products list
			// mode=1 avoir que les produits ; mode=2 seulement les services
			$ImgParam = []; // , "sqlfilters" => '(t.volume_units:>:0)'
			$ImgResult = $this->CallAPI("GET",  $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."documents?modulepart=product&id=".$id."", $ImgParam);
			// Eventually return raw json
			if ($json) return $ImgResult;
			$ImgResult = json_decode($ImgResult, true);
			//var_dump($CatResult["error"]);
			if (isset($ImgResult["error"]) && $ImgResult["error"]["code"] >= 300) {
				//echo _('No image found (or error occured)').PHP_EOL;
				return False;
				}
			return $ImgResult;
			}
		return False;
		}

	/* *********************************** */
	// WRITE DATA
	/* *********************************** */

	function CreateMember($data) {
		//echo "Creation de la fiche<br />";
		//var_dump($data);
		if (	$data['member-email'] 
			&& $data['member-firstname']
			&& $data['member-lastname']
			&& $data['member-address']
			&& $data['member-zip']
			&& $data['member-town']
			&& $data['member-phone'] ) {
				//echo "Tout est là :<br />";


				$newMember = [
						"firstname"	=> $data['member-firstname'],
						"lastname"	=> $data['member-lastname'],
						"email"		=> $data['member-email'],
						"typeid"	=>'3',
						"morphy"	=>'phy',
						"zip"		=>$data['member-zip'],
						"town"		=>$data['member-town'],
						"phone_perso"	=>$data['member-phone']
						];
				$newMemberResult = $this->CallAPI("POST", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members", json_encode($newMember));
				$newMemberResult = json_decode($newMemberResult, true);

				//var_dump($newMemberResult);
				if (isset($newMemberResult["error"]) && $newMemberResult["error"]["code"] >= "300") {
					//var_dump($newMemberResult);
					return False;
					}
				else {
					return $newMemberResult;
				}
			}
		}


	function CreateSubscription($memberid,$bankid,$lineid) {
		//echo "Creation de la souscription<br />";
		if ($memberid) {
				//echo "Tout est là.<br />";

				$now_en = date('Y-m-d'); // now
				$now = time(); // now in english format
				$nextsub = $now + (60 * 60 * 24 * $this->CONFIG['members-validity']); // Next subscription will be in $nextsub seconds
				$nextsub_en = date('Y-m-d',$nextsub); // Convert it in english date format

				//echo "Start $now <br />";
				//echo "End $nextsub <br />";

				$newSubscription = [
						"start_date"	=> $now,
						"end_date"	=> $nextsub,
						"amount"	=> $this->CONFIG['dolibarr']['subscriptionamount'],
						"label"		=> $this->CONFIG['dolibarr']['paiementlabel'],
						"accountid"	=> $bankid,
						#"operation"	=> $operation,
						"payment"	=> $now_en,
						"paymentdate"	=> $now_en,
						"paymentyear"	=> '2021',
						"paymentmonth"	=> '01',
						"paymentday"	=> '01',
						"paymentsave"	=> 'bankdirect',
						"fk_bank"	=> $lineid
						];
				//var_dump(json_encode($newSubscription));
				$newSubscriptionResult = $this->CallAPI("POST", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members/".$memberid."/subscriptions", json_encode($newSubscription));
				$newSubscriptionResult = json_decode($newSubscriptionResult, true);

				//var_dump($newSubscriptionResult);
				if (isset($newSubscriptionResult["error"]) && $newSubscriptionResult["error"]["code"] >= "300") {
					return False;
					}
				else {
					return $newSubscriptionResult;
					}
				}
			}


	function UpdateMember($memberid,$data) {
		if ($memberid) {
				$memberdata= [
						"address" => $data['address'],
						"zip" => $data['zip'],
						"town" => $data['city']
						];

				//var_dump(json_encode($memberdata));
				//var_dump($memberid);
				//var_dump($this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members/".$memberid);
				$UpdateMember = $this->CallAPI("PUT", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."members/".$memberid, json_encode($memberdata));
				$UpdateMember = json_decode($UpdateMember, true);
				//var_dump($UpdateMember);
				if (isset($UpdateMember["error"]) && $UpdateMember["error"]["code"] >= "300") {
					return False;
					}
				else {
					return $UpdateMember;
					}
				}
			}



	function UpdateSubscription($subscriptionid,$lineid) {
		//echo "Creation de la souscription<br />";
		if ($subscriptionid) {
				//echo "Tout est là.<br />";
				$now_en = date('Y-m-d'); // now
				$now = time(); // now in english format
				$nextsub = $now + (60 * 60 * 24 * $this->CONFIG['members-validity']); // Next subscription will be in$nextsub seconds
				$nextsub_en = date('Y-m-d',$nextsub); // Convert it in english date format

				$Subscription = [
						"fk_bank"	=> $lineid
						];
				$SubscriptionResult = $this->CallAPI("PUT", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."subscriptions/".$subscriptionid."", json_encode($Subscription));
				$SubscriptionResult = json_decode($SubscriptionResult, true);

				//var_dump($newSubscriptionResult);
				if (isset($SubscriptionResult["error"]) && $SubscriptionResult["error"]["code"] >= "300") {
					return False;
					}
				else {
					return $SubscriptionResult;
				}
			}
		}


	function CreateBankLine($bankid) {
		//echo "Creation de la ligne comptable<br />";
		$now_en = date('Y-m-d'); // now
		$now = time(); // now in english format
		//$nextsub = $now + (60 * 60 * 24 * $this->CONFIG['members-validity']); // Next subscription will be in$nextsub seconds
		//$nextsub_en = date('Y-m-d',$nextsub); // Convert it in english date format

		$newBankLine = [
				"date"		=>	$now,
				"type"		=>	$this->CONFIG['dolibarrpaiementtype'][$bankid],
				"label"		=>	$this->CONFIG['dolibarr']['paiementlabel'],
				"amount"	=>	$this->CONFIG['dolibarr']['subscriptionamount']
				//"category": 0,
				//"cheque_number": "string",
				//"cheque_writer": "string",
				//"cheque_bank": "string"
				];
		$newBankLineResult = $this->CallAPI("POST", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."bankaccounts/".$bankid."/lines", json_encode($newBankLine));
		$newBankLineResult = json_decode($newBankLineResult, true);

		var_dump($newBankLineResult);
		if (isset($newBankLineResult["error"]) && $newBankLineResult["error"]["code"] >= "300") {
			return False;
			}
		else {
			return $newBankLineResult;
			}
		}


	function CreateBankLink($bankid,$banklineid,$memberid,$membername) {
		$now_en = date('Y-m-d'); // now
		$now = time(); // now in english format

		// Note that the field label seems to be rewrited anyway.
		$newBankLink = [
				"url_id"	=>	$memberid,
				"url"		=>	$this->CONFIG['dolibarr']['url'].'/adherents/card.php?rowid=',
				"label"		=>	$membername,
				"type"		=>	'member'
				];
		$newBankLinkResult = $this->CallAPI("POST", $this->CONFIG['dolibarr']['apikey'], $this->CONFIG['dolibarr']['api']."bankaccounts/".$bankid."/lines/".$banklineid."/links", json_encode($newBankLink));
		$newBankLinkResult = json_decode($newBankLinkResult, true);

		if (isset($newBankLinkResult["error"]) && $newBankLinkResult["error"]["code"] >= "300") {
			var_dump($newBankLinkResult);
			return False;
			}
		else {
			return $newBankLinkResult;
			}
		}


	/* *********************************** */
	// Example of function to call a REST API
	/* *********************************** */
	function CallAPI($method, $apikey, $url, $data = false)
	{
		$curl = curl_init();
		$httpheader = ['DOLAPIKEY: '.$apikey];

		switch ($method) {
		case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);
			$httpheader[] = "Content-Type:application/json";

			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;

		case "PUT":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
			$httpheader[] = "Content-Type:application/json";

			if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
		default:
			if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
		}
	    // Optional Authentication:
	    //    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    //    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

		//echo $url;
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader);

		$result = curl_exec($curl);

		curl_close($curl);

		//var_dump($result);
		return $result;
	}

	function StockRefreshCache() {

		// First get item list from Dolibarr Stock
		$stocked=$this->getStock();
		$products=array();
		//echo '<table id="dolistock">';
		$keylistsql="(";
		$i=0;
		$err=0;
		$updated=0;
		$inserted=0;

		// Store config value of Dolibarr image and category field in a more convenient variable
		$image_field=$this->CONFIG['dolibarr']['image_field'];
		//$cat_field=$this->CONFIG['dolibarr']['category_field'];
		foreach ($stocked as $key=>$stock) {
			//if ($key!='124') continue; //debug
			// Append a comma after each key to prepare deletion of extra items
			($i>0)?$keylistsql.=",":NULL;
			// Append item ID
			$keylistsql.=$key;
			// Notice user
			echo "Working on product $key. Inserting/Updating internal table".PHP_EOL;
			// Get product from Dolibarr instance
			$product=$this->getProductInfo($key);
			// Get categories of this product (only the first will be kept)
			$categories=$this->getProductCategory($key);
			// Get product main image
			$image=$this->getProductImage($key);
			// If Stock is empty, set it to zero
			//if ($key==124) {var_dump($image);} //debug
			($product['stock_reel']=='')?$product['stock_reel']=0:NULL;
			// If first category exists keep it
			if ($categories) {
				if ($categories['label']){$cat_raw=$categories['label'];$cat_value="'".$categories['label']."'";echo "[DEBUG] Found a category for this product : ".$categories['label'].PHP_EOL;}
				else if ($categories[0]['label']){$cat_raw=$categories[0]['label'];$cat_value="'".$categories[0]['label']."'";echo "[DEBUG] Found a category for this product : ".$categories[0]['label'].PHP_EOL;}
				else $cat_value='NULL';
				}
			else {$cat_raw='';$cat_value='NULL';}

                        // If image exists, keep it
                        if ($image && ($image['share'] or $image['ecmfiles_infos'][0]['share'] or $image['ecmfiles_infos'][1]['share']) or $image[0]['share']) {
                                // Quick fix since Dolibarr 18.0.2 that removed ecmfiles_infos from image share api for unknown reason
                                if ($image['share']) $imagehash=$image['share'];
                                // Quick fix since Dolibarr 16.0.5 that removed ecmfiles_infos from image share api for unknown reason
                                else if ($image[0]['share']) $imagehash=$image[0]['share'];
                                // For versions older than 15.0.2
                                else if ($image['ecmfiles_infos'][0]['share']) $imagehash=$image['ecmfiles_infos'][0]['share'];
                                // Quick fix since Dolibarr 15.0.2 that put image share in a second index of ecmfiles_infos for unknown reason
                                else if ($image['ecmfiles_infos'][1]['share']) $imagehash=$image['ecmfiles_infos'][1]['share'];
                                $image_value="'".$this->CONFIG['dolibarr']['url']."/document.php?hashp=".$imagehash."'";
                                echo "[DEBUG] Found an image hash for this product : ".$imagehash.PHP_EOL;
                                //else $image_value='NULL';
                                }
			else if ($image_field && substr($product[$image_field], 0, 4)=='http')
				{$image_value="'".$this->my->escape_string($product[$image_field])."'";
				echo "[WARNING] Falling back to image field '".$image_field."' , found : ".$product[$image_field].".".PHP_EOL;}
			else $image_value='NULL';


			// Set category weight
			if ($this->CONFIG['stockcatweight'][$cat_raw]) {$catweight=$this->CONFIG['stockcatweight'][$cat_raw];}
			else $catweight=0;

			// Insert or Update query
			$sql="
			INSERT INTO stock (id,label,price,image,description,category,ref,stock) 
			VALUES(
				".$key.",
				'".$this->my->escape_string($product['label'])."',
				".$product['price'].",
				".$image_value.",
				'".$this->my->escape_string($product['description'])."',
				".$cat_value.",
				'".$this->my->escape_string($product['ref'])."',
				".$product['stock_reel'].")
			ON DUPLICATE KEY 
			UPDATE    
			label='".$this->my->escape_string($product['label'])."'
			,price=".$product['price']."
			,image=".$image_value."
			,description='".$this->my->escape_string($product['description'])."'
			,category=".$cat_value."
			,ref='".$this->my->escape_string($product['ref'])."'
			,stock=".$product['stock_reel']."
			,weight=".$catweight."
			";
			//echo nl2br($sql);
			//if ($key==124) {echo nl2br($sql);} //debug
			// If ok
			$ok['query']=$this->my->query_simple($sql);
			if ($ok['query']){ echo _('stock-item-upcreate-ok').PHP_EOL;}
			else {echo ('stock-item-upcreate-nok')."<br />".$this->my->last_error.PHP_EOL; echo "Query was : ".$sql;$err++;}
			#
			//echo $this->my->last_affected_rows;
			#
			if ($this->my->last_affected_rows==2) {$updated++;}
			else if ($this->my->last_affected_rows==1) {$inserted++;}
			$i++;
			}
		$infos.="$i elements has been checked : $inserted inserted, $updated updated, $err errors.".PHP_EOL;
		# Close key list (used for deletion)
		$keylistsql.=")";
		if ($i>0) {
			echo "Deleting unlisted products".PHP_EOL;
			$sql='DELETE FROM stock WHERE id NOT IN '.$keylistsql.' LIMIT 100';
			$ok['query']=$this->my->query_simple($sql);
			if ($ok['query']) { echo _('stock-item-delete-ok').PHP_EOL;$infos.=$this->my->last_affected_rows." "._('unlisted items has been deleted').PHP_EOL;}
			else $errors.=_('stock-item-delete-nok')." - ".$this->my->last_error.PHP_EOL;
			}
		echo "########### Report ########### ".PHP_EOL;

		echo $infos.PHP_EOL;
		echo $warnings.PHP_EOL;
		echo $errors.PHP_EOL;
		//echo $keylistsql;
		}

	function getLocalStock() {
		(! $this->CONFIG['stock']['display_without_image'])?$filter=" AND (image IS NOT NULL AND image!='')":NULL;
		(! $this->CONFIG['stock']['display_stock_null'])?$filter.=" AND stock >= 1 ":NULL;
		$sql = "SELECT
		id,
		label,
		price,
		image,
		description,
		IFNULL(category,'".$this->CONFIG['stock']['uncategorized']."') category,
		".$this->CONFIG['stockcatweight'][$category]."
		ref,
		stock
		FROM stock 
		WHERE 1 
		".$filter." 
		ORDER BY weight DESC,category
		LIMIT 10000";
		//echo $sql;
		$result=$this->my->query_assoc($sql);
		//var_dump($result);
		return $result;
		}
	}
?>
