<?php
function captcha() {
	// Check GD library presence
	if( !function_exists('gd_info') ) {
		throw new Exception('Required GD library is missing');
	}

	// Generate HTML for image src
	if ( strpos($_SERVER['SCRIPT_FILENAME'], $_SERVER['DOCUMENT_ROOT']) ) {
		$image_src = substr(__FILE__, strlen( realpath($_SERVER['DOCUMENT_ROOT']) )) . '?_CAPTCHA';
		$image_src = '/' . ltrim(preg_replace('/\\\\/', '/', $image_src), '/');
	} else {
		$_SERVER['WEB_ROOT'] = str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['SCRIPT_FILENAME']);
		$image_src = substr(__FILE__, strlen( realpath($_SERVER['WEB_ROOT']) )) . '?_CAPTCHA';
		$image_src = '/' . ltrim(preg_replace('/\\\\/', '/', $image_src), '/');
	}
	return $image_src;
}

// Provide Captcha
if( isset($_GET['_CAPTCHA']) ) {
	session_start();
	header ('Content-Type: image/png');
	$height=30;
	$width=100;
	$im = imagecreatetruecolor($width, $height)
		or die('Cannot Initialize new GD image stream');
	$text_color = imagecolorallocate($im, 0, 14, 91);
	$bg = imagecolorallocate($im, 255, 255, 255);
	for ($i=0; $i < $width ; $i++) {
		for ($j=0; $j < $height ; $j++) {
			imagesetpixel ($im ,$i,$j ,$bg);
			}
		}
	$val1=mt_rand(1,10);
	$val2=mt_rand(1,10);
	$operator=mt_rand(1,2);
	if ($operator==1)
        	{
		$operator='x';
		$result=$val1*$val2;
		}
	else {
		$operator='+';
		$result=$val1+$val2;
		}
	$_SESSION['captchacode'] = $result;
	$text=$val1.' '.$operator.' '.$val2.' =';
	imagestring($im, 5, 20, 5,$text, $text_color);
	imagepng($im);
	imagedestroy($im);
}
