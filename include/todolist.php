<?php

/* Defining the ToDo class */

class ToDo{
	
	/* An array that stores the todo item data: */
	
	private $data;
	public $my;
	
	/* The constructor */
	function __construct($par,$my){
		if(is_array($par))
			$this->data = $par;
	$this->my=$my;
	}
	
	/*
		This is an in-build "magic" method that is automatically called 
		by PHP when we output the ToDo objects with echo. 
	*/
		
	function __toString(){
		
		// The string we return is outputted by the echo statement
		($this->data['loved']=='1')?$love_class="unlove":$love_class='love';
		return '
			<li id="todo-'.$this->data['id'].'" class="todo">
			
				<div class="text">'.$this->data['text'].'</div>
				
				<div class="actions">
					<a href="#" class="edit">Edit</a>
					<a href="#" class="delete">Delete</a>
					<a href="#" class="'.$love_class.'">Love</a>
					(<span class="lovecount">'.$this->data['love'].'</span>)
				</div>
				
			</li>';
	}
	
	
	/*
		The following are static methods. These are available
		directly, without the need of creating an object.
	*/
	
	
	
	/*
		The edit method takes the ToDo item id and the new text
		of the ToDo. Updates the database.
	*/

	function edit($id, $text,$category){
		// Clean text
		$text = $this->esc($text);
		// Refuse empty text
		if(!$text) throw new Exception("Wrong update text!");
		// If category has changed, delete and then add in the other category
		$this->my->query_simple("SELECT * FROM todolist WHERE id=".$id." and category='".$category."' LIMIT 1");
		if ($this->my->num_rows==0) { #Means that category has changed, we delete and recreate
			$this->delete($id,$category);
			$this->createNew($text,$category);
			}
		# Else we only update
		else
			{
			$this->my->query_simple("	UPDATE todolist
							SET text='".$text."',
							category='".$category."'
							WHERE id=".$id."
							LIMIT 1"
						);
		
			if($this->my->last_affected_rows!=1)
				throw new Exception("Couldn't update item!");
			}
	}
	
	/*
		The delete method. Takes the id of the ToDo item
		and deletes it from the database.
	*/
	
	function delete($id,$category){
		$sql = "DELETE FROM todolist WHERE id='".$id."' LIMIT 1";
		//echo $sql;
		$this->my->query_simple($sql);
		
		if($this->my->last_affected_rows!=1)
			throw new Exception("Couldn't delete item!");

		// Delete eventually love records
		$sql = "DELETE FROM todolistup WHERE todo_id='".$id."' LIMIT 1000";
		//echo $sql;
		$this->my->query_simple($sql);
	}		
	/*
		The rearrange method is called when the ordering of
		the todos is changed. Takes an array parameter, which
		contains the ids of the todos in the new order.
	*/
	function rearrange($key_value,$category){

		$updateVals = array();
		foreach($key_value as $k=>$v)
		{
			$strVals[] = 'WHEN '.(int)$v.' THEN '.((int)$k+1).PHP_EOL;
		}
		
		if(!$strVals) throw new Exception("No data!");
		
		// We are using the CASE SQL operator to update the ToDo positions en masse:
		
		$r=$this->my->query_simple("	
						UPDATE todolist SET position = CASE id
						".join($strVals)."
						ELSE position
						END
						WHERE category='".$category."'");
		return $sql;
		if(!$r)
			throw new Exception("Error updating positions!");
	}
	
	/*
		The createNew method takes only the text of the todo,
		writes to the database and outputs the new todo back to
		the AJAX front-end.
	*/
	
	function createNew($text,$category){

		$text = $this->esc($text);
		if(!$text) throw new Exception("Wrong input data!");
		
		$posResult = $this->my->query_simple("SELECT MAX(position)+1 FROM todolist WHERE category='".$category."'");
		
		if($this->my->num_rows)
			list($position) = $posResult->fetch_array();

		if(!$position) $position = 1;
		$sql="INSERT INTO todolist SET text='".$text."', position = ".$position.", category = '".$category."'";
		//echo $sql;
		$this->my->query_simple($sql);
		
		if($this->my->last_affected_rows!=1)
			throw new Exception("Error inserting TODO!");
		
		// Creating a new ToDo and outputting it directly:
		
		echo (new ToDo(array(
			'id'	=> $this->my->last_id,
			'text'	=> $text,
			'category' => $category
			),
			$this->my)
		);
		echo "New Created";
		exit;
	}
	
	/*
		A helper method to sanitize a string:
	*/
	
	function esc($str){
		
		if(ini_get('magic_quotes_gpc'))
			$str = stripslashes($str);
		
		return $this->my->escape_string(strip_tags($str));
	}
	
} // closing the class definition

?>
