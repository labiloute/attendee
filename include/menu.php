<?php 
if (file_exists($CONFIG['base_folder'].'/images/'.$CONFIG['logo']))$logo_path=$CONFIG['base_folder'].'/images/'.$CONFIG['logo'];
else $logo_path='images/attendee_logo.png';
?>

<?php ($_GET['event_id'])?$evt_link='&event_id='.$_GET['event_id']:$evt_link=NULL; ?>

<nav class="navbar navbar-expand-md navbar-dark  bg-dark"><!-- removed fixed-top -->
    <a class="navbar-brand" href="?"><img src="<?php echo $logo_path; ?>" id="logo" /> Attendee</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse_v2" aria-controls="navbarCollapse_v2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    <div class="collapse navbar-collapse" id="navbarCollapse_v2">
    <ul class="nav navbar-nav navbar navbar-expand-md navbar-dark  bg-dark">
        <li class="nav-item active"><a class="nav-link" href="<?php echo $CONFIG['website-link'] ?>"><?php echo _('Back to').' '.$CONFIG['website-name'] ?></a></li>
        <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label"><?php echo _('Events'); ?></span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
            	<?php if ($CONFIG['enable-quickbook']) { ?>
		<li><a class="nav-link" href="?page=quickbook"><?php echo _('Quick booking') ?></a></li>
		<?php } ?>
                <li><a class="nav-link" href="?page=calendar"><?php echo _('Calendar view') ?></a></li>
                <li><a class="nav-link" href="?"><?php echo  _('List view') ?></a></li>
            </ul>
        </li>

	<?php if (! $_SESSION['profile']) { ?>
	<li class="nav-item"><a class="nav-link" href="?page=userspace"><?php echo _('Login') ?></a></li>
	<?php } ?>

	<?php if ($_SESSION['profile']) { ?>
        <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label"><?php echo _('Your space'); ?></span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="?page=userspace"><?php echo _('Your profile') ?></a></li>
                <li><a class="nav-link" href="?page=search"><?php echo _('Artinder') ?></a></li>
		<?php if ($CONFIG['enable-todolist']) { ?>
		<li><a class="nav-link" href="?page=todolist"><?php echo _('TodoList') ?></a></li>
		<?php } ?>

            </ul>
        </li>
	<?php } ?>

	<?php if ($auth->CheckAllowedNetworks() || $CONFIG['newmember-security']==False || $_SESSION['profile']=='ADMIN' || $_SESSION['profile']=='MANAGER') { ?>
	<li class="nav-item">
		<a class="nav-link" href="?page=newmember"><?php echo _('Subscription') ?></a>
	</li>
	<?php } ?>


	<?php 	if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) { ?>
        <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label"><?php echo _('Administration'); ?></span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
		<?php 	if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) { ?>
                <li><a class="nav-link" href="?page=schedule"><?php echo _('Schedule event') ?></a></li>
                <li><a class="nav-link" href="?page=places"><?php echo _('Manage places') ?></a></li>
		<li><a class="nav-link" href="?page=stats"><?php echo _('Global stats') ?></a></li>
		<li><a class="nav-link" href="?page=stats_users"><?php echo _('Users stats') ?></a></li>
		<li><a class="nav-link" href="?page=trainings"><?php echo _('Training stats') ?></a></li>
		<li><a class="nav-link" href="?page=letters"><?php echo _('Newsletters') ?></a></li>
		<?php } ?>
            </ul>
        </li>
	<?php } ?>

	<?php 	if ($CONFIG['dolibarr']['apikey'] and $CONFIG['dolibarr']['apikey']!='') { ?>
	<li class="nav-item">
		<a class="nav-link" href="?page=dolistock"><?php echo _('DoliStock') ?></a>
	</li>
	<?php } ?>

	<li class="nav-item">
		<a class="nav-link help" onclick="document.getElementById('help-modal').style.display='block'" class="w3-button" href="#"><?php echo _('Help'); ?></a>
	</li>

    </ul>
	<div id="main-login">
        	<?php echo $auth->GetLoginButton(True); ?>
	</div>	
    </div>

</nav>

<!--
<nav class="navbar navbar-expand-md navbar-dark  bg-dark">
      <a class="navbar-brand" href="?"><img src="<?php echo $CONFIG['base_folder'].'/images/'.$CONFIG['logo']; ?>" id="logo" /> Attendee</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $CONFIG['website-link'] ?>"><?php echo _('Back to').' '.$CONFIG['website-name'] ?></a>
          </li>
          <li class="nav-item">
	<?php if ($_GET['page']=='calendar') { ?>
	            <a class="nav-link" href="?"><?php echo _('List view') ?></a>
	<?php } else { ?>
	            <a class="nav-link" href="?page=calendar"><?php echo _('Calendar view') ?></a>
	<?php } ?>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?page=userspace"><?php echo _('Your space') ?></a>
          </li>
	<?php 	if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) { ?>
          <li class="nav-item">
            <a class="nav-link" href="?page=schedule"><?php echo _('Schedule event') ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?page=places"><?php echo _('Manage places') ?></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?page=stats"><?php echo _('Stats') ?></a>
          </li>
	<?php } ?>
	<?php 	if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')) { ?>
          <li class="nav-item">
            <a class="nav-link" href="?page=todolist"><?php echo _('TodoList') ?></a>
          </li>

	<?php } ?>

	<?php if ($auth->CheckAllowedNetworks() || $CONFIG['newmember-security']==False || $_SESSION['profile']=='ADMIN' || $_SESSION['profile']=='MANAGER') { ?>
          <li class="nav-item">
            <a class="nav-link" href="?page=newmember"><?php echo _('Subscription') ?></a>
          </li>
	<?php } ?>

	<?php if ($_SESSION['profile']) { ?>
          <li class="nav-item">
            <a class="nav-link" href="?page=search"><?php echo _('Artinder') ?></a>
          </li>
	<?php } ?>

	<?php 	if ($CONFIG['dolibarr']['apikey'] and $CONFIG['dolibarr']['apikey']!='') { ?>
         <li class="nav-item">
            <a class="nav-link" href="?page=dolistock"><?php echo _('DoliStock') ?></a>
          </li>
	<?php 	}  ?>

	<li class="nav-item"><a class="nav-link help" onclick="document.getElementById('help-modal').style.display='block'" class="w3-button" href="#"><?php echo _('Help'); ?></a>
        </li>
        </ul>

        <?php echo $auth->GetLoginButton(True); ?>
      </div>
    </nav>
-->
