<?php



class Event
{
	public $my;
	public $id;
	public $error;
	public $CONFIG;

	/**
	* Constructor
	*/
	function __construct($CONFIG,$my,$id=False)
	{
        	$this->CONFIG=$CONFIG;
		$this->my=$my;
		$this->id=$id;
		$this->error=False;
	}


	function getfields($fields) {
		$fields = implode(",",$fields);
		$sql = "SELECT $fields FROM sessions WHERE id=".$this->id." LIMIT 1";
		//echo $sql;
		$q=$this->my->query_simple($sql);
		$r=$q->fetch_assoc();
		return $r;
		}

	function isfull($id) {
		$this->id=$id;
		$max=$this->getfields(array('max_attendee'));
		// Count attendee actually registered (without unconfirmed)
		$sql='SELECT * FROM attendees WHERE session_id=\''.$this->id.'\' AND state=\''.$this->CONFIG['state_confirmed'].'\'';
		$q=$this->my->query_simple($sql);
		$n=$this->my->num_rows;
		//var_dump($n);
		//var_dump($max);
		if ($n>$max['max_attendee']-1) return True;
		else return False;
		}


	function ScheduleForm($event_id,$template_id,$Places) {
		$form=False;
		// Query for template or specific session
		if (isset($event_id) or isset($template_id)) {
			// Init Classes
			$Place = new Place($this->CONFIG,$my);

			// Select either the template or the session depending of which element is asked by user
			if (isset($template_id) && $template_id!="") {
				$sql = "SELECT * FROM templates WHERE id='".$template_id."' LIMIT 1";
				}
			// Priority to event_id
			if (isset($event_id) && $event_id!="") {
				$sql = "SELECT * FROM sessions WHERE id='".$event_id."' LIMIT 1";
				}

			// Execute and fetch
			$r=$this->my->query_assoc($sql);
			$r=$r->fetch_assoc();

			// Fetch users
			$sql = 'SELECT 
			users.id,
			users.username,
			users.user_first_name,
			users.user_last_name,
			users.user_comment
			FROM `users`
			WHERE users.disabled IS NULL
			ORDER BY user_last_name ASC
			';
			$u=$this->my->query_assoc($sql);
			$nb_u = $this->my->num_rows;

			// Fetch places
			$places = $Places->GetPlacesList();

			// Conditionnal statements
			($r['notice']=='1')?$notice='checked':$notice='';
			($r['quickbook']=='1')?$quickbook='checked':$quickbook='';

			$form='
			<span class="schedule-wrapper title">'._('Title').'</span>
			<span class="schedule-wrapper content"><input type="text" value="'.$r['title'].'" id="schedule-title" name="schedule-title" required /></span>
			<!-- PRICE SELECTOR (Html) -->

			';


			if ($r['price']=='-1'){$schedulepricespecial1='checked="checked"';$grey='grey';}
			else if($r['price']=='0'){$schedulepricespecial0='checked="checked"';$grey='grey';}
			else{$schedulepricespecial0='';$schedulepricespecial1='';$grey='';}

			$form.='
			<span class="schedule-wrapper title">'._('Price').'</span>
			<span class="schedule-wrapper content">
				<input type="radio" id="schedule-price-special-1" name="schedule-price-special" value="-1" '.$schedulepricespecial1.'>
				<label for="schedule-price-special-1">'.$this->CONFIG['rewrite_minus_one_to'].'</label><br>
				<input type="radio" id="schedule-price-special-0" name="schedule-price-special" value="0" '.$schedulepricespecial0.'>
				<label for="schedule-price-special-0">'._('Free as in free beer').'</label><br>
			'._('or').' <input type="text" id="schedule-price" class="'.$grey.'" name="schedule-price" value="'.$r['price'].'" required /> '.$this->CONFIG['currency_symbol'].'
			</span>
			<!-- PRICE SELECTOR (Html) -->

			<!-- USER ID SELECTOR (Html) -->
			<span class="schedule-wrapper title">'._('schedule-user-username').'</span>
			<span class="schedule-wrapper content">
			<select id="schedule-user-select" name="schedule-user-select">
			<option value="">-- '._('Select').' --</option>
			';
			$i=0;
			// List users in selectbox
			while ($row = $u->fetch_assoc()) {
				($row['username']==$r['username'])?$sel_session_user_select='selected="selected"':$sel_session_user_select=NULL;
				$form.='<option value="'.$row['username'].'" '.$sel_session_user_select.'>'.$row['user_last_name'].' '.$row['user_first_name'].'</option>';
				$i++;
				}

			$form.='</select><br />

			<!-- Notify animator -->
			<input type="checkbox" id="schedule-notice" name="schedule-notice" value="1" '.$notice.' />
			<label for="schedule-notice">'._('Notify animator').'</label>

			<!-- Place ID SELECTOR (Html) -->
			<span class="schedule-wrapper title">'._('schedule-place').'</span>
			<span class="schedule-wrapper content">
			<select id="schedule-place-select" name="schedule-place-select" required >
			<option value="">-- '._('Select').' --</option>
			';

			$i=0;
			// List users in selectbox
			while ($row = $places->fetch_assoc()) {
				($row['id']==$r['place'])?$sel_session_place_select='selected="selected"':$sel_session_place_select=NULL;
				$form.='<option value="'.$row['id'].'" '.$sel_session_place_select.'>'.$row['title'].'</option>';
				$i++;
				}

			$form.='
			</select><br />
			</span>
			
			
			<span class="schedule-wrapper title">'._('Description').'</span>
			<span class="schedule-wrapper content-editor">
				<textarea id="schedule-comment" name="schedule-comment" height="15">'.$r['comment'].'</textarea></span>
			<span class="schedule-wrapper title">'._('Image').'</span>
			';

			// Display all images available in folder
			$images = array_diff(scandir(dirname(__FILE__).'/../'.$this->CONFIG['base_folder'].'/images/'), array('.', '..'));
			//var_dump($images);
			$form_images .= '<select name="schedule-image" id="schedule-image">';
			$form_images .= '<option value="">-- '._('Select file').' --</option>';
			//var_dump($r);
			foreach ($images as $image) {
				($image==$r['image'])?$sel_image='selected="selected"':$sel_image=NULL;
				$form_images .= '<option value="'.$image.'" '.$sel_image.'>'.$image.'</option>';
				}
			$form_images .= '</select>';

			$form.='
			<span class="schedule-wrapper content">
				'.$form_images.'
			'._('or').'
			<input type="file" name="schedule-uploadimage" />
			<span class="schedule-wrapper title">'._('schedule-max-attendee').'</span>
			<span class="schedule-wrapper content"><input type="text" id="schedule-max-attendee" name="schedule-max-attendee" value="'.$r['max_attendee'].'" required /></span>
			<span class="schedule-wrapper content">
			<input type="checkbox" id="schedule-quickbook" name="schedule-quickbook" value="1" '.$quickbook.' />
			<label for="schedule-quickbook">'._('Available for quick booking').'</label>
			</span>
			<!-- USER ID SELECTOR (Html) -->
			';
			}
		return $form;
		}


	function Html_Session_Filter() {

		$sql = 'SELECT
		sessions.id as session_id,
		sessions.title as session_title,
		templates.title as template_title,
		templates.id as template_id
		FROM `sessions`
		LEFT JOIN templates on sessions.template_id=templates.id
		WHERE sessions.start >= NOW() - INTERVAL 1 DAY
		GROUP BY sessions.title
		';


		$res=$this->my->query_assoc($sql);

		// Only if query succeed
		if ($res) {
			$nb_types = $this->my->num_rows;

			// List CONFIRMED states (move this in config ?)
			$confirmed=array('CONFIRMED','PAID','FREE');
			?>
			<form id="sessions-title-filter" name="sessions-title-filter" action="?" method="POST">
			<select id="session-title-select" name="session-title-select">
			<?php

			$filter='<option value="all"><span style="color:red;">&#128194;</span> '._('Filter').' : '._('All events').' &#9660;</option>';

			$i=0;

			// Select valued stored in session if it exists
			if ($_SESSION['event_id']!=NULL) {
				$filter.='<option value="'.$_SESSION['event_id'].'" type="session" selected="selected">'._('Filter').' : '._('Event number').' '.$_SESSION['event_id'].' &#9660;</option>';
				}


			// Display Filter
			while ($row = $res->fetch_assoc()) {
				/*if ($row['session_title']==NULL) {*/
				if ($row['template_id']) {
					$type="template";
					$id=$row['template_id'];
					($row['session_title']!=NULL && $row['session_title']!="")?$title=$row['session_title']:$title=$row['template_title'];
					}
				else
					{
					$title=$row['session_title'];
					$type="session";
					$id=$row['session_id'];
					}

				if ("template-".$row['template_id']==$_SESSION['session-title-select'] && $type=="template") $sel_session_title_select='selected="selected"';
				else if ("session-".$row['session_id']==$_SESSION['session-title-select'] && $type=="session") $sel_session_title_select='selected="selected"';
				else $sel_session_title_select=NULL;
				$filter.='<option value="'.$type.'-'.$id.'" type="'.$type.'" '.$sel_session_title_select.'>'._('Filter').' : '.$title.' &#9660;</option>';
				$i++;
				}
			
			
			$filter.='</select>
			</form>';
			// Display filter
			return $filter;
			} // if $res
		}//HTML_Session_Filter
		
		
	function get_Events($sql_filter,$sql_date_filter) {
		$sql = 'SELECT

		sessions.id as session_id,
		templates.id as template_id,
		templates.title as template_title,
		templates.price as template_price,
		templates.comment as template_comment,
		templates.image as template_image,

		sessions.start as start,
		sessions.end as end,
		sessions.image as session_image,
		sessions.comment as session_comment,
		sessions.title as session_title,
		sessions.max_attendee as session_max_attendee,
		sessions.price as session_price,
		sessions.canceled as session_canceled,
		places.description as session_place,

		users_session.username as session_animator_username,
		users_session.user_last_name as session_animator_last_name,
		users_session.user_first_name as session_animator_first_name,
		users_session.user_comment as session_animator_comment


		-- users_template.user_last_name as template_animator_last_name,
		-- users_template.user_first_name as template_animator_first_name,
		-- users_template.user_comment as template_animator_comment


		FROM `sessions`

		LEFT JOIN templates ON sessions.template_id = templates.id
		LEFT JOIN users as users_session ON sessions.username=users_session.username
		LEFT JOIN users as users_template ON templates.username=users_template.username
		LEFT JOIN places on sessions.place = places.id

		WHERE 1
		'.$sql_filter.'

		'.$sql_date_filter.'
		-- AND DATE(sessions.start) > NOW() - INTERVAL 1 DAY
		ORDER BY sessions.start ASC';

		//echo $sql;
		$res=$this->my->query_assoc($sql);
		return $res;
		}// Get_Events
	}

?>
