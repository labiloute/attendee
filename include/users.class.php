<?php
// Event link
($_GET['event_id'])?$evt_link='&event_id='.$_GET['event_id']:$evt_link=NULL;
//include_once('mysql.php');
class LocalUser {

    public $CONFIG;
	public $my;
	public $ldap;
	public $infos;
	public $warnings;
	public $errors;
	public $profile;

	function __construct($CONFIG,$my,$ldap=False) {

	//Store settings
	$this->CONFIG = $CONFIG;
	$this->my = $my;

	// Enable LDAP
	if ($ldap)
		$this->ldap = $ldap;

	// Enable SSO (unhandled)
	//if ($_SERVER['REMOTE_USER'])
	//	$this->user = $_SERVER['REMOTE_USER'];

	// Logs
	$this->infos=NULL;
	$this->warnings=NULL;
	$this->errors=NULL;

	// debug
	//echo password_hash('admin',PASSWORD_DEFAULT);

	}


        //****************************/
	// Check if user already registered
        //****************************/

	function IsRegistered($userid,$field,$confirmkey,$check_last_pwd_update=True) {
		// Check last password update if requested (only for non ldap and non admins)
		($check_last_pwd_update)?$sql_check_last_pwd_update = 'AND (last_pwd_update IS NULL OR ldap = 1 OR profile=\'ADMIN\')':$sql_check_last_pwd_update='';
		$sql='SELECT * FROM users
			WHERE 1
			'.$sql_check_last_pwd_update.'
			AND '.$field.' = "'.$userid.'"
			AND confirmkey = "'.$confirmkey.'"
			LIMIT 1';

		//echo $sql;
		$res=$this->my->query_assoc($sql);
		if ($this->my->num_rows == 1) return True;
		else return False;
		}

        //****************************/
	// Bind local user DB
        //****************************/
	function CheckInternalDB($username,$password) {
		$valid=False;
		if (isset($username) && isset($password)) {
			//echo $ici;
			if(preg_match("/^[a-zA-Z0-9@.]+$/", $username) == 1) {
				$sql = 'SELECT 
				*
				FROM users
				WHERE username=\''.$username.'\'
				AND password IS NOT NULL
				AND last_pwd_update IS NOT NULL
				AND disabled IS NULL
				LIMIT 1';

				//echo $sql;
				//echo $password;
				$res=$this->my->query_assoc($sql);

				$nb_users = $this->my->num_rows;

				$r = $res->fetch_assoc();

				$hash = $r['password'];

				//echo "Password avant verification : ".$password.PHP_EOL;
				//echo "Hash avant verification : ".$hash.PHP_EOL;
				// Check password
				if (password_verify($password, $hash)) {
					$valid=True;
					$this->profile=$r['profile'];
					}
				}
			}
		// If everything else 
		return $valid;
		}
        //****************************/
	// Update Local user account with given info
        //****************************/
	function UpdateExternalId($username,$external_id) {
		$sql="	UPDATE users SET external_id='".$external_id."'
				WHERE username='".$username."' 
				LIMIT 1";
			$res=$this->my->query_simple($sql);
			if (!$res)
				$this->errors.=_("Error updating local account<br />");
		}
	function UpdateLastLogin($username) {
		$sql="	UPDATE users SET last_login=NOW()
				WHERE username='".$username."' 
				LIMIT 1";
			$res=$this->my->query_simple($sql);
			if (!$res)
				$this->errors.=_("Error refreshing last login<br />");
		}

	function UpdateLocalAccount($username,$password=NULL,$profile=NULL,$first_name=NULL,$last_name=NULL,$mail=NULL,$user_comment=NULL,$ldap=NULL,$confirmkey=NULL,$external_id=NULL,$contact_agreement=False,$password_update=True,$address=NULL,$zip=NULL,$city=NULL) {
		if (! $username) return False;
		$sql = 'SELECT 
			*
			FROM users
			WHERE username=\''.$username.'\'
			LIMIT 1';

			$res=$this->my->query_assoc($sql);
			$nb_users = $this->my->num_rows;
			$r = $res->fetch_assoc();

		// Eventually Geocode address
		$fulladdressup=$address." ".$zip." ".$city;
		$fulladdressdb=$r['address']." ".$r['zip']." ".$r['city'];
		// If address has changed (or address or coordinates were previously NULL), query Nominatim for new geocoding
		if ($city && $address && $zip && ($fulladdressdb!=$fulladdressup or $fulladdressdb="  " or (!$r['lat'] and !$r['lon']))) {
			$geo = $this->Geocode($address,$zip,$city);
			//var_dump($geo);
			($geo['lat'])?$lat=$geo['lat']:$lat=NULL;
			($geo['lon'])?$lon=$geo['lon']:$lon=NULL;
			}

		// If a user found, only update
		if ($nb_users==1) {
			// If password if set, so user ask for a password change
			($password && $password!="" && $password_update)?$sql_password=",password='".password_hash($this->my->escape_string($password),PASSWORD_DEFAULT)."'":$sql_password=NULL;
			// If password is set we reset the "Last password change" timestamp
			($password && $password!="" && $password_update)?$sql_last_pwd_update=", last_pwd_update=NOW()":$sql_last_pwd_update=NULL;
			// If profile is set, we update
			($profile &&  $profile!="")?$sql_profile=",profile='".$profile."'":$sql_profile=NULL;
			($ldap && $ldap!="")?$sql_ldap=",ldap=".$ldap."":$sql_ldap=NULL;
			($mail && $mail!="")?$sql_mail=",mail='".$mail."'":$sql_mail=NULL;
			($first_name && $first_name!="")?$sql_first_name=",user_first_name='".$this->my->escape_string($first_name)."'":$sql_first_name=NULL;
			($last_name && $last_name!="")?$sql_last_name=",user_last_name='".$this->my->escape_string($last_name)."'":$sql_last_name=NULL;
			($user_comment)?$sql_user_comment=",user_comment='".$user_comment."'":$sql_user_comment=NULL;
			($external_id)?$sql_external_id=",external_id='".$external_id."'":$sql_external_id=NULL;
			($confirmkey)?$sql_confirmkey=",confirmkey='".$confirmkey."'":$sql_confirmkey=NULL;
			($contact_agreement!==False)?$sql_contact_agreement=",contact_agreement='".$contact_agreement."'":$sql_contact_agreement=NULL;
			($address)?$sql_address=",address='".$address."'":$sql_address=NULL;
			($zip)?$sql_zip=",zip='".$zip."'":$sql_zip=NULL;
			($city)?$sql_city=",city='".$city."'":$sql_city=NULL;
			($lat)?$sql_lat=",lat='".$lat."'":$sql_lat=NULL;
			($lon)?$sql_lon=",lon='".$lon."'":$sql_lon=NULL;
			// Construct SQL
			$sql="	UPDATE users SET 
					disabled=NULL
					".$sql_ldap."
					".$sql_profile."
					".$sql_mail."
					".$sql_password."
					".$sql_first_name."
					".$sql_last_name."
					".$sql_user_comment."
					".$sql_confirmkey."
					".$sql_last_pwd_update."
					".$sql_contact_agreement."
					".$sql_lat."
					".$sql_lon."
					".$sql_address."
					".$sql_zip."
					".$sql_city."
				WHERE username='".$username."' 
				LIMIT 1";
			$res=$this->my->query_simple($sql);
			if (!$res) {
				$this->errors.=_("Error updating local account").'<br />'.$this->my->last_error.'<br />';
				return False;
				}
			elseif ($this->my->last_affected_rows>0)
				$this->infos.='<br />'._("Account has been updated").'.';
			}
		// Else create user
		else {
			($password)?$password="'".password_hash($password,PASSWORD_DEFAULT)."'":$password='NULL';
			($password && $password!='NULL')?$last_pwd_update='NOW()':$last_pwd_update='NULL';
			($first_name)?$first_name="'".$this->my->escape_string($first_name)."'":$first_name="''";
			($last_name)?$last_name="'".$this->my->escape_string($last_name)."'":$last_name="''";
			($ldap=='1' || $ldap===True)?$ldap='\'1\'':$ldap='NULL';
			($confirmkey)?$confirmkey="'".$confirmkey."'":$confirmkey='NULL';
			($external_id)?$external_id="'".$external_id."'":$external_id='NULL';
			($address)?$address="'".$address."'":$address='NULL';
			($zip)?$zip="'".$zip."'":$zip='NULL';
			($city)?$city="'".$city."'":$city='NULL';
			($lat)?$lat="'".$lat."'":$lat='NULL';
			($lon)?$lon="'".$lon."'":$lon='NULL';
			$sql="INSERT INTO users(
					username,
					user_first_name,
					user_last_name,
					mail,
					password,
					profile,
					user_comment,
					disabled,
					ldap,
					confirmkey,
					external_id,
					last_pwd_update,
					lat,
					lon,
					address,
					zip,
					city
					)
				VALUES (
					'".$username."',
					".$first_name.",
					".$last_name.",
					'".$mail."',
					".$password.",
					'".$profile."',
					NULL,
					NULL,
					".$ldap.",
					".$confirmkey.",
					".$external_id.",
					".$last_pwd_update.",
					".$lat.",
					".$lon.",
					".$address.",
					".$zip.",
					".$city."
					)
				";
			//echo $sql;
			$res=$this->my->query_simple($sql);
			if ($res)
				$this->infos.=_("Local account successfully created").'<br />';
			else {
				//-DEBUG-echo $sql;
				$this->errors.=_("Error creating local account").'<br />'.$this->my->last_error.'<br />';
				return False;
				}
			}
		}
        //****************************/
	// Bind credentials on Ldap server
        //****************************/
	function CheckLdap($username,$password,$internal) {
		//echo $username;
		$valid=False;
		if ($this->ldap && $username && $password) {
			$this->ldap->connect();
			try {
				$valid=False;
				// Get complete DN of user
				$dn=$this->ldap->getDN($username,'uid');
				// Bind user and pwd
				$bind=$this->ldap->bind($dn,$password);
				//echo "Bind : "; var_dump($bind);
				// Get First and Last name of the user
				$this->first_name=$this->ldap->getAttribute(NULL,'givenname',$dn);//First name
				$this->last_name=$this->ldap->getAttribute(NULL,'sn',$dn);// Last name
				$this->mail=$this->ldap->getAttribute(NULL,'mail',$dn);// Get email address
				//$this->auth_attributes=$this->ldap->getAttribute(NULL,$this->CONFIG['ldap_auth_attribute'],$dn,True);// Get value of auth attribute (config)

				if ($this->CONFIG['ldap_auth_attribute']!='' and $this->CONFIG['ldap_auth_attribute'])
                                        $this->auth_attributes=$this->ldap->getAttribute(NULL,$this->CONFIG['ldap_auth_attribute'],$dn,True);// Get value of auth attribute (config)

				// Eventually convert it to an array if not already given as array
				if (!is_array($this->auth_attributes)) {$this->auth_attributes=array($this->auth_attributes);}

                                // Get members of mapped groups
                                $memb_ADMIN=$this->ldap->getMembers($this->CONFIG['ldap_map_ADMIN']);
                                $memb_MANAGER=$this->ldap->getMembers($this->CONFIG['ldap_map_MANAGER']);
                                $memb_ANIMATOR=$this->ldap->getMembers($this->CONFIG['ldap_map_ANIMATOR']);
                                if ($dn && $bind) {
                                        // Lookup in custom attribute  (Role Based Access Control : https://ldapwiki.com/wiki/RBAC)
                                        if (sizeof($this->auth_attributes)>0) {
                                                foreach ($this->auth_attributes as $auth_attribute) {
                                                        //echo $auth_attribute;
                                                        if (strpos($auth_attribute,$this->CONFIG['dbname'].':')!==False)
                                                                {
                                                                //debug-echo "Trouve $auth_attribute dans $auth_attribute --";
                                                                $auth_attribute_role=str_replace($this->CONFIG['dbname'].':','',$auth_attribute);
                                                                //echo $auth_attribute_role;
                                                                }
                                                        }
                                                }
                                        // Compare group membership to select good profile
                                        if (in_array($username,$memb_ADMIN) or $auth_attribute_role=='ADMIN'){$valid=True;$this->profile='ADMIN';}
                                        else if (in_array($username,$memb_MANAGER) or $auth_attribute_role=='MANAGER'){$valid=True;$this->profile='MANAGER';}
                                        else if (in_array($username,$memb_ANIMATOR) or $auth_attribute_role=='ANIMATOR'){$valid=True;$this->profile='ANIMATOR';}
                                        else echo "[ERROR] User has no ldap group OR has no custom ldap attribute set !";
                                        // If both internal and ldap auth worked, no need to update local password because they are the same for sure !
                                        if ($internal) $update_password=False;
                                        // If everything worked, create or update local user and password (mandatory for user info and so on...)
                                        $this->UpdateLocalAccount($username,$password,$this->profile,$this->first_name,$this->last_name,$this->mail,NULL,'1',NULL,NULL,False,$update_password);
				}
			}
			catch (Exception $e) {
				echo "Error with LDAP";
			}
		}
		return $valid;
	}
        //****************************/
	// Main Auth Method
        //****************************/
	function Authenticate($username,$password) {
		if ($username && $password) {
			// Init logs
			$infos=NULL;
			$warnings=NULL;
			$errors=NULL;
			//echo "Password avant authentification : ".$password.PHP_EOL;
			$internal=$this->CheckInternalDB($username,$password);
			$ldap=$this->CheckLdap($username,$password,$internal);
			if ($internal || $ldap) {
				$this->infos.='<br />'._('Authentication successfull');
				$_SESSION['username']=$username;
				$_SESSION['profile']=$this->profile;
				$this->UpdateLastLogin($username);
				return True;
				}
			else {
				$this->warnings.=_('Invalid authentication');
				unset($_SESSION);
				session_destroy();
				return False;
				}
			}
		}

	//*****************************/
	// GLOBAL USER INFO
	//****************************/

	function GetUserInfo($userid,$field='users.username',$disabled=False) {
		//if (! $field) $field='users.username';
		($disabled)?$sql_disabled='':$sql_disabled='AND DISABLED IS NULL';
		$sql = '
		SELECT * FROM users
		WHERE '.$field.'="'.$userid.'" 
		'.$sql_disabled.' 
		LIMIT 1';

		$res=$this->my->query_assoc($sql);
		if ($this->my->num_rows == 0) return False;
		//var_dump($this->my->num_rows);
		$r = $res->fetch_assoc();
		//var_dump($r);
		return $r;
		}



	//*****************************/
	// USER SKILLS
	//****************************/
	function GetSkillsForm($id=NULL,$page=NULL,$selectedskills=NULL,$selectedrqsts=NULL) {
		$res=$this->GetSkills($id);
		$nb=$this->my->num_rows;
		if ($nb>0) {
			$html.='<form id="skillsform" method="POST" action="?page='.$page.'#attendee-tabs-4">';
			$html.='<div class="attendee-skills-buttons"><input type="submit" name="save-skills" value="'._('Save').'" /></div>';
			$html.='<table class="global-stats-table" id="attendee-skills">
			<tr>
				<th>'._('Starred').'</th>
				<th>'._('Skills').'</th>
				<th>'._('I can help').'</th>
				<th>'._('I need help').'</th>
			</tr>
			';
			while ($s=$res->fetch_assoc()) {
				if (file_exists($this->CONFIG['base_folder'].'/images/star-'.$s['starred'].'.png')) $img=$this->CONFIG['base_folder'].'/images/star-'.$s['starred'].'.png';
				else if (file_exists($this->CONFIG['base_folder'].'/images/star.png')) $img=$this->CONFIG['base_folder'].'/images/star.png';
				else if (file_exists('images/star-'.$s['starred'].'.png')) $img='images/star-'.$s['starred'].'.png';
				else $img='images/star.png';
				if ($s['starred']) {
					$starred_img='<img src="'.$img.'" height="30" />';
					}
				else $starred_img='';
				($selectedskills && in_array($s['id'],$selectedskills))?$ckd='checked="checked"':$ckd="";
				($selectedrqsts && in_array($s['id'],$selectedrqsts))?$ckd_rqst='checked="checked"':$ckd_rqst="";

				//var_dump($selectedrqsts);
				if ($s['cat'] != $previous_cat) {
					$html.='<tr ><td class="cat" colspan="4" >'.$s['cat'].'</td></tr>';
					}
				$html.='
				<tr>
				<td>'.$starred_img.'</td>
				<td>'.$s['title'].'</td>
				<td><input type="checkbox" name="skills[]" '.$ckd.' value="'.$s['id'].'"/></td>
				<td><input type="checkbox" name="trainingrqst[]" '.$ckd_rqst.' value="'.$s['id'].'"/></td>
				</tr>';
				$previous_cat=$s['cat'];
				}
			$html.='</table>';
			$html.='<div class="attendee-skills-buttons"><input type="submit" name="save-skills" value="'._('Save').'" /></div>';
			$html.='</form>';
			}
		else $html=_('No result');
		return $html;
		}

	function GetSkills($id=NULL,$disabled=False,$searchterm=False,$starred=False) {
		if ($starred) {
			$sql_starred='AND starred!=\'0\'';
			$order_by='ORDER BY starred';
			}
		else {
			$sql_starred=NULL;
			$order_by='order BY cat, subcat, title';
			}
		// If id given is an array, construct query with all Ids
		if (is_array($id)) {
			$i=0;
			foreach ($id as $idelem) {
				($i>0)?$sql_filter_by_id.=' OR':$sql_filter_by_id.=' AND (';
				$sql_filter_by_id.=' id=\''.$idelem.'\'';
				$i++;
				}
			$sql_filter_by_id.=')';
			}
		// Or just a query with an ID
		elseif ($id) $sql_filter_by_id='AND id=\''.$id.'\'';
		// Display or not disabled items
		($disabled)?$sql_disabled="AND disabled=1":$sql_disabled="AND disabled=0";
		// Or eventually search by terms
		($searchterm)?$sql_searchterm=' AND LOWER(title) LIKE LOWER("%'.$searchterm.'%")':$searchterm="";
		$sql = 'SELECT * from skills
		WHERE 1
		'.$sql_disabled.'
		'.$sql_filter_by_id.'
		'.$sql_searchterm.'
		'.$sql_starred.'
		'.$order_by.'
		LIMIT 1000';
		//
		//-DEBUG-echo $sql;
		$q=$this->my->query_array($sql);
		//$r=$q->fetch_assoc();
		return $q;
		}

	function GetUserSkills($userid) {
		if ( ! $userid) return False;
		$sql = 'SELECT * from users
		WHERE username="'.$userid.'"
		LIMIT 1';
		$q=$this->my->query_array($sql);
		return $q;
		}


	function GetUserSkillsLabel($userid,$starred=False) {
		if ( ! $userid) return False;
		$skills=$this->GetUserSkills($userid);
		$sql_skills = $skills->fetch_array();
		// Extact skills from query result
		$skills= $sql_skills['skillsid'];
		$skills = explode(';',$skills);
		$skills = $this->GetSkills($skills,False,False,$starred);
		return $skills;
		}

	function GetAllSkills() {
		$sql = 'SELECT skillsid,trainingrqstid from users
		WHERE 1
		AND disabled IS NULL
		LIMIT 10000';
		$q=$this->my->query_array($sql);
		return $q;
		}

	function GetUserHavingSkills($skills=False,$check_agreement=True,$check_geo=False) {
		// Filter on skills if requested
		if ($skills) {
			$i=0;
			foreach ($skills as $skill) {
				($i>0)?$or=' OR ':$or='AND (';
				$sql_skills.=' '.$or.' skillsid LIKE "%;'.$skill.';%" '.PHP_EOL;
				$i++;
				}
				$sql_skills.=')';
			}
		// Filter on agreement
		($check_agreement)?$sql_agreement='AND contact_agreement=\'1\'':$check_agreement=False;

		// Filter on geo data if requested
		($check_geo)?$sql_geo="AND lat IS NOT NULL AND lat != '' AND lon IS NOT NULL AND lon != '' ":$sql_geo='';

		// Construct query
		$sql = 'SELECT *
			FROM users
			WHERE 1
			AND disabled IS NULL
			'.$sql_skills.'
			'.$sql_geo.'
			'.$sql_agreement.'';
		//echo nl2br($sql);
		$q=$this->my->query_array($sql);
		return $q;
		}

	function SaveSkillsHtml($userid,$skills,$trainingrqsts) {

		if ( ! $skills or count($skills)==0) $sql_skills='';
		else {
			$sql_skills=";";
			foreach ($skills as $skill) {
				$sql_skills.=$skill.';';
				}
			}

		//var_dump($trainingrqsts);

		if ( ! $trainingrqsts or count($trainingrqsts)==0) $sql_trainingrqst = '';
		else {
			$sql_trainingrqst=";";
			foreach ($trainingrqsts as $trainingrqst) {
				$sql_trainingrqst.=$trainingrqst.';';
				}
			}

		$sql='UPDATE users SET skillsid="'.$sql_skills.'", trainingrqstid="'.$sql_trainingrqst.'" WHERE username="'.$userid.'"';
		//echo $sql;
		$q=$this->my->query_simple($sql);

		if (! $q->errors)
			$this->infos.=_('Profile sucessfully updated');
		else
			$this->errors.=_('Error updating profile : '.$auth->errors);

		}

	function HtmlSearchForm($page=False,$skills=False) {
		if (! $page) $page=$_GET['page'];
		//var_dump($skills);
		if ($skills) {
			$fullskills=$this->GetSkills($skills);
			while ($row = $fullskills->fetch_array()) {
				$selectedskills.='<div title="'.$row['title'].'" class="selectedskill" id="skill-'.$row['id'].'">';
				$selectedskills.='<span class="skill-remove" remid="skill-'.$row['id'].'">';
				$selectedskills.='		&#10060;';
				$selectedskills.='<input class="selected-skillid" name="searchedskills[]" value="'.$row['id'].'"  />';
				$selectedskills.='		'.$row['title'].'';
				$selectedskills.='</div>';
				}
			}
		//var_dump($_POST);
		$html = '<form id="search-user" method="POST" action="?page='.$page.'">
				<input type="text" class="search-user-input autocompletefield" id="autocomplete-skills" placeholder="'._('Search for a skill').'" />
				<input type="submit" class="search-user-submit" value="'._('Search').'" />
				<input type="submit" class="search-user-submit" name="fill-with-requests" value="'._('My requests').'" />
				<div id="selected-skills-id-list">'.$selectedskills.'</div>
			</form>';
		return $html;
		}
	/*
	function HtmlStarredCompetencies($user) {
		$c_starred=$this->GetUserSkills($userid=$userid,$starred=True);
		$a_skills=explode(";",$c_starred['skillsid']);
		var_dump($a_skills);
		}
	*/

        //****************************/
        // Extract real IP in our prefered order
        //****************************/
	function getClientIP_env() {
        /*
         if ($_GET['debug']) {
                echo "HTTP_CLIENT_IP : ".getenv('HTTP_CLIENT_IP').'<br />';
                echo "HTTP_X_FORWARDED_FOR : ".getenv('HTTP_X_FORWARDED_FOR').'<br />';
                echo "HTTP_X_FORWARDED : ".getenv('HTTP_X_FORWARDED').'<br />';
                echo "HTTP_FORWARDED_FOR : ".getenv('HTTP_FORWARDED_FOR').'<br />';
                echo "HTTP_FORWARDED : ".getenv('HTTP_FORWARDED').'<br />';
                echo "REMOTE_ADDR :".getenv('REMOTE_ADDR').'<br />';
                }
        */
        if (getenv('HTTP_CLIENT_IP'))
                $ip  = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR')) {
                $ip  = getenv('HTTP_X_FORWARDED_FOR');
        	if (strpos($ip,',')) { $ip=explode(',',$ip)[0];}
		}
        else if(getenv('HTTP_X_FORWARDED'))
                $ip  = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
                $ip  = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
                $ip  = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
                $ip  = getenv('REMOTE_ADDR');
        else
                return False;
        return $ip;
        }

        //****************************/
        // Geocoding
        //****************************/
	function Geocode($address,$zip,$city) {
		$result=False;
		$url = $this->CONFIG['nominatim_url'];

		// Adresses
		$fulladdresses[] = $address.' '.$zip.' '.$city;

		// Géocodage
		foreach($fulladdresses as $fulladdress)
			{
			$USERAGENT = $_SERVER['HTTP_USER_AGENT'];
    			$opts = array('http'=>array('header'=>"User-Agent: $USERAGENT\r\n"));
			$context = stream_context_create($opts);
			//echo "Recherche des coordonnées pour ".$fulladdress.'<br />';
			$params = http_build_query(array('q' => $fulladdress, 'format' => 'json'));
			//$params = http_build_query(array('q' => 'Louvigny', 'format' => 'json'));
			//$params = http_build_query(array('street' => $street, 'postalcode'=> $postal, 'city'=> $city, 'format' => 'json'));
			//echo $url.$params;
			$appel_api = file_get_contents($url.$params,false,$context);
			//$appel_api = file_get_contents('https://nominatim.openstreetmap.org/search?q=Louvigny&format=json',false,$context);
			//var_dump($appel_api);
			$resultats = json_decode($appel_api);
			
			if ($resultats[0]->lat && $resultats[0]->lon)
				$result=array('lat'=>$resultats[0]->lat,'lon'=>$resultats[0]->lon);

			//$infos=var_export($resultats[0], true);
			
			//echo $infos['lat'];
			//echo $infos['lon'];
			}
		return $result;
		}


	function GetGeoInfo($skills=False) {
		// First Get All Skills and put them into an array
		$AllSkills=$this->GetSkills();
		// Init array to store skills
		$Skills=[];
		while($s=$AllSkills->fetch_array()) {
			$sid=$s['id'];
			//echo $s['title'];
			$Skills[$sid]=$s['title'];
			}

		// Get All users
		$sql="SELECT
		username
		,mail
		,user_first_name
		,user_last_name
		,lat
		,lon
		,address
		,zip
		,city
		,skillsid
		,trainingrqstid
		FROM users
		WHERE 1
		AND disabled IS NULL
		AND lat IS NOT NULL
		AND lat != ''
		AND lon IS NOT NULL
		AND lon != ''
		AND contact_agreement='1'
		LIMIT 10000";
		
		$usershavingskills=$this->GetUserHavingSkills($skills,True,True);

		//echo $sql;
		$table=[];
		//$res=$this->my->query_array($sql);
		while($r=$usershavingskills->fetch_array()) {
			// Extract Skills
			//var_dump($r['skillsid']);
			$a_skills=explode(";",$r['skillsid']);
			$a_training=explode(";",$r['trainingrqstid']);
			$a_skills = array_filter($a_skills);
			$a_training = array_filter($a_training);              
			//var_dump($a_training);
			$name=$r['user_first_name']." ".$r['user_last_name'];
			$mappointlabel=$name."<br />";
			$mappointlabel.='<strong>'._('Contact').'</strong>: <a href="mailto:'.$r['mail'].'">'.$r['mail'].'</a>';
			
			if (count($a_skills)>0) {
				$mappointlabel.='<br /><br /><strong>'._('Can help').'</strong><br />';
				foreach ($a_skills as $s) {
					$mappointlabel.=$Skills[$s].'<br />';
					}
				}
			if (count($a_training)>0) {
				$mappointlabel.='<br /><strong>'._('Request help').'</strong><br />';
				foreach ($a_training as $t) {
					$mappointlabel.=$Skills[$t].'<br />';
					}
				}
			$username=$r['username'];
			$table[$username]=array("lat"=>$r['lat'],"lon"=>$r["lon"],"label"=>$mappointlabel);
			}
		return json_encode($table);
		}

        //****************************/
        // Check if local IP match the config
        //****************************/
        function CheckAllowedNetworks() {
                //$remoteip = $_SERVER['REMOTE_ADDR'];
                $remoteip = $this->getClientIP_env();

                //echo $remoteip;
                foreach ($this->CONFIG['newmember-networks'] as $network) {
                        if (strpos($remoteip, $network) !== false) {
                                return True;
                                }
                        }
                return False;
                }

	function GetLoginButton($logout=False) {
		$html='<a class="nav-link login-window" href="#login-box" title="'._('Click here to login').'">&#9094;<span class="sr-only">(current)</span></a>';
		if ($logout && $_SESSION['username'])
			$html.='<a href="?logout=logout'.$evt_link.'" class="nav-link logout-window" title="'._('Click here to logout').'">&#9099;</a>';
		return $html;
		}
}
//End of class
?>
