<?php
// ***
// Notes ! The locale need to exists on your system (web server) : check echo system('locale -a') if required;
// ***

// enable verbose mode if not set to false
if (!isset($verbose)) $verbose = True;
if (!isset($localepath)) $localepath="locale";

$codeset = "utf8";  // warning ! not UTF-8 with dash '-' 

//$locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
//var_dump($_SERVER['HTTP_ACCEPT_LANGUAGE']);
$locale = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0]; # get Browser Locale
if ($CONFIG['force_locale']) {$locale = $CONFIG['force_locale'];}

// Eventually Convert it to unix-like system locale (convert fr-FR to fr_FR)
if (strpos($locale,"-")) {
   $splitted_locale = explode("-",$locale);
   $locale = $splitted_locale[0]."_".$splitted_locale[1];
}
// Eventually repeat if given locale is a short one (convert fr to fr_FR)
if (strlen($locale)==2) {
   $locale = strtolower($locale)."_".strtoupper($locale);
}

// Eventually add codeset if not given
if (! strpos($locale,".")) {
   $locale = $locale.".".$codeset;
}

//echo $locale;

$r = putenv("LC_ALL=".$locale);
$r2 = putenv("LANGUAGE=".$locale);
if (( ! $r or ! $r2) && $verbose) {echo "Failed one of the putenv commands".PHP_EOL;}
//$r = setlocale(LC_TIME, "");
//if (!$r && $verbose) {echo "Failed setlocale LC_TIME".PHP_EOL;}
$r = setlocale(LC_ALL, $locale.'.'.$codeset);
if (!$r && $verbose) {echo "Failed setlocale on LC_ALL".PHP_EOL;}
if (!$r) $r = setlocale(LC_ALL, 'fr_FR.'.$codeset); # If it still fail force french because it is my native language ;)
if (!$r && $verbose) {echo "Fallback to french failed".PHP_EOL;}
if (!$r) $r = setlocale(LC_ALL, 'en_US.'.$codeset); # If everything fail force US english

//echo "<br />";
//echo setlocale(LC_ALL, '0');
if (!$r && $verbose) {echo "Failed setlocale".PHP_EOL;}
$domain="messages";
//bindtextdomain($domain, 'locale/nocache');
$r = bindtextdomain($domain, $localepath);
if (!$r && $verbose) {echo "Failed bindtext".PHP_EOL;}
$r = bind_textdomain_codeset($domain, 'UTF-8');
if (!$r && $verbose) {echo "Failed codeset".PHP_EOL;}
$r=textdomain($domain);
if (!$r && $verbose) {echo "Failed textdomain".PHP_EOL;}
?>
