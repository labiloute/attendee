<?php
class Attendee {
	function __construct($CONFIG,$my) {

	//Store settings
	$this->CONFIG = $CONFIG;
	$this->my = $my;
	$this->my->query_simple("SET NAMES 'utf8'");

	// Logs
	$this->infos=NULL;
	$this->warnings=NULL;
	$this->errors=NULL;
	}

        //*****************************/
        // MANAGE ATTENDEE STATE
        //****************************/

        function ChangeState($attendee_id,$state,$key=NULL,$fromstate=NULL) {
                // $my;
                ($key)?$sql_key=' AND confirmkey=\''.$key.'\' ':$sql_key=NULL;
                ($fromstate)?$sql_state = ' AND state=\''.$fromstate.'\' ':$sql_state=NULL;
                $sql = 'UPDATE attendees SET state=\''.$state.'\' WHERE id=\''.$attendee_id.'\' '.$sql_key.' '.$sql_state.' LIMIT 1';
                //echo $sql;
                $this->my->query_simple($sql);
                $ret=$this->my->last_affected_rows;
                //echo "<br />Nombre de ligne trouvées : ".$return;
                return $ret;
                }


        function GetState($attendee_id) {
                $sql = 'SELECT state FROM attendees WHERE id=\''.$attendee_id.'\' LIMIT 1';
                //echo $sql;
                //$this->my->query_simple($sql);
                $q=$this->my->query_array($sql);
                $ret=$q->fetch_array();
                //$ret=$this->my->last_affected_rows;
                //echo "<br />Nombre de ligne trouvées : ".$return;
                return $ret['state'];
                }

	//*****************************/
	// MANAGE SKILLS
	//****************************/
	function GetSkillsForm($id=NULL,$page=NULL,$selectedskills=NULL) {
		$res=$this->GetSkills($id);
		$nb=$this->my->num_rows;
		if ($nb>0) {
			$html.='<form id="skillsform" method="POST" action="?page='.$page.'">';
			$html.='<table class="global-stats-table" id="attendee-skills">
			<tr>
				<th>'._('Compétence').'</th>
				<!---<th>'._('Je cherche de l\'aide').'</th>-->
				<th>'._('Je peux aider').'</th>
			</tr>
			';
			while ($s=$res->fetch_assoc()) {
				($selectedskills && in_array($s['id'],$selectedskills))?$ckd='checked="checked"':$ckd="";
				if ($s['cat'] != $previous_cat) {
					$html.='<tr ><td class="cat" colspan="2" >'.$s['cat'].'</td></tr>';
					}
				$html.='
				<tr>
				<td>'.$s['title'].'</td>
				<!--<td><input type="checkbox" /></td>-->
				<td><input type="checkbox" name="skills[]" '.$ckd.' value="'.$s['id'].'"/></td>
				</tr>';
			$previous_cat=$s['cat'];
				}
			$html.='</table>';
			$html.='<div class="attendee-skills-buttons"><input type="submit" value="'._('Save').'" /></div>';
			$html.='</form>';
			}
		else $html='Aucun résultat';
		return $html;
		}

	function GetSkills($id=NULL,$disabled=False,$searchterm=False) {
		// If id given is an array, construct query with all Ids
		if (is_array($id)) {
			$i=0;
			foreach ($id as $idelem) {
				($i>0)?$sql_filter_by_id.='OR':$sql_filter_by_id.='AND (';
				$sql_filter_by_id.=' id=\''.$idelem.'\'';
				$i++;
				}
			$sql_filter_by_id.=')';
			}
		// Or just a query with an ID
		elseif ($id) $sql_filter_by_id='AND id=\''.$id.'\'';
		// Display or not disabled items
		($disabled)?$sql_disabled="AND disabled=1":$sql_disabled="AND disabled=0";
		// Or eventually search by terms
		($searchterm)?$sql_searchterm=' AND LOWER(title) LIKE LOWER("%'.$searchterm.'%")':$searchterm="";
		$sql = 'SELECT * from skills
		WHERE 1
		'.$sql_disabled.'
		'.$sql_filter_by_id.'
		'.$sql_searchterm.'
		order BY cat, subcat, title
		LIMIT 1000';
		//return $sql;
		//echo $sql;
		$q=$this->my->query_array($sql);
		//$r=$q->fetch_assoc();
		return $q;
		}

	function GetUserSkills($userid) {
		if ( ! $userid) return False;
		$sql = 'SELECT * from userskills
		WHERE id="'.$userid.'"
		LIMIT 1';
		$q=$this->my->query_array($sql);
		return $q;
		}

	function GetUserHavingSkills($skills) {
		// Skills should be an array id skills ID
		$i=0;
		foreach ($skills as $skill) {
			($i>0)?$or=' OR ':$or='';
			$sql_filter.=' '.$or.' skillsid LIKE "%;'.$skill.';%" '.PHP_EOL;
			$i++;
			}
		//echo $sql_filter;

		$sql = 'SELECT *
			FROM userskills
			WHERE 1
			AND (
			'.$sql_filter.'
			)';
		//echo nl2br($sql);
		$q=$this->my->query_array($sql);
		return $q;
		}

	function SaveSkillsHtml($user,$skills) {

		if ( ! $skills or count($skills)==0) $sql_skills='';
		else {
			$sql_skills=";";
			foreach ($skills as $skill) {
				$sql_skills.=$skill.';';
				}
			}
		//var_dump($sql_skills);

		$sql='INSERT INTO userskills(id, skillsid) VALUES("'.$user.'", "'.$sql_skills.'") 
			ON DUPLICATE KEY UPDATE skillsid = "'.$sql_skills.'";';
		$q=$this->my->query_simple($sql);
	}

	function HtmlSearchForm($page=False,$skills=False) {
		if (! $page) $page=$_GET['page'];
		//var_dump($skills);
		if ($skills) {
			$fullskills=$this->GetSkills($skills);
			while ($row = $fullskills->fetch_array()) {
				$selectedskills.='<div title="'.$row['title'].'" class="selectedskill" id="skill-'.$row['id'].'">';
				$selectedskills.='<span class="skill-remove" remid="skill-'.$row['id'].'">';
				$selectedskills.='		&#10060;';
				$selectedskills.='<input class="selected-skillid" name="skills[]" value="'.$row['id'].'"/>';
				$selectedskills.='		'.$row['title'].'';
				$selectedskills.='</div>';
				}
			}
		$html = '<form id="search-user" method="POST" action="?page='.$page.'">
				<input type="text" class="search-user-input autocompletefield" id="autocomplete-skills" />
				<input type="submit" class="search-user-submit" value="'._('Search').'"/>
				<div id="selected-skills-id-list">'.$selectedskills.'</div>
			</form>';
		return $html;
		}

}# End of class
?>
