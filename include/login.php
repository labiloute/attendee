<?php
// Authenticate
$success=$auth->Authenticate(strtolower($_POST['username'] ?? ''),$_POST['password']);

//var_dump($success);
//******************/
// Get user info
//******************/

//if ($_GET['userid']) $userid = $_GET['userid'];
if ($_POST['username']) $userid = $_POST['username'];
else if ($_SESSION['username']) $userid = $_SESSION['username'];
else $userid=False;
if ($userid) {
	$userinfo = $auth->GetUserInfo($userid);
	$useremail = $userinfo['mail'];
	$external_id = $userinfo['external_id'];
	}

// This code is executed if authentication succeed, Dolibarr is active, profile is NON-ADMIN, and auth require a valid subscription (ldap or classic)
if ($success && $Dolibarr && $userinfo['profile']!='ADMIN' &&
	(
		($CONFIG['dolibarr']['login_ldap_require_valid_sub']&& $userinfo['ldap']) 
		or ($CONFIG['dolibarr']['login_require_valid_sub'] && ! $userinfo['ldap'])
	)
	) {
	
	$DolibarrInfo = $Dolibarr->GetMemberInfo($userinfo['mail']);
	$endofsubscription=$DolibarrInfo['last_subscription_date_end'];
	//var_dump($endofsubscription);
	// If subscription is not valid anymore, propose a link to the renewal page
	if (!$endofsubscription or ($endofsubscription && ! $Dolibarr->is_valid($endofsubscription))) {
		($CONFIG['renew_url'])?$renew_url = $CONFIG['renew_url']:$renew_url='?page=newmember';
		$success=False;
		$errors.=_('Your subscription is not valid');
		$errors.='<br />';
		$errors.=sprintf(_("To keep benefit of organisation, please renew your subscription"),$CONFIG['website-name'],$renew_url);;
		$_GET['logout']='logout';
		$auth->infos=NULL;
		}
	}

// This code update local account at login
if ($success && $Dolibarr) {
	// Get DolibarrInfo if not already gathered
	if (!$DolibarrInfo) $DolibarrInfo = $Dolibarr->GetMemberInfo($userinfo['mail']);
	//var_dump($DolibarrInfo);
	//var_dump($userinfo);
	// Eventually Update local account with essential information


	// Only request update for field that changed
	($DolibarrInfo['firstname']!=$userinfo['user_first_name'])?$first_name=$DolibarrInfo['firstname']:$first_name=NULL;
	($DolibarrInfo['lastname']!=$userinfo['user_last_name'])?$last_name=$DolibarrInfo['lastname']:$last_name=NULL;
	($DolibarrInfo['town']!=$userinfo['city'])?$city = $DolibarrInfo['town']:$city=NULL;
	($DolibarrInfo['address']!=$userinfo['address'])?$address = $DolibarrInfo['address']:$address=NULL;
	($DolibarrInfo['zip']!=$userinfo['zip'])?$zip = $DolibarrInfo['zip']:$zip=NULL;
	($DolibarrInfo['email']!=$userinfo['mail'])?$mail = $DolibarrInfo['email']:$mail=NULL;
	($DolibarrInfo['id']!=$userinfo['external_id'])?$dolibarrid = $DolibarrInfo['id']:$dolibarrid=NULL;

	// request update of local account
	$auth->UpdateLocalAccount($userid,NULL,NULL,$first_name,$last_name,$mail,NULL,NULL,NULL,$dolibarrid,False,NULL,$address,$zip,$city);
	}

// Get debug from Auth class
$infos.=$auth->infos;
$warnings.=$auth->warnings;
$errors.=$auth->errors;


//******************/
// Logout
//******************/
if (isset($_GET['logout'])) {
	unset($_SESSION);
	session_destroy ();
	}
	//echo $_SERVER['REMOTE_ADDR'];
?>



<?php ($_GET['event_id'])?$evt_link='&event_id='.$_GET['event_id']:$evt_link=NULL; ?>

<div id="login-box" class="login-popup">
<a href="#" class="close icono-cross"></a>
  <form method="post" class="signin" action="?login=login<?php echo $evt_link; ?>">
        <fieldset class="textbox">
        <label class="username">
        <span><?php echo _('Username') ?></span>
        <input id="username" name="username" value="" type="text" autocomplete="on" placeholder="<?php echo _('Username') ?>">
        </label>
        <label class="password">
        <span><?php echo _('Password') ?></span>
        <input id="password" name="password" value="" type="password" placeholder="<?php echo _('Password') ?>">
        </label>
        <button class="submit button" type="submit"><?php echo _('Sign in') ?></button>
        <p>
        <!--<a class="forgot" href="#">Forgot your password?</a>-->
        </p>
        </fieldset>
  </form>
</div>

