<?php

class Letters {

	public $CONFIG;
	public $my;
	public $infos;
	public $warnings;
	public $errors;
	public $inserted_addresses;
	public $inserted_links;
	public $disabled_addresses;
	public $already_disabled_addresses;
	public $unknown_addresses;

	function __construct($CONFIG,$my) {

	//Store settings
	$this->CONFIG = $CONFIG;
	$this->my = $my;
	// Logs
	$this->infos=NULL;
	$this->warnings=NULL;
	$this->errors=NULL;

	}


	function GetAddressId($address) {
		$sql='SELECT * FROM letters_addresses WHERE address=\''.$this->my->escape_string($address).'\' LIMIT 1';
		$q=$this->my->query_array($sql);
		if ($this->my->num_rows==0) return False;
		else return $q;
		}

	function GetAddress($address_id) {
		$sql='SELECT * FROM letters_addresses WHERE address_id=\''.$this->my->escape_string($address_id).'\' LIMIT 1';
		$q=$this->my->query_array($sql);
		if ($this->my->num_rows==0) return False;
		else return $q;
		}

	function GetAddresses($disabled=True) {
		if ($disabled===False) $sql_disabled="WHERE la.disabled != 1";
		$sql='SELECT * FROM letters_addresses la '.$sql_disabled.' ORDER BY la.disabled ASC LIMIT 10000';
		$q=$this->my->query_array($sql);
		if ($this->my->num_rows==0) return False;
		else return $q;
		}
		
	function GetLink($gid,$address_id) {
		if (!$gid or !$address_id) return False;
		$gid=(int) $gid;
		$address_id=(int) $address_id;
		$sql='SELECT * FROM `letters_groups_to_users` WHERE address_id='.$this->my->escape_string($address_id).' AND group_id='.$this->my->escape_string($gid).' LIMIT 1';
		$q=$this->my->query_array($sql);
		if ($this->my->num_rows==0) return False;
		else return $q;
		}

	function GetLinks($gid) {
		if (!$gid) return False;
		$gid=(int) $gid;
		$sql='SELECT * FROM `letters_groups_to_users` WHERE group_id='.$this->my->escape_string($gid).'';
		$q=$this->my->query_array($sql);
		$this->num_rows=$this->my->num_rows;
		if ($this->my->num_rows==0) return False;
		else return $q;
		}

	function GetLetters($id=False,$code_id=False) {
		if ($id) {$id=(int) $id;$letters_content=',letter_content';$sql_content='WHERE letter_id='.$this->my->escape_string($id).'';}
		if ($code_id) {$letters_content=',letter_content';$sql_code='WHERE letter_code=\''.$this->my->escape_string($code_id).'\'';}
		$sql = 'SELECT letter_id,letter_title,letter_width,letter_bgcolor,letter_pagecolor,letter_code,letter_enabled '.$letters_content.' from letters_letters
		'.$sql_content.'
		'.$sql_code.'
		LIMIT 1000';
		//echo $sql;
		$q=$this->my->query_array($sql);
		return $q;
		}

	function GetLetter($id=False,$code_id=False) {
		if (!$id && !$code_id) return False;
		$id=(int) $id;
		//$code_id=(int) $code_id;
		$letter=$this->GetLetters($id,$code_id);
		$row = $letter->fetch_assoc();
		return $row;
		}

	function GetLetterContent($id=False) {
		if (! $id) return False;
		$id=(int) $id;
		$letter=$this->GetLetters($id);
		while ($row = $letter->fetch_assoc()) {
			$result=$row['letter_content'];
			}
		return $result;
		}

	function GetGroups($id=False) {
		if ($id) {$id=(int) $id;$sql_content='WHERE group_id='.$this->my->escape_string($id).'';}
		$sql = 'SELECT lg.group_id, lg.group_name
		FROM letters_groups lg
		'.$sql_content.'
		LIMIT 1000';
		//echo $sql;
		$q=$this->my->query_array($sql);
		return $q;
		//return $result;
		}

	function RenameGroup($id,$newname) {
		if (!$id) return False;
		$id=(int) $id;
		$sql = 'UPDATE letters_groups lg
		SET group_name=\''.$this->my->escape_string($newname).'\'
		WHERE lg.group_id=\''.$this->my->escape_string($id).'\'
		LIMIT 1';
		//echo $sql;
		$q=$this->my->query_array($sql);
		if ($q) return False;
		else return True;
		//return $result;
		}

	function GetGroupId($gname) {
		if (!$gname) return False;
		$sql = 'SELECT lg.group_id, lg.group_name
		FROM letters_groups lg
		WHERE lg.group_name=\''.$this->my->escape_string($gname).'\'
		LIMIT 1';
		$q=$this->my->query_array($sql);
		return $q;
		}

	function GetLettersToGroups($id) {
		if (!$id) return False;
		$id=(int) $id;
		$sql = 'SELECT group_id
		FROM letters_to_groups ltg
		WHERE letter_id='.$this->my->escape_string($id).'
		LIMIT 1000';
		//echo $sql;
		$q=$this->my->query_array($sql);
		return $q;
		//return $result;
		}

	// Shipping are not duplicated if an address belongs to multiple groups
	function GetShippings($letter_id=False,$shipping_status=False,$letter_enabled=True,$limit=False) {
		// Default limit
		(!$limit)?$this->limit=20:$this->limit=$limit;

		// Protect fields
		$letter_id=$this->my->escape_string($letter_id);

		// Filter on letter id
		($letter_id)?$sql_id="AND ll.letter_id='".$letter_id."'":$sql_id="";
		
		// Shipping status
		//var_dump($shipping_status);
		if ($shipping_status==1) $sql_status="AND ls.status=1";
		elseif ($shipping_status=='NULL') $sql_status="AND ls.status IS NULL";
		elseif ($shipping_status==0) $sql_status="AND ls.status=0";
		else $sql_status="";
		
		// Letter status
		($letter_enabled)?$sql_letter_enabled="AND ll.letter_enabled='1'":$sql_letter_enabled="";


		$sql = "SELECT 	ll.letter_id,
				ll.letter_title,
				ll.letter_content,
				ll.letter_enabled,
				ll.letter_width,
				ll.letter_bgcolor,
				ll.letter_pagecolor,
				ll.letter_code,
				lg.group_name ,
				la.address_id,
				la.address,
				ls.status,
				ls.datetime
			FROM
			letters_addresses la
			LEFT JOIN letters_groups_to_users lgtu ON la.address_id = lgtu.address_id 
			LEFT JOIN letters_groups lg ON lgtu.group_id = lg.group_id 
			LEFT JOIN letters_to_groups ltg ON lg.group_id = ltg.group_id 
			LEFT JOIN letters_letters ll on ltg.letter_id = ll.letter_id
			LEFT JOIN letters_shipping ls ON ls.letter_id = ll.letter_id AND ls.address_id = la.address_id 
			WHERE 1
			/* Eventually filter only on enabled letters */
			".$sql_letter_enabled."
			AND la.disabled IS NULL
			/* Eventually filter on shipped address */
			".$sql_status."
			/* Eventually filter on a specific Letter ID */
			".$sql_id."
			GROUP by ll.letter_id,la.address_id
			LIMIT ".$this->limit."";
		
		//echo $sql;
		//var_dump($this->my);
		$q=$this->my->query_array($sql);
		//var_dump($q);
		return $q;
		}


	function ResetShippings($letter_id,$status) {
		//var_dump($status);
		if (!$letter_id) return False;
		if ($status != 1 and $status != 0) return False;
		
		// Protect fields
		$letter_id=$this->my->escape_string($letter_id);
		$status=$this->my->escape_string($status);
		
		//var_dump($letter_id);
		$sql='DELETE FROM letters_shipping WHERE letter_id=\''.$letter_id.'\' AND status=\''.$status.'\' LIMIT 10000';
		//echo $sql;
		$q=$this->my->query_simple($sql);
		if ($q) return True; else return False;
		}

	function GetAddressesofGroup($gid) {
		//if (!$id) return False;
		$sql='SELECT * FROM letters_groups lg
			LEFT JOIN letters_groups_to_users lgtu ON lgtu.group_id = lg.group_id 
			LEFT JOIN letters_addresses la ON la.address_id = lgtu.address_id
			WHERE lg.group_id=\''.$gid.'\'
			LIMIT 5000';
		$sql = 'SELECT lgtu.link_id,la.address_id,la.address FROM letters_addresses la
			LEFT JOIN letters_groups_to_users lgtu ON la.address_id = lgtu.address_id
			WHERE lgtu.group_id=\''.$gid.'\'
			LIMIT 5000
			';
		$q=$this->my->query_array($sql);
		$this->num_rows=$this->my->num_rows;
		return $q;
		//return $result;	
		}


	function GetGroupsOfAddress($address_id) {
		if (!$address_id) return False;
		$sql = 'SELECT lgtu.link_id,lg.group_id,lg.group_name 
			FROM letters_addresses la
			LEFT JOIN letters_groups_to_users lgtu ON lgtu.address_id=la.address_id 
			LEFT JOIN letters_groups lg ON lg.group_id = lgtu.group_id 
			WHERE la.address_id =\''.$address_id.'\'
			AND lg.group_id IS NOT NULL
			LIMIT 1000
			';
		//echo $sql;
		$q=$this->my->query_array($sql);
		return $q;
		//return $result;	
		}

	function GetAddressesOutOfGroup($gid) {
		//if (!$id) return False;
		$sql = '
			SELECT la.address_id,la.address FROM letters_addresses la
			LEFT JOIN letters_groups_to_users lgtu ON la.address_id = lgtu.address_id
			WHERE NOT EXISTS (
				SELECT la2.address FROM letters_addresses la2
				LEFT JOIN letters_groups_to_users lgtu2 ON la2.address_id = lgtu2.address_id
				WHERE lgtu2.group_id=\''.$gid.'\' AND la.address=la2.address
				)
			GROUP BY la.address_id
			LIMIT 1000
		';
		//echo $sql;
		$q=$this->my->query_array($sql);
		return $q;
		//return $result;	
		}

	function SetShippingsDone($letter_id,$address_id,$status) {
		//var_dump($letter_id);
		//var_dump($address_id);
		// Protect fields
		$address_id=$this->my->escape_string($address_id);
		$letter_id=$this->my->escape_string($letter_id);
		
		$sql = "INSERT INTO letters_shipping(letter_id,address_id,status,datetime)
			VALUES($letter_id,$address_id,'1',NOW())";
		//echo $sql;
		$this->my->query_simple($sql);
		//return $txt;
		}

	function UpdateLetter($form,$letter_id=False) {
		// Prepare fields and SQL
		if (!$form['letter-title'])$form['letter-title']=_('Untitled');
		if ($form['letter-enabled']=='on'){$insert_enabled='1';$sql_enabled=',letter_enabled=1';} else {$insert_enabled=0;$sql_enabled=',letter_enabled=0';}
		if ($form['letter-width']){$insert_width=$this->my->escape_string($form['letter-width']);$sql_width=',letter_width='.$this->my->escape_string($form['letter-width']);} else {$insert_width='NULL';$sql_width=',letter_width=NULL';}
		if ($form['letter-bgcolor']){$insert_bgcolor=$this->my->escape_string($form['letter-bgcolor']);$sql_bgcolor=',letter_bgcolor=\''.$this->my->escape_string($form['letter-bgcolor']).'\'';} else {$insert_bgcolor='NULL';$sql_bgcolor=',letter_bgcolor=NULL';}
		if ($form['letter-pagecolor']){$insert_pagecolor=$this->my->escape_string($form['letter-pagecolor']);$sql_pagecolor=',letter_pagecolor=\''.$this->my->escape_string($form['letter-pagecolor']).'\'';} else {$insert_pagecolor='NULL';$sql_pagecolor=',letter_pagecolor=NULL';}
		$sql_title=',letter_title=\''.$this->my->escape_string($form['letter-title']).'\'';
		
		// Construct Query
		$sql = "UPDATE letters_letters
			SET 
			letter_content='".$this->my->escape_string($form['letter-source'])."'
			".$sql_enabled."
			".$sql_width."
			".$sql_bgcolor."
			".$sql_pagecolor."
			".$sql_title."
			WHERE letter_id='".$letter_id."'
			LIMIT 1";
		if (!$letter_id) {
			$letter_code=$this->CreateUniqueId();
			$sql="INSERT INTO letters_letters(letter_title,letter_width,letter_bgcolor,letter_pagecolor,letter_code,letter_content,letter_enabled) 
			VALUES(
				'".$this->my->escape_string($form['letter-title'])."',
				".$insert_width.",
				'".$this->my->escape_string($form['letter-bgcolor'])."',
				'".$this->my->escape_string($form['letter-pagecolor'])."',
				'".$letter_code."',
				'".$this->my->escape_string($form['letter-source'])."',
				'".$insert_enabled."'
				)";
			}

		$this->my->query_simple($sql);
		if (!$letter_id) $letter_id=$this->my->last_id;
		//echo $sql;
		return $letter_id;
		}
		
	function UpdateLettersToGroups($letter_id,$groups) {
		if (!$letter_id) return False;
		// First clean existing links
		$q="DELETE FROM letters_to_groups WHERE letter_id='".$letter_id."' LIMIT 1000";
		//echo $q;
		$this->my->query_simple($q);
		
		// Then add link
		foreach ($groups as $group) {
			$sql = "INSERT INTO letters_to_groups(letter_id,group_id) VALUES(".$letter_id.",".$group.")";
			//echo $sql;
			$this->my->query_simple($sql);
			}
		}

	function CreateLink($gid,$address_id) {
		$sql='INSERT IGNORE INTO letters_groups_to_users(group_id,address_id) VALUES('.$gid.','.$address_id.')';
		$this->my->query_simple($sql);
		return True;
		}

	function RemoveLetter($letter_id) {
		$sql='DELETE FROM letters_letters WHERE letter_id='.$letter_id.' LIMIT 1';
		//echo $sql;
		$this->my->query_simple($sql);
		$sql='DELETE FROM letters_to_groups WHERE letter_id='.$letter_id.'';
		//echo $sql;
		$this->my->query_simple($sql);
		return True;
		}

	function RemoveLink($link_id) {
		$sql='DELETE FROM letters_groups_to_users WHERE link_id='.$link_id.' LIMIT 1';
		$this->my->query_simple($sql);
		return True;
		}

	function RemoveGroup($group_id) {
		$sql='DELETE FROM letters_groups WHERE group_id='.$group_id.' LIMIT 1';
		$this->my->query_simple($sql);
		$sql='DELETE FROM letters_groups_to_users WHERE group_id='.$group_id.'';
		$this->my->query_simple($sql);
		return True;
		}

	function RemoveAddress($address_id) {
		$sql='DELETE FROM letters_groups_to_users WHERE address_id='.$address_id.'';
		$this->my->query_simple($sql);
		$sql='DELETE FROM letters_addresses WHERE address_id='.$address_id.'';
		$this->my->query_simple($sql);
		return True;
		}


	function Unsubscribe($address=False,$address_id=False) {
		if ($address && $address_id) return False;
		if (!$address && !$address_id) return False;

		if ($address_id){
			$sql_filter='la.address_id=\''.$address_id.'\'';
			}
		else {$sql_filter='la.address=\''.$address.'\'';}
		
		$limit ="LIMIT 1";
		
		$sql='UPDATE letters_addresses la SET disabled=1 WHERE '.$sql_filter.' '.$limit.' ';
		
		//echo $sql;
		$this->my->query_simple($sql);
		if (! $this->my->last_error) return _("The following address has been removed from shippings of this sender")." : ".$address;
		else return False;
		}


	function EnableAddress($address_id) {
		$sql='UPDATE letters_addresses la SET disabled=NULL WHERE la.address_id=\''.$address_id.'\'';
		//echo $sql;
		$this->my->query_simple($sql);
		if (! $this->my->last_error) return _("Unable to enable address with ID")." : ".$address_id;
		else return False;
		}

	function AddAddress($addresses,$gid=False) {
		//var_dump($addresses);
		// If separator is a new line
		$lines = preg_split('/\n|\r/',$addresses);
		$lines = preg_split('/\r\n|\r|\n/',$addresses);
		// If there is multiple lines (and last line is not just a carriage return
		if (count($lines)>1 && $lines[1]!="") {$address_list = preg_split('/\r\n|\r|\n/', $addresses);}
                // If separator is a comma+space
                else if (strpos($addresses,", ")!==false) {$address_list=explode(", ",$addresses);}
                // If separator is a ;+space
                else if (strpos($addresses,"; ")!==false) {$address_list=explode("; ",$addresses);}
		// If separator is a space
		else if (strpos($addresses," ")!==false) {$address_list=explode(" ",$addresses);}
		// If separator is a comma
		else if (strpos($addresses,",")!==false) {$address_list=explode(",",$addresses);}
		// If separator is a ;
		else if (strpos($addresses,";")!==false) {$address_list=explode(";",$addresses);}
		// Else it is an address alone
		else $address_list=array($addresses);
		$errors=0;
		
		/*echo "Point virgule : ";var_dump(strpos(";",$addresses));
		echo "Virgule : ";var_dump(strpos(",",$addresses));
		echo "Espace : ";var_dump(strpos(" ",$addresses));*/

		$this->inserted_addresses=0;
		$this->inserted_links=0;		
		foreach ($address_list as $a) {
			// If empty address
			if ($a=="") continue;
			// Check if address already exists
			$exists=$this->GetAddressId($a);
			// If exists get ID
			if ($exists) {
				while ($row = $exists->fetch_assoc()) {
					$address_id=$row['address_id'];
					}
				//var_dump($address_id);
				}
			// Else, create address in DB
			else {
				$sql='INSERT IGNORE INTO letters_addresses(address) VALUES(\''.$a.'\')';
				//echo $sql;
				$res1=$this->my->query_simple($sql);
				$this->inserted_addresses+=$this->my->last_affected_rows;
				//var_dump($res1);
				$address_id=$this->my->last_id;
				if (!$res1) {$errors++;}
				}
			// Create link to group, if requested
			if ($gid && $address_id && ! $this->GetLink($gid,$address_id)) {
				$sql='INSERT IGNORE INTO letters_groups_to_users(group_id,address_id) VALUES('.$gid.','.$address_id.')
				ON DUPLICATE KEY UPDATE group_id=group_id';
				//echo $sql;
				$res2=$this->my->query_simple($sql);
				$this->inserted_links+=$this->my->last_affected_rows;
				if (!$res2) {$errors++;}
				}
			//if (!$address_id or !$res2) {$errors++;}
			}
		return $errors;
		}
		
		
	function MassDisableAddresses($addresses) {
		//var_dump($addresses);
		// If separator is a new line
		$lines = preg_split('/\n|\r/',$addresses);
		if (count($lines)>1) {$address_list = preg_split('/\r\n|\r|\n/', $addresses);}
		// If separator is a space
		else if (strpos($addresses," ")!==false) {$address_list=explode(" ",$addresses);}
		// If separator is a comma
		else if (strpos($addresses,",")!==false) {$address_list=explode(",",$addresses);}
		// If separator is a ;
		else if (strpos($addresses,";")!==false) {$address_list=explode(";",$addresses);}
		// Else it is an address alone
		else $address_list=array($addresses);
		$errors=0;

		$unknown=array();
		$alreadydisabled=array();
		$success=array();
		$errors=array();
		$total=0;

		foreach ($address_list as $a) {
			// If empty address
			if ($a=="") continue;
			//echo "Working on $a";
			// Check if address already exists
			$exists=$this->GetAddressId($a);
			// If exists get ID
			if ($exists) {
				while ($row = $exists->fetch_assoc()) {
					$address_id=$row['address_id'];
					$disabled=$row['disabled'];
					}
				if ($disabled!=1) {
					$dis=$this->Unsubscribe($a);
					if ($dis) $success[]=$a;
					else $errors=$a;
					}
				else $alreadydisabled[]=$a;
				}
			// Else, address is unknkown in database
			else {
				$unknown[]=$a;
				}
			$total++;
			}
		$this->disabled_addresses=count($success);
		$this->already_disabled_addresses=count($alreadydisabled);
		$this->unknown_addresses=count($unknown);		
		$this->errors=count($errors);		
		return True;
		}

	/*	
	function AddAddressToGroup($gid,$addresses) {
		//var_dump($addresses);
		// If separator is a new line
		$lines = preg_split('/\n|\r/',$addresses);
		if (count($lines)>1) {$address_list = preg_split('/\r\n|\r|\n/', $addresses);}
		// If separator is a space
		else if (strpos($addresses," ")!==false) {$address_list=explode(" ",$addresses);}
		// If separator is a comma
		else if (strpos($addresses,",")!==false) {$address_list=explode(",",$addresses);}
		// If separator is a ;
		else if (strpos($addresses,";")!==false) {$address_list=explode(";",$addresses);}
		// Else it is an address alone
		else $address_list=array($addresses);
		$errors=0;
		
		//echo "Point virgule : ";var_dump(strpos(";",$addresses));
		//echo "Virgule : ";var_dump(strpos(",",$addresses));
		//echo "Espace : ";var_dump(strpos(" ",$addresses));
		
		foreach ($address_list as $a) {
			// If empty address
			if ($a=="") continue;
			// Check if address already exists
			$exists=$this->GetAddressId($a);
			// If exists get ID
			if ($exists) {
				while ($row = $exists->fetch_assoc()) {
					$address_id=$row['address_id'];
					}
				//var_dump($address_id);
				}
			// Else, create address in DB
			else {
				$sql='INSERT IGNORE INTO letters_addresses(address) VALUES(\''.$a.'\')';
				//echo $sql;
				$res1=$this->my->query_simple($sql);
				//var_dump($res1);
				$address_id=$this->my->last_id;
				if (!$res1) {$errors++;}
				}
			// Create link to group
			if ($address_id && ! $this->GetLink($gid,$address_id)) {
				$sql='INSERT IGNORE INTO letters_groups_to_users(group_id,address_id) VALUES('.$gid.','.$address_id.')
				ON DUPLICATE KEY UPDATE group_id=group_id';
				//echo $sql;
				$res2=$this->my->query_simple($sql);
				if (!$res2) {$errors++;}
				}
			//if (!$address_id or !$res2) {$errors++;}
			}
		return $errors;
		}
	*/
	function AddGroup($gname) {
		$errors=0;
		if (!$gname) return False;
		$exists=$this->GetGroupId($gname);
		//var_dump($exists);
		if ($exists->num_rows>0) {echo "Already exists";return False;}
		
		$sql='INSERT IGNORE INTO letters_groups(group_name) VALUES(\''.$gname.'\')';
		//echo $sql;
		$res=$this->my->query_simple($sql);
		if (!$res) {$errors++;}
		return $errors;
		}

	// Used to create a unique identifier for the external link
	function CreateUniqueId() {
		$i=0;
		// Loop until code is unique
		while (true) {
			// Generate unique ID
			$str = rand();
			$result = md5($str);

			// Check if it exists
			$sql = 'SELECT letter_code FROM letters_letters WHERE letter_code=\''.$result.'\' LIMIT 1';
			//echo $sql;
			
			$q=$this->my->query_array($sql);
			if ($this->my->num_rows==0) return $result;
			$i++;
			// Break if it takes too long
			if ($i>100) return False;
			}
		}
		
	// Get amount of pending email for this letter
	function GetPending($letter_id) {
		$sql = "SELECT Count(Distinct la.address_id) as nb FROM letters_addresses la 
		LEFT JOIN letters_groups_to_users lgtu ON la.address_id = lgtu.address_id
		LEFT JOIN letters_groups lg ON lgtu.group_id = lg.group_id 
		LEFT JOIN letters_to_groups ltg ON lg.group_id = ltg.group_id 
		LEFT JOIN letters_letters ll on ltg.letter_id = ll.letter_id 
		LEFT JOIN letters_shipping ls ON ls.letter_id = ll.letter_id AND ls.address_id = la.address_id 
		WHERE 1 
		/* Only enabled addresses */
		AND la.disabled IS NULL 
		/* Specific letter only */
		AND ll.letter_id='".$letter_id."'
		AND ls.status IS NULL
		LIMIT 1";
		$q=$this->my->query_array($sql);
		$row = $q->fetch_assoc();
		return $row['nb'];
		}
		
	function DisableLetter($letter_id) {
		if (!$letter_id) return False;
		$sql = "UPDATE letters_letters SET letter_enabled=0 WHERE letter_id='".$letter_id."' LIMIT 1";
		$q=$this->my->query_simple($sql);
		if ($q) return True;
		else return False;
		}
		
		
	function getLastShipping($letter_id=False) {
		if ($letter_id) $sql_letter='WHERE letter_id=\''.$letter_id.'\'';
		else $sql_letter='';
		$sql='SELECT datetime FROM letters_shipping '.$sql_letter.' ORDER BY datetime DESC LIMIT 1';
		$q=$this->my->query_assoc($sql);
		if (!$q) return False;
		else if ($q->num_rows==0) return _('Never');
		else {$r=$q->fetch_assoc();return $r['datetime'];}
		}
	/****************************/
	/* FORMATTED CONTENT ********/
	/****************************/


	function GetShippingStats($letter_id) {
		if (!$letter_id) return False;
		$total=0;
		$success=0;
		$errors=0;
		$pending=0;
		
		$sql="SELECT ls.status,Count(Distinct la.address_id) as nb FROM letters_addresses la 
		LEFT JOIN letters_groups_to_users lgtu ON la.address_id = lgtu.address_id
		LEFT JOIN letters_groups lg ON lgtu.group_id = lg.group_id 
		LEFT JOIN letters_to_groups ltg ON lg.group_id = ltg.group_id 
		LEFT JOIN letters_letters ll on ltg.letter_id = ll.letter_id 
		LEFT JOIN letters_shipping ls ON ls.letter_id = ll.letter_id AND ls.address_id = la.address_id 
		WHERE 1 
		/* Only enabled addresses */
		AND la.disabled IS NULL 
		/* Specific letter only */
		AND ll.letter_id='".$letter_id."' 
		GROUP by ls.status LIMIT 1000";

		//echo $sql;
		
		$q=$this->my->query_array($sql);
		
		while ($row = $q->fetch_assoc()) {
			//var_dump($row['status']);
			if ($row['status']=='0') $errors=$row['nb'];
			else if ($row['status']=='1') $success=$row['nb'];
			else if ($row['status']==False) $pending=$row['nb'];
			$total+=$row['nb'];
			}

		$html='<span class="total" title="'._('Total').'"><span class="value">'.$total.'</span></span>';
		$html.='<span class="pending" title="'._('Pending').'"><span class="value">'.$pending.'</span></span>';
		$html.='<span class="success" title="'._('Success').'"><span class="value">'.$success.'</span><input type="submit" name="reset-success" value="&#10060;" /></span>';
		$html.='<span class="errors" title="'._('Errors').'"><span class="value">'.$errors.'</span><input type="submit" name="reset-errors" value="&#10060;" /></span>';
		return $html;
		}

	// Get CSS content from ckeditor style file
	function MailHeader() {
		// Get good CSS file, possibly a custom one
		$css='';
		if ($this->CONFIG['ckeditor-styles-policy']=='merge' or $this->CONFIG['ckeditor-styles-policy']=='custom') {
			if (file_exists($this->CONFIG['base_folder'].'/css/ck-content-styles.css'))
				$css.= file_get_contents($this->CONFIG['base_folder'].'/css/ck-content-styles.css');
			}
		$css.=''.PHP_EOL;
		if ($this->CONFIG['ckeditor-styles-policy']=='merge' or $this->CONFIG['ckeditor-styles-policy']=='default' or !$this->CONFIG['ckeditor-styles-policy'])
			$css.= file_get_contents('css/ck-content-styles.css');


		// Remove ckeditor class
		$css = str_replace(".ck.ck-content", "", $css);
		// Put CSS content into head and script tags
		$header = "<head>".PHP_EOL."<style>".PHP_EOL.$css.PHP_EOL."</style>".PHP_EOL."</head>".PHP_EOL.PHP_EOL;		
		return $header;
		}

	// Construct mail, based on GetShippings() Method
	function ConstructMail($row,$header=False) {
		// Ignore data missing
		if (!$row) return False;
		
		// Add header if missing
		if (! $header) {
			//$header = "<head>".PHP_EOL."<style>".PHP_EOL.$header;
			// Create empty header
			$header = "<head>".PHP_EOL."<style>".PHP_EOL."</style>".PHP_EOL."</head>".PHP_EOL.PHP_EOL;
			}
		
		
		// Define Background color
		($row['letter_bgcolor'])?$bgcolor=$row['letter_bgcolor']:$bgcolor='#ffffff';
		($row['letter_pagecolor'])?$pagecolor=$row['letter_pagecolor']:$pagecolor='#ffffff';
		
		(!$row['letter_width'] or $row['letter_width']=='')?$width='width:100%;':$width='width:'.$row['letter_width'].'px';
		
		// Define width
		if ($row['letter_width']) {
			$css_body='<body style="background-color:'.$bgcolor.';">';
			$css_body_end='</body>';
			$css_width='<div style="'.$width.';margin:0 auto;background-color:'.$pagecolor.';">';
			$css_width_end='</div>';
			}
		else {
			$css_body='<body class="nostyle">';
			$css_body_end='</body>';
			$css_width='';
			$css_width_end='';
			}

		// Construct link to read online
		$online_link='<a href="'.$this->CONFIG['base_url'].'/?page=letters-display&display_id='.$row['letter_code'].'">'._('Read online version').'</a>';
		
		// Construct unsubscribe link
		$unsubscribe='<a href="'.$this->CONFIG['base_url'].'/?page=letters-display&unsubscribe='.$row['address'].'">'._('Unsubscribe from this sender').'</a>';

		// Assemble footer
		$footer='<div style="color:grey;text-align:center;margin : 0 auto;'.$width.';">'.$online_link.' | '.$unsubscribe.'</div>';
		
		// Define Full content
		$full_content=$header.$css_body.$css_width.$row['letter_content'].$css_width_end.$footer.$css_body_end;
	
		return $full_content;
		}

	function GetLetterContentHtml($id) {
		if (! $id) return False;
		$letter=$this->GetLetters($id);
		while ($row = $letter->fetch_assoc()) {
			$html.='<div>'.$row['letter_content'].'></div>';
			}
		return $html;
		}

	function GetLettersHtml($letter_id=False) {
		$letters=$this->GetLetters();
		while ($row = $letters->fetch_assoc()) {
			($letter_id==$row['letter_id'])?$css_sel=' selected':$css_sel='';
			$html.='<span class="letters-leftspan'.$css_sel.'">
			<!--<span class="leftmenu-namewrapper">-->
				<a href="?page=letters&letter_id='.$row['letter_id'].'">
				'.$row['letter_title'].'</a>
			<!--</span>-->
			<button name="rmletter" type="submit" value="'.$row['letter_id'].'" title="'._('Delete').'">&#10060;</button></span>';
			}
		return $html;
		}
		
	/*
	function GetShippingsHtml() {
		$shippings=$this->GetShippings();
		while ($row = $shippings->fetch_assoc()) {
			//var_dump($row);
			$txt.='Shipping letter "'.$row['letter_title'].'" to "'.$row['address'].'"'.PHP_EOL;
			}
		return $txt;
		}
	*/
	function GetSelectGroups($id) {
		//var_dump($id);
		//if (! $id) return False;
		$select='';
		// First we get groups associated to the current letter
		$ltg = $this->GetLettersToGroups($id);
		$altg=array();
		//var_dump($t->now_rows);
		if ($ltg->num_rows>0) {
			while ($row = $ltg->fetch_assoc()) {
				$altg[]=$row['group_id'];
				}		
			}
		//var_dump($altg);

		// Then we get all groups
		$groups=$this->GetGroups();
		//$select.='<option value="">'._('None').'</option>'.PHP_EOL;
		while ($row = $groups->fetch_assoc()) {
			$members=$this->GetLinks($row['group_id']);
			(in_array($row['group_id'],$altg))?$sel='selected':$sel='';
			$select.='<option value="'.$row['group_id'].'" '.$sel.'>'.$row['group_name'].' ('.$this->num_rows.')</option>'.PHP_EOL;
			}
		return $select;
		}
#
	function GetGroupsHtml($group_id=False) {
		$g = $this->GetGroups();
		//$altg=array();
		$html='';
		while ($row = $g->fetch_assoc()) {
			($group_id==$row['group_id'])?$css_sel=' selected':$css_sel='';
			$html.='
			<span class="letters-leftspan '.$css_sel.'">
				<a href="?page=letters-groups&group_id='.$row['group_id'].'">
					'.$row['group_name'].'
				</a>
			<button name="rmgroup" type="submit" value="'.$row['group_id'].'" title="'._('Delete').'">&#10060;</button>
			</span>'.PHP_EOL;
			}
		return $html;
		}
		
	function GetGroupsOfAddressHtml($adress_id) {
		$g = $this->GetGroupsOfAddress($adress_id);
		//$altg=array();
		$html='';
		while ($row = $g->fetch_assoc()) {
			$html.='<span class="group-adresses"><a href="?page=letters-groups&group_id='.$row['group_id'].'">
			&#128106; '.$row['group_name'].'</a>
			<button name="rmlink" type="submit" value="'.$row['link_id'].'" title="'._('Delete').'">&#10060;</button></span>'.PHP_EOL;
			}
		return $html;
		}
#
	function GetAddressesofGroupHtml($gid) {
		$g = $this->GetAddressesofGroup($gid);
		//$altg=array();
		$html='';
		while ($row = $g->fetch_assoc()) {
			$html.='
			<span class="letters-leftspan users-list"><a href="?page=letters-addresses&address_id='.$row['address_id'].'">'.$row['address'].'</a>
			 <!--<a href="?page=letters-groups&group_id='.$gid.'&rmlink='.$row['link_id'].'" title="'._('Delete').'">&#10060;</a>-->
			 <button name="rmlink" type="submit" value="'.$row['link_id'].'">&#10060;</button></span><br />'.PHP_EOL;
			}
		//var_dump($html);
		return $html;
		}
#
	function GetAddressesHtml($address_id=False) {
		$g = $this->GetAddresses();
		//$altg=array();
		$html='';
		if ($g !== false) {
			while ($row = $g->fetch_assoc()) {
				($address_id==$row['address_id'])?$css_sel=' selected':$css_sel='';
				// If disabled
				if ($row['disabled']){
					$disabling='<button name="enableaddress" type="submit" value="'.$row['address_id'].'" title="'._('Enable').'">&#128077;</button>';
					$css_disabled='disabled';
					}
				else {
					$disabling='<button name="disableaddress" type="submit" value="'.$row['address_id'].'" title="'._('Disable').'">&#8856;</button>';
					$css_disabled='';
				}
				// Construct HTML
				$html.='<span class="letters-leftspan letters-addresses '.$css_disabled.''.$css_sel.'">
						<a href="?page=letters-addresses&address_id='.$row['address_id'].'" class="'.$css_sel.'">
						'.$row['address'].'
						</a>
					<button name="rmaddress" type="submit" value="'.$row['address_id'].'" title="'._('Delete').'">&#10060;</button>
					'.$disabling.'</span>'.PHP_EOL;
			}
		}
		return $html;
		}
#
	function GetAddressesOutofGroupHtml($gid) {
		$g = $this->GetAddressesOutofGroup($gid);
		$html='';
		while ($row = $g->fetch_assoc()) {
			$html.='<span class="letters-users-list"><a href="?page=letters-addresses&address_id='.$row['address_id'].'">'.$row['address'].'</a>
			 <!--<a href="?page=letters-groups&group_id='.$gid.'&createlink='.$row['address_id'].'">'._('Add').'</a>-->
			 <button name="createlink" type="submit" value="'.$row['address_id'].'">'._('Add').'</button></span><br />'.PHP_EOL;
			}
		//var_dump($html);
		return $html;
		}
#		
	function Menu() {
		return '
		<a href="?page=letters">&#128466; '._('Letters').'</a>
		<a href="?page=letters-groups">&#128106; '._('Groups').'</a>
		<a href="?page=letters-addresses">&#128125; '._('Addresses').'</a>
		<span class="lastshipping">'._('Last shipping (any letter)').' : '.$this->getLastShipping().'</span>';
		}

	}
	// Class is over
?>
