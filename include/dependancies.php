<?php
/*
This file include somes specific required dependancies, i.e. Egulias Email Validator and Lexer AbstractLexer. They are both required by swiftmailer
*/


// Lexer AbstractLayer
include_once('lexer/AbstractLayer.php');
// Egulias Email Validator
include_once('egulias/EmailValidator/Validation/EmailValidation.php');
include_once('egulias/EmailValidator/Validation/RFCValidation.php');
include_once('egulias/EmailValidator/Validation/DNSCheckValidation.php');
include_once('egulias/EmailValidator/Exception/InvalidEmail.php');

include_once('egulias/EmailValidator/Parser/Parser.php');
include_once('egulias/EmailValidator/Warning/Warning.php');
include_once('egulias/EmailValidator/Warning/LocalTooLong.php');
include_once('egulias/EmailValidator/Parser/LocalPart.php');
include_once('egulias/EmailValidator/Parser/DomainPart.php');
//include_once('egulias/EmailValidator/Exception/NoDomainPart.php');// In case of necessity

include_once('egulias/EmailValidator/Validation/MultipleValidationWithAnd.php');
include_once('egulias/EmailValidator/Validation/NoRFCWarningsValidation.php');
include_once('egulias/EmailValidator/Validation/SpoofCheckValidation.php');

include_once('egulias/EmailValidator/EmailValidator.php');
include_once('egulias/EmailValidator/EmailParser.php');
include_once('egulias/EmailValidator/Validation/MultipleErrors.php');
include_once('egulias/EmailValidator/EmailLexer.php');
//
