<?php

//var_dump($_POST);
$A = new Attendee($CONFIG,$my);
//echo $_SERVER['SERVER_NAME'];

//* Functions */

/* Change state */
function ChangeState($my,$attendee_id,$state,$key=NULL,$fromstate=NULL) {
	// $my;
	($key)?$sql_key=' AND confirmkey=\''.$key.'\' ':$sql_key=NULL;
	($fromstate)?$sql_state = ' AND state=\''.$fromstate.'\' ':$sql_state=NULL;
	$sql = 'UPDATE attendees SET state=\''.$state.'\' WHERE id=\''.$attendee_id.'\' '.$sql_key.' '.$sql_state.' LIMIT 1';
	//echo $sql;
	$my->query_simple($sql);
	$return=$my->last_affected_rows;
	//echo "<br />Nombre de ligne trouvées : ".$return;
	return $return;
	}

function CleanSimpleField($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
	}
function CleanFileName($string) {
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	$string = preg_replace('/[^A-Za-z0-9\-\.]/', '', $string); // Removes special chars.
	return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

// Verify uploaded file
function UploadImage($files,$selectbox,$basefolder) {
	$maxfilesize = 512000;
	$maxfilesizeKo= round($maxfilesize / 1024);
	$dest = dirname(__FILE__).'/../'.$basefolder.'/images/';
	 if(isset($files[$selectbox])){
		$errors= array();
		$file_name = $files[$selectbox]['name'];
		$file_size = $files[$selectbox]['size'];
		$file_tmp = $files[$selectbox]['tmp_name'];
		$file_type = $files[$selectbox]['type'];
		$file_ext=strtolower(end(explode('.',$files['image']['name'])));
		$types = array("image/png");
		$new_filename = CleanFileName($file_name);
		if(in_array($file_type,$types)=== false){
			$errors[]="File not allowed, please choose a PNG file.";
			}
		if($file_size > $maxfilesize) {
			$errors[]='File size must not be bigger than '.$maxfilesizeKo.' Ko';
			}
		if (file_exists($dest.$new_filename)) {
			$errors[]='Sorry. File '.$new_filename.' already exists.';
			}

		if(empty($errors)==true) {
			move_uploaded_file($file_tmp,$dest.$new_filename);
			//echo "Success. Moving file to : ".$dest.clean($file_name);
			return $new_filename;
			}
			else{
				return $errors;
			}
		}
	}



//**********/
// Create place
//**********/

if (	isset($_POST['create-place'])
	&& isset($_POST['place-title'])
	&& $_POST['place-description']!=""
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {
	$sql = 'INSERT INTO places(title,description,disabled) 
		VALUES(
			"'.$my->escape_string($_POST['place-title']).'",
			"'.$my->escape_string($_POST['place-description']).'",
			"0"
			)';

	$ok['query']=$my->query_simple($sql);
	if ($ok['query']) $infos.=_('place-creation-ok');
	else $errors.=_('place-creation-nok')."<br />".$my->last_error;
	}



//**********/
// Modify place
//**********/

if (	isset($_POST['modify-place'])
	&& isset($_POST['place-title']) 
	&& isset($_POST['places-title-select'])
	&& $_POST['place-description']
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {
	$sql = 'UPDATE places
		SET 
		title="'.$my->escape_string($_POST['place-title']).'",
		description="'.$my->escape_string($_POST['place-description']).'"
		WHERE id="'.$_POST['places-title-select'].'"
		LIMIT 1';
	$ok['query']=$my->query_simple($sql);
	if ($ok['query']) $infos.=_('place-modification-ok');
	else $errors.=_('place-modification-nok')."<br />".$my->last_error;
}


//**********/
// Disable place
//**********/

if (	isset($_POST['disable-place'])
	&& isset($_POST['places-title-select'])
	&& isset($_POST['disable-place']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	//$_POST['places-title-select']=CleanSimpleField($_POST['places-title-select']);
	$sql = 'UPDATE places SET disabled=\'1\' WHERE id=\''.CleanSimpleField($_POST['places-title-select']).'\' LIMIT 1';

	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Place successfully disabled');
	else
		$errors.=_('Error disabling place');
}

//**********/
// Enable place
//**********/

if (	isset($_POST['enable-place'])
	&& isset($_POST['places-title-select'])
	&& isset($_POST['enable-place']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	//$_POST['places-title-select']=CleanSimpleField($_POST['places-title-select']);
	$sql = 'UPDATE places SET disabled=\'0\' WHERE id=\''.CleanSimpleField($_POST['places-title-select']).'\' LIMIT 1';

	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Place successfully enabled');
	else
		$errors.=_('Error enabling place');
}

//**********/
// Create session
//**********/

if (	isset($_POST['schedule-start'])
	&& $_POST['schedule-start']!=""
	&& $_POST['schedule-submit']
	&& isset($_POST['schedule-end']) 
	&& $_POST['schedule-end']!=""
	&& isset($_POST['schedule-max-attendee'])
	&& $_POST['schedule-max-attendee'] != ""
	//&& isset($_POST['schedule-title-select']) 
	&& isset($_POST['schedule-price'])
	&& isset($_POST['schedule-title'])
	&& isset($_POST['schedule-place-select'])
	&& isset($_POST['schedule-comment'])
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {

	// Select OR Uploaded file (if set) OR selected file
	// Init
	$ok['upload']=True;
	if ($_FILES['schedule-uploadimage']['name']!="") {
		$upload = UploadImage($_FILES,'schedule-uploadimage',$CONFIG['base_folder']);
		// If upload successful, use filename for session
		if (! is_array($upload)) {
			$schedule_image = "'".$upload."'";
			}
		else { // Append errors to error div
			$ok['upload']=False;
			$schedule_image='NULL';
			foreach ($upload as $err) {
				$errors.=$err.'<br />';
				}
			}
		}
	else if ($_POST['schedule-image']!='') $schedule_image="'".$_POST['schedule-image']."'";
	else $schedule_image = 'NULL';


	$date = new DateTime($_POST['schedule-start']);
	$schedule_start=$date->format('Y-m-d H:i:s');
	$date = new DateTime($_POST['schedule-end']);
	$schedule_end=$date->format('Y-m-d H:i:s');

	// Some rewrite
	($_POST['schedule-title-select']=="")?$template_id='NULL':$template_id=$_POST['schedule-title-select'];
	($_POST['schedule-notice']!='1')?$notice='0':$notice=1;
	($_POST['schedule-quickbook']!='1')?$quickbook='0':$quickbook=1;
	($_POST['schedule-user-select']=='' OR $_POST['schedule-user-select']==NULL)?$username='NULL':$username="'".$_POST['schedule-user-select']."'";
	($_POST['schedule-place-select']=='' OR $_POST['schedule-place-select']==NULL)?$place='NULL':$place=$_POST['schedule-place-select'];

	$sql = 'INSERT INTO sessions(start,end,template_id,max_attendee,price,title,comment,image,username,place,quickbook,notice) 
		VALUES(	"'.$schedule_start.'",
			"'.$schedule_end.'",
			'.$template_id.',
			'.CleanSimpleField($_POST['schedule-max-attendee']).',
			'.CleanSimpleField($_POST['schedule-price']).',
			"'.$my->escape_string($_POST['schedule-title']).'",
			"'.$my->escape_string($_POST['schedule-comment']).'",
			'.$schedule_image.',
			'.$username.',
			'.$place.',
			'.$quickbook.',
			'.$notice.'
			)';
	$ok['query']=$my->query_simple($sql);
	$schedule_inserted_id=$my->last_id;
	if ($ok['query'] && $ok['upload']) $infos.=_('schedule-creation-ok');
	else $errors.=_('schedule-creation-nok')."<br />".$my->last_error;
}

//**********/
// Modify session
//**********/

if (
	isset($_POST['schedule-start']) 
	&& $_POST['schedule-modify']
	&& $_POST['schedule-start']!=""
	&& isset($_POST['schedule-end']) 
	&& $_POST['schedule-end']!=""
	&& isset($_POST['schedule-max-attendee'])
	&& $_POST['schedule-max-attendee'] != ""
	//&& isset($_POST['schedule-title-select']) 
	&& isset($_POST['schedule-price'])
	&& isset($_POST['schedule-title'])
	&& isset($_POST['schedule-place-select'])
	&& isset($_POST['schedule-comment'])
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {


	// Some rewrite
	$date = new DateTime($_POST['schedule-start']);
	$schedule_start=$date->format('Y-m-d H:i:s');
	$date = new DateTime($_POST['schedule-end']);
	$schedule_end=$date->format('Y-m-d H:i:s');
	($_POST['schedule-user-select']=='' OR $_POST['schedule-user-select']==NULL)?$username='NULL':$username='\''.CleanSimpleField($_POST['schedule-user-select']).'\'';
	($_POST['schedule-place-select']=='' OR $_POST['schedule-place-select']==NULL)?$place='NULL':$place=CleanSimpleField($_POST['schedule-place-select']);
	($_POST['schedule-notice']!='1')?$notice='0':$notice=1;
	($_POST['schedule-quickbook']!='1')?$quickbook='0':$quickbook=1;

	// Select OR Uploaded file (if set) OR selected file
	// Init
	$ok['upload']=True;
	if ($_FILES['schedule-uploadimage']['name']!="") {
		$upload = UploadImage($_FILES,'schedule-uploadimage',$CONFIG['base_folder']);
		// If upload successful, use filename for session
		if (! is_array($upload)) {
			$schedule_image = "'".$upload."'";
			}
		else { // Append errors to error div
			$ok['upload']=False;
			$schedule_image='NULL';
			foreach ($upload as $err) {
				$errors.=$err.'<br />';
				}
			}
		}
	else if ($_POST['schedule-image']!='') $schedule_image="'".$_POST['schedule-image']."'";
	else $schedule_image = 'NULL';

	$sql = 'UPDATE sessions
		SET 
		start="'.$schedule_start.'",
		end = "'.$schedule_end.'",
		max_attendee='.CleanSimpleField($_POST['schedule-max-attendee']).',
		price='.CleanSimpleField($_POST['schedule-price']).',
		title="'.$my->escape_string($_POST['schedule-title']).'",
		comment="'.$my->escape_string($_POST['schedule-comment']).'",
		image='.$schedule_image.',
		username='.$username.',
		place="'.$place.'",
		quickbook="'.$quickbook.'",
		notice="'.$notice.'"

		WHERE id="'.CleanSimpleField($_GET['event_id']).'"

		LIMIT 1';

	$ok['query']=$my->query_simple($sql);
	if ($ok['query'] && $ok['upload']) $infos.=_('schedule-modification-ok');
	else $errors.=_('schedule-modification-nok')."<br />".$my->last_error;
}

//**********/
// Server-side form checks
//**********/

if (	isset($_POST['schedule-start']) && isset($_POST['schedule-end'])
	&& ($_POST['schedule-modify'] || $_POST['schedule-submit'])) {
	// Check date inconsistency
	if (strtotime($_POST['schedule-start']) >= strtotime($_POST['schedule-end'])) {
		// In case of creation, get the id of inserted element
		if (!$_GET['event_id']) $_GET['event_id']=$schedule_inserted_id;
		$errors.=_('schedule-dates-nok')."<br />";
		}
	//var_dump($_GET['event_id']);
	}

//**********/
// Cancel session
//**********/

if (	isset($_POST['cancel-animation-button']) 
	&& isset($_POST['cancel-animation-session_id']) 
	&& isset($_SESSION['profile']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	$sql = 'UPDATE sessions SET canceled=\'1\' WHERE id=\''.CleanSimpleField($_POST['cancel-animation-session_id']).'\' LIMIT 1';

	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Animation successfully canceled');
	else
		$errors.=_('Error canceling animation');
}

//**********/
// Restore session
//**********/

if (	isset($_POST['restore-animation-button']) 
	&& isset($_POST['restore-animation-session_id']) 
	&& isset($_SESSION['profile']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	$sql = 'UPDATE sessions SET canceled=\'0\' WHERE id=\''.CleanSimpleField($_POST['restore-animation-session_id']).'\' LIMIT 1';

	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Animation successfully restored');
	else
		$errors.=_('Error restoring animation');
	}

//**********/
// Delete session
//**********/

if (
	isset($_POST['schedule-start']) 
	&& $_POST['schedule-delete']
	&& $_POST['schedule-start']!=""
	&& isset($_POST['schedule-end']) 
	&& $_POST['schedule-end']!=""
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {

	$sql = 'DELETE FROM sessions
		WHERE id="'.CleanSimpleField($_GET['event_id']).'"		
		LIMIT 1;
		';
	$ok=$my->query_simple($sql);
	$sql2 = 'DELETE FROM attendees WHERE session_id="'.CleanSimpleField($_GET['event_id']).'" LIMIT 1000';
	$ok2=$my->query_simple($sql2);
	if ($ok && $ok2) $infos.=_('schedule-deletion-ok');
	else $errors.=_('schedule-deletion-nok')."<br />".$my->last_error;
	$_GET['event_id']=NULL;
	}


//**********/
// Modify template
//**********/

if (	$_POST['template-update']
	&& isset($_POST['schedule-title-select'])
	&& $_POST['schedule-title-select'] != ""
	&& isset($_POST['schedule-title'])
	&& $_POST['schedule-title'] != "" // NON-Allowed to be empty
	&& isset($_POST['schedule-max-attendee']) // Allowed to be empty
	&& isset($_POST['schedule-price']) // Allowed to be empty
	&& isset($_POST['schedule-comment'])// Allowed to be empty
	&& isset($_POST['schedule-image']) // Allowed to be empty
	&& isset($_POST['schedule-place-select']) // Allowed to be empty
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {

	// Some rewrite
	($_POST['schedule-price']=="")?$schedule_price='NULL':$schedule_price='"'.CleanSimpleField($_POST['schedule-price']).'"';
	($_POST['schedule-comment']=="")?$schedule_comment='NULL':$schedule_comment='"'.$my->escape_string($_POST['schedule-comment']).'"';
	($_POST['schedule-max-attendee']=="")?$schedule_max_attendee='NULL':$schedule_max_attendee='"'.CleanSimpleField($_POST['schedule-max-attendee']).'"';
	($_POST['schedule-user-select']=="")?$schedule_user_select='NULL':$schedule_user_select='"'.CleanSimpleField($_POST['schedule-user-select']).'"';
	($_POST['schedule-place-select']=="")?$schedule_place_select='NULL':$schedule_place_select='"'.CleanSimpleField($_POST['schedule-place-select']).'"';
	($_POST['schedule-notice']!='1')?$notice='0':$notice=1;
	($_POST['schedule-quickbook']!='1')?$quickbook='0':$quickbook=1;
	
	// Select OR Uploaded file (if set) OR selected file
	// Init
	$ok['upload']=True;
	if ($_FILES['schedule-uploadimage']['name']!="") {
		$upload = UploadImage($_FILES,'schedule-uploadimage',$CONFIG['base_folder']);
		// If upload successful, use filename for session
		if (! is_array($upload)) {
			$schedule_image = "'".$upload."'";
			}
		else { // Append errors to error div
			$ok['upload']=False;
			$schedule_image='NULL';
			foreach ($upload as $err) {
				$errors.=$err.'<br />';
				}
			}
		}
	else if ($_POST['schedule-image']!='') $schedule_image="'".$_POST['schedule-image']."'";
	else $schedule_image = 'NULL';

	$sql = 'UPDATE templates
		SET

			title="'.$my->escape_string($_POST['schedule-title']).'",
			max_attendee='.$schedule_max_attendee.',
			price='.$schedule_price.',
			image='.$schedule_image.',
			comment='.$schedule_comment.',
			username='.$schedule_user_select.',
			place='.$schedule_place_select.',
			quickbook='.$quickbook.',
			notice = "'.$notice.'"
		WHERE id="'.CleanSimpleField($_POST['schedule-title-select']).'"
		LIMIT 1';

	$ok['query']=$my->query_simple($sql);
	if ($ok['query'] && $ok['upload']) $infos.=_('template-modification-ok');
	else $errors.=_('template-modification-nok')."<br />".$my->last_error;
	}


/*******************/
/* Create template */
/*******************/
if (	$_POST['template-create'] 
	&& $_POST['schedule-title-select'] == ""
	&& isset($_POST['schedule-title'])
	&& $_POST['schedule-title'] != "" // NON-Allowed to be empty
	&& isset($_POST['schedule-max-attendee']) // Allowed to be empty
	&& isset($_POST['schedule-price']) // Allowed to be empty
	&& isset($_POST['schedule-comment'])// Allowed to be empty
	&& isset($_POST['schedule-image']) // Allowed to be empty
	&& isset($_POST['schedule-place-select']) // Allowed to be empty
	&& isset($_SESSION['profile'])
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {

	// Some rewrite
	($_POST['schedule-price']=="")?$schedule_price='NULL':$schedule_price='"'.CleanSimpleField($_POST['schedule-price']).'"';
	($_POST['schedule-comment']=="")?$schedule_comment='NULL':$schedule_comment='"'.$my->escape_string($_POST['schedule-comment']).'"';
	($_POST['schedule-max-attendee']=="")?$schedule_max_attendee='NULL':$schedule_max_attendee='"'.CleanSimpleField($_POST['schedule-max-attendee']).'"';
	($_POST['schedule-user-select']=="")?$schedule_user_select='NULL':$schedule_user_select='"'.CleanSimpleField($_POST['schedule-user-select']).'"';
	($_POST['schedule-place-select']=="")?$schedule_place_select='NULL':$schedule_place_select='"'.CleanSimpleField($_POST['schedule-place-select']).'"';
	($_POST['schedule-notice']!='1')?$notice='0':$notice=1;
	($_POST['schedule-quickbook']!='1')?$quickbook='0':$quickbook=1;

	// Select OR Uploaded file (if set) OR selected file
	// Init
	$ok['upload']=True;
	if ($_FILES['schedule-uploadimage']['name']!="") {
		$upload = UploadImage($_FILES,'schedule-uploadimage',$CONFIG['base_folder']);
		// If upload successful, use filename for session
		if (! is_array($upload)) {
			$schedule_image = "'".$upload."'";
			}
		else { // Append errors to error div
			$ok['upload']=False;
			foreach ($upload as $err) {
				$errors.=$err.'<br />';
				}
			}
		}
	else if ($_POST['schedule-image']!='') $schedule_image="'".CleanSimpleField($_POST['schedule-image'])."'";
	else $schedule_image = 'NULL';

	$sql = 'INSERT into templates (
			title,
			max_attendee,
			price,
			image,
			comment,
			username,
			place,
			quickbook,
			notice
			)
		VALUES (
		"'.$my->escape_string($_POST['schedule-title']).'",
		'.$schedule_max_attendee.',
		'.$schedule_price.',
		'.$schedule_image.',
		'.$schedule_comment.',
		'.$schedule_user_select.',
		'.$schedule_place_select.',
		'.$quickbook.',
		'.$notice.'
		)';

	$ok=$my->query_simple($sql);
	if ($ok) $infos.=_('template-creation-ok');
	else $errors.=_('template-creation-nok')."<br />".$my->last_error;
	}



//**********/
// Register
//**********/
if (isset($_GET['register'])) {
	// Time formatter
	$dt = new DateTime;
	$formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::MEDIUM);

	// Get info
	$session_id = CleanSimpleField($_GET['register']);
	$email = filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);

	// Verify Captcha - only for unregistered users
	if (! $userid && strtolower($_POST['input-captcha-'.$session_id])!=strtolower($_SESSION['captchacode'])) {
		$errors=_('Captcha error');
		}
	else if ($_POST['email']) {
		// Get 'max attendee' value of session
		$sql='		SELECT 
					sessions.max_attendee as max_attendee,
					templates.title as title,
					sessions.start as start,
					sessions.end,
					sessions.notice
				FROM sessions 
				LEFT JOIN templates ON sessions.template_id = templates.id
				WHERE sessions.id=\''.$session_id.'\'';

		$q=$my->query_simple($sql);
		$r=$q->fetch_assoc();

		$r['start'] = $formatter->format(strtotime($r['start']));

		// Check that attendee is not already registered
		$sql='SELECT * FROM attendees WHERE session_id=\''.$session_id.'\' AND email=\''.$email.'\' ';
		$q=$my->query_simple($sql);
		$s=$my->num_rows;

		// Create Event object
		$Event=new Event($CONFIG,$my,$session_id);
		// Check that user is not already registered
		if ($s > 0) {
			$errors.=_('already-registered');
			}
		// If event is in the past
		else if (strtotime($r['end'])<strtotime("now") && ! isset($_SESSION['profile'])) {
			$errors.=_('You are not allowed to register for an event in the past');
			}
		// check that session is not full
		else if ($Event->isfull($session_id)) {
			$errors.=_('max-attendees-reached');
			}
		else {
			// Create confirmation Key
			$confirmkey = substr(md5(microtime()),rand(0,26),24);
			($CONFIG['enable_mail_confirmation'])?$state='WAIT4CONFIRM':$state='CONFIRMED';

			// Insert into database
			$sql='INSERT INTO attendees(session_id,email,state,confirmkey) VALUES(\''.$session_id.'\',\''.$email.'\',\''.$state.'\',\''.$confirmkey.'\')';
			$my->query_simple($sql);

			// Create email title
			$title=sprintf(_('email-title'),$CONFIG['swiftmailer_sender_name'],$r['title'],$r['start']);


			// Build Confirm & Cancel links
			$link_confirm = $CONFIG['base_url'].'?event_id='.$session_id.'&attendee_id='.$my->last_id.'&confirmkey='.$confirmkey.'&confirmregistration=1';
			$link_cancel = $CONFIG['base_url'].'?event_id='.$session_id.'&attendee_id='.$my->last_id.'&confirmkey='.$confirmkey.'&cancelregistration=1';


			if ($CONFIG['msg']['event-subscription-confirmation'])
				{
				// Replace [EVENTNAME] & [EVENTDATE]
				$event_sub_confirm = str_replace("[EVENTNAME]", $r['title'], $CONFIG['msg']['event-subscription-confirmation']);
				$event_sub_confirm = str_replace("[EVENTDATE]", $r['start'], $event_sub_confirm);
				$event_sub_confirm = str_replace("[CONFIRMLINK]", $link_confirm, $event_sub_confirm);
				$event_sub_confirm = str_replace("[CANCELLINK]", $link_cancel, $event_sub_confirm);
				}
			else {
				$event_sub_confirm = '
						'.$CONFIG['website-name'].'<br />
						Event : "'.$r['title'].'" on '.$r['start'].'<br /><br />
						Please confirm : <a href="'.$link_confirm.'">Confirm</a><br />
						or<br />
						cancel :  <a href="'.$link_cancel.'">Cancel</a><br />
						<br /><br />
						'.$CONFIG['msg']['signature'].'<br />
						';
			
				}



			// *******************************

			/*
			// Message starts with css
			$message=$CONFIG['msg']['css'];

			// Then we put message
			if ($CONFIG['msg']['email-confirmation'])
				$message.=sprintf($CONFIG['msg']['email-confirmation'],$CONFIG['website-name'],$r['start']);
			else
				$message.=sprintf(_('email-confirmation'),$CONFIG['website-name'],$r['start']);

			// We put confirmation and cancel button
			$message.='<br />';

			// We add buttons
			if ($CONFIG['msg']['registerbuttons'])
				$message.=sprintf($CONFIG['msg']['registerbuttons'],$link_confirm,_('Click here to confirm'),$link_cancel,_('Click here to cancel'));
			else
				$message.=sprintf('<div class="confirmregistration"><a class="boutton" href="%s">%s</a></div><br /><div class="cancel"><a class="boutton" href="%s">%s</a></div>',$link_confirm,_('Click here to confirm'),$link_cancel,_('Click here to cancel'));

			// And signature
			$message.='<br />';
			$message.=$CONFIG['msg']['signature'];
			*/
			

			// We send email
			$mail->Send($email,$title,$event_sub_confirm);

			// And log success
			$infos.=_('Confirmation email sent');
			}
		}
	}

//************/
// Confirm   */
//************/
if (isset($_GET['attendee_id']) && isset($_GET['confirmkey']) && isset($_GET['confirmregistration'])) {
	$attendee_id = CleanSimpleField($_GET['attendee_id']);
	$confirmkey = CleanSimpleField($_GET['confirmkey']);

	// Check that attendee can CONFIRM (only with correct confirm key, on future events)
	$sql = 'SELECT 	attendees.*, 
			sessions.start,
			sessions.end
		FROM attendees 
		LEFT JOIN sessions on attendees.session_id = sessions.id
		WHERE 1
		AND attendees.id=\''.$attendee_id.'\' 
		AND confirmkey=\''.$confirmkey.'\' 
		#AND sessions.end >= NOW()
		LIMIT 1';

	$q=$my->query_simple($sql);
	$ok=$my->num_rows;
	$r=$q->fetch_assoc();
	$event_id = $r['session_id'];
	$attendee_email = $r['email'];

	// PRE-REQUISITES
	// Create Event object
	if ($event_id) {
		// Create new event object
		$Event=new Event($CONFIG,$my,$event_id);
		// Get current state for this registration
		$currstate=$A->GetState($attendee_id);
                // Check If event is already confirmed
                if ($currstate==$CONFIG['state_confirmed']) {
                        $warnings.=_('Confirmation NOK');
                        }
                // Check If event is in the past
                else if (strtotime($r['end'])<strtotime("now")) {
                        $errors.=_('Unable to modify registration for an event in the past');
                        }
                // Check If event is full
                else if ($Event->isfull($event_id)) {
                        $errors.=_('max-attendees-reached');
                        }
                // Otherwise, try to change state
                else {
                        //$ok2 = $A->ChangeState($my,$attendee_id,'CONFIRMED',$confirmkey);
			// Use OO style
                        $ok2 = $A->ChangeState($attendee_id,'CONFIRMED',$confirmkey);
                        if ($ok2 != 1) $errors.=_('Internal error');
                        }

                // If State change successfull
                if ($ok2 == 1) {
                        $infos.=_('Confirmation OK');
                        // Eventually send notification to animator
                        $Animator=new Animator($CONFIG,$my);
                        $notice=$Animator->notify($Event,$attendee_email,$mail);
                        if ( ! $notice) $errors.=_('Error sending notification to animator');
                        }
                }
	else
		$errors.=_('Unable to find attendee');

	}


//**********/
// Cancel registration
//**********/
if (isset($_GET['attendee_id']) && isset($_GET['confirmkey']) && isset($_GET['cancelregistration'])) {
	$attendee_id = CleanSimpleField($_GET['attendee_id']);
	$confirmkey = CleanSimpleField($_GET['confirmkey']);

	// Verif attendee key
	// Check that attendee can CONFIRM (only with correct confirm key, on future events)
	$sql = 'SELECT 	attendees.*, 
			sessions.start,
			sessions.end
		FROM attendees 
		LEFT JOIN sessions on attendees.session_id = sessions.id
		WHERE 1
		AND attendees.id=\''.$attendee_id.'\' 
		AND confirmkey=\''.$confirmkey.'\' 
		#AND sessions.start >= NOW()
		LIMIT 1';

	$q=$my->query_simple($sql);
	$ok=$my->num_rows;
	$r=$q->fetch_assoc();
	$state=$r['state'];
	$event_id = $r['session_id'];
	$attendee_email = $r['email'];

	// Create Event object
	$Event=new Event($CONFIG,$my,$event_id);

	// If event is in the past
	if (strtotime($r['end'])<strtotime("now")) {
		$errors=_('Unable to modify registration for an event in the past');
		}

	// If attendee exists
	if ($ok==1) {
		// Change state in DB
		$ok2 = ChangeState($my,$attendee_id,'CANCELED',$confirmkey);

		if ($ok2 == 1) {
			$infos.=_('Cancelation OK');
			// Eventually send notification to animator
			$Animator=new Animator($CONFIG,$my);
			$notice=$Animator->notify($Event,$attendee_email,$mail,'cancelation');
			if ( ! $notice) $errors.=_('Error sending notification to animator');
			}
		else
			$errors.=_('Cancelation NOK');
		}
	else
		$errors.=_('Unable to find attendee');
	}

//**********/
// Change state of attendee
//**********/
if (isset($_POST['newstate']) && isset($_POST['save-attendee-state']) && isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {
	$newstate = CleanSimpleField($_POST['newstate']);
	$attendee_id = CleanSimpleField($_POST['attendee_id']);
	$ok2 = ChangeState($my,$attendee_id,$newstate);
	if ($ok2 == 1)
		$infos.=_('Modification OK');
	else
		$errors.=_('Modification NOK');
	}

//**********/
// Delete attendee
//**********/

if (isset($_POST['delete-attendee']) && isset($_POST['delete-attendee']) && isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {
	$attendee_id = CleanSimpleField($_POST['attendee_id']);
	$sql = 'DELETE FROM attendees WHERE id=\''.$attendee_id.'\' LIMIT 1';

	$ok=$my->query_simple($sql);
	if ($ok)
		$infos.=_('Attendee deletion OK');
	else
		$errors.=_('Attendee deletion failed');;
	}

//**********/
// Filters
//**********/
$sql_filter="";

// Modify and store value
if (isset($_GET['event_id'])) {
	$_SESSION['event_id']=CleanSimpleField($_GET['event_id']);
	unset($_SESSION['session-title-select']);
	}
else if (isset($_POST['session-title-select'])) {
	$_SESSION['session-title-select'] =CleanSimpleField($_POST['session-title-select']);
	unset($_SESSION['event_id']);
	}

// react on stored values
if ((isset($_SESSION['event_id'])) && ($_GET['page']=='')) {
	$sql_filter.=' AND sessions.id=\''.$_SESSION['event_id'].'\' ';
	}

// TODO - More comments needed ?
else if (isset($_SESSION['session-title-select']) && ($_SESSION['session-title-select']!='all')  && (!$_GET['page'] or $_GET['page']=='')) {
	// Init	
	(isset($_POST['session-title-select']))?$session_title_select=$_POST['session-title-select']:$session_title_select=$_SESSION['session-title-select'];
	// reset event_id if set
	$t=explode("-",$_SESSION['session-title-select']);
	$type=$t[0];
	$id=$t[1];

	if ($type == "session") {
		$sql_filter.=' AND sessions.id=\''.$id.'\' ';
		}
	else if ($type == "template") {
		$sql_filter.=' AND templates.id=\''.$id.'\' ';
		}
	$sql_filter.=' AND sessions.start>=NOW() - INTERVAL 1 DAY';
	}
else if (! $_GET['page'] or $_GET['page']=='' or ! isset($_GET['page'])) {
	$sql_filter.=' AND sessions.start>=NOW() - INTERVAL 1 DAY';
}
else if ($_GET['page']=='calendar') {
	$sql_filter.=' AND sessions.start>=NOW() - INTERVAL '.$CONFIG['calendar_history'].' MONTH AND sessions.start<=NOW() + INTERVAL '.$CONFIG['calendar_upcoming'].' MONTH';
}

//*************/
// Take Animation */
//*************/

if (	isset($_POST['take-animation-button']) 
	&& isset($_POST['take-animation-session_id']) 
	&& isset($_SESSION['profile']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	$sql = 'UPDATE sessions SET username=\''.CleanSimpleField($userid).'\' WHERE id=\''.CleanSimpleField($_POST['take-animation-session_id']).'\' LIMIT 1';

	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Animation successfully booked');
	else
		$errors.=_('Error booking animation');
}

//*************/
// Release Animation */
//*************/

if (	isset($_POST['release-animation-button']) 
	&& isset($_POST['release-animation-session_id']) 
	&& isset($_SESSION['profile']) 
	&& ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')
	) {

	$sql = 'UPDATE sessions SET username=NULL WHERE id=\''.CleanSimpleField($_POST['release-animation-session_id']).'\' AND  username=\''.CleanSimpleField($userid).'\' LIMIT 1';
	$ok=$my->query_simple($sql);

	if ($ok)
		$infos.=_('Animation successfully released');
	else
		$errors.=_('Error releasing animation');
}


//******************/
//
// USERSPACE
//
//******************/


//******************/
// Reset user profile (userpace)
//******************/
if ($_POST['userspace-reset-request'] && $_POST['userspace-userid-reset']) {
	//
	$requesteduserid = CleanSimpleField($_POST['userspace-userid-reset']);
	$userinfo = $auth->GetUserInfo($requesteduserid,'users.mail');
	$useremail = $requesteduserid;
	$external_id = NULL;
	$valid = True;

	// Check Dolibarr requirements
	if ($Dolibarr && $userinfo['profile']!='ADMIN' && 
		(
		($userinfo['ldap'] && $CONFIG['dolibarr']['login_ldap_require_valid_sub']) or 
		($CONFIG['dolibarr']['login_require_valid_sub'] && !$userinfo['ldap'])
		)
	    ) {
		//$endofsubscription=$Dolibarr->get_last_subscription_date($requesteduserid);
		//echo $endofsubscription.'<br />';
		$DolibarrInfo = $Dolibarr->GetMemberInfo($requesteduserid);
		
		if ($DolibarrInfo) {
			$valid = $Dolibarr->is_valid($DolibarrInfo['last_subscription_date_end']);
			//echo $DolibarrInfo['last_subscription_date_end'].'<br />';
			$useremail = $DolibarrInfo['email'];
			$external_id = $DolibarrInfo['id'];
			//var_dump($external_id);
			//var_dump($useremail);
			}
		else $valid=False;
		}

	//var_dump($userinfo);
	// If something went wrong
	if (! $valid) {
		$warnings.=_('We are unable to check your userspace. Are you a valid member ?');
		}
	else if ($userinfo['ldap']==1) {
		$warnings.=_('Your account is linked to an LDAP directory')."<br />";
		$warnings.=_('Please use the following tool to reset your password')." : ";
		$warnings.='<a href="'.$CONFIG['ldap_external_pwd_tool'].'" target="new">'._('Click here').'</a><br />';
		}
	// Only require a valid Dolibarr subscription if setting is True
	else if ($userinfo) {
		// Create confirmation Key
		$confirmkey = substr(md5(microtime()),rand(0,26),24);

		//var_dump($DolibarrInfo);
		($DolibarrInfo['firstname'])?$userfirstname=$DolibarrInfo['firstname']:$userfirstname=NULL;
		($DolibarrInfo['lastname'])?$userlastname=$DolibarrInfo['lastname']:$userlastname=NULL;

		//var_dump($DolibarrInfo);

		// Update confirmation key (and eventually new external id)
		$auth->UpdateLocalAccount($requesteduserid,NULL,NULL,$userfirstname,$userlastname,$useremail,NULL,NULL,$confirmkey,$external_id);

		// Create email title
		$title=sprintf(_('email-title-userspace-reset'),$CONFIG['swiftmailer_sender_name']);

		// Create Raw message
		$link_confirm = $CONFIG['base_url'].'?page=userspace&userspace='.$userinfo['id'].'&confirmkey='.$confirmkey.'&resetpassword=1';

		// Message starts with css
		$message=$CONFIG['msg']['css'];

		// Then we add message content
		if ($CONFIG['msg']['email-confirmation-password-reset'])
			$message.=sprintf($CONFIG['msg']['email-confirmation-password-reset'],$CONFIG['website-name']);
		else
			$message.=sprintf(_('email-confirmation-password-reset'),$CONFIG['website-name']);

		// Next is button
		$message.='<br />';

		// We add buttons
		if ($CONFIG['msg']['email-confirmation-password-reset-buttons'])
			$message.=sprintf($CONFIG['msg']['email-confirmation-password-reset-buttons'],$link_confirm,_('Click here to complete reset of your password'));
		else
			$message.=sprintf('<div class="confirmregistration"><a class="boutton" href="%s">%s</a></div>',$link_confirm,_('Click here to complete reset of your password'));

		// And signature
		$message.='<br />';
		$message.=$CONFIG['msg']['signature'];

		// We send it
		$mail->Send($requesteduserid,$title,$message);
	
		// Yep !
		$infos.=_('Confirmation email sent');

		}
	else
		{
		// Or we log error
		$errors.=_('An unknown error occured');
		}
}
//******************/
// Create user profile (userpace)
//******************/

if ($_POST['userspace-create-request'] && $_POST['userspace-userid']) {

	$requesteduserid = CleanSimpleField($_POST['userspace-userid']);
	$userinfo = $auth->GetUserInfo($requesteduserid,'users.mail');
	$useremail = $requesteduserid;
	$external_id = NULL;
	$valid = True;

	// Check Dolibarr requirements
	if ($Dolibarr && $userinfo['profile']!='ADMIN' &&
		(
		($userinfo['ldap'] && $CONFIG['dolibarr']['login_ldap_require_valid_sub']) or 
		($CONFIG['dolibarr']['login_require_valid_sub'] && !$userinfo['ldap'])
		)
	    ) {
		//$endofsubscription=$Dolibarr->get_last_subscription_date($requesteduserid);
		//echo $endofsubscription.'<br />';
		$DolibarrInfo = $Dolibarr->GetMemberInfo($requesteduserid);
		if ($DolibarrInfo) {
			$valid = $Dolibarr->is_valid($DolibarrInfo['last_subscription_date_end']);
			//echo $DolibarrInfo['last_subscription_date_end'].'<br />';
			$useremail = $DolibarrInfo['email'];
			$external_id = $DolibarrInfo['id'];
			}
		else $valid=False;
		}

	//echo "<br /><br />";
	//var_dump($DolibarrInfo);

	// If something went wrong
	if (! $valid) {
		$warnings.=_('We are unable to create a userspace for you, sorry. Did you renew your subscription ?');
		}
	// user already exists.
	else if ($userinfo) {
		$warnings.=_('Your userspace already exists');
		$warnings.='<br />'._('Have you forgot your password ?');
	}
	// Only require a valid Dolibarr subscription if setting is True
	else if (! $userinfo) {
		// Create confirmation Key
		$confirmkey = substr(md5(microtime()),rand(0,26),24);

		//var_dump($DolibarrInfo);
		($DolibarrInfo['firstname'])?$userfirstname=$DolibarrInfo['firstname']:$userfirstname=NULL;
		($DolibarrInfo['lastname'])?$userlastname=$DolibarrInfo['lastname']:$userlastname=NULL;
		($DolibarrInfo['address'])?$useraddress=$DolibarrInfo['address']:$useraddress=NULL;
		($DolibarrInfo['zip'])?$userzip=$DolibarrInfo['zip']:$userzip=NULL;
		($DolibarrInfo['town'])?$usercity=$DolibarrInfo['town']:$usercity=NULL;

		// Create temporary account
		$profilecreation=$auth->UpdateLocalAccount($requesteduserid,NULL,'USER',$userfirstname,$userlastname,$useremail,NULL,NULL,$confirmkey,$external_id,False,False,$useraddress,$userzip,$usercity);

		if (! $profilecreation) {
			$errors.=$auth->errors;
			}

		// Create email title
		$title=sprintf(_('email-title-userspace'),$CONFIG['swiftmailer_sender_name']);

		// Create Raw message
		$link_confirm = $CONFIG['base_url'].'?page=userspace&userspace='.$my->last_id.'&confirmkey='.$confirmkey.'&setpassword=1';

		// Message starts with custom css
		$message=$CONFIG['msg']['css'];

		// Then we add message
		if ($CONFIG['msg']['email-confirmation-userspace'])
			$message.=sprintf($CONFIG['msg']['email-confirmation-userspace'],$CONFIG['website-name']);
		else
			$message.=sprintf(_('email-confirmation-userspace'),$CONFIG['website-name']);

		// We add confirmation link at the bottom
		$message.='<br />';

		if ($CONFIG['msg']['email-confirmation-userspace-buttons'])
			$message.=sprintf($CONFIG['msg']['email-confirmation-userspace-buttons'],$link_confirm,_('Click here to complete creation of your userspace'));
		else
			$message.=sprintf('<div class="confirmregistration"><a class="boutton" href="%s">%s</a></div>',$link_confirm,_('Click here to complete creation of your userspace'));

		// And the signature
		$message.='<br />';
		$message.=$CONFIG['msg']['signature'];

		// Then we send it
		$mail->Send($requesteduserid,$title,$message);

		// Log everything
		$infos.=_('Confirmation email sent');

		}
	else
		{
		// Or throw an error
		$errors.=_('An unknown error occured');
		}
	}


//******************/
// Save NEW password
//******************/

if ($_POST['set_password'] && $_GET['setpwdid'] && $_GET['confirmkey']) {
	$userinfo = $auth->GetUserInfo($_GET['setpwdid'],'users.id',False);
	//var_dump($userinfo);
	$username = strtolower($userinfo['username']);
	$password = $_POST['password'];
	//var_dump($username);
	//var_dump($password);
	if ($_POST['password']!=$_POST['password2']) $warnings.=_('Sorry, password do not match');
	else if (! $userinfo) $errors.=_('Unable to find you. Is your account disabled ?');
	else if (! $auth->IsRegistered($_GET['setpwdid'],'users.id',$_GET['confirmkey'],False)) $errors.=_('Wrong confirmation key');
	else {
		// Create a new confirm key for next time (forgotten password, etc)
		$confirmkey = substr(md5(microtime()),rand(0,26),24);
		$auth->UpdateLocalAccount($username,$password,NULL,NULL,NULL,NULL,NULL,NULL,$confirmkey);
		$auth->Authenticate($username,$password);
		$userid=$username;
		}
	}

//******************/
// Save profile (userspace)
//******************/

if ($_POST['profile-submit'] && $userid) {
	//echo $userid;
	//$DolibarrInfo = $Dolibarr->GetMemberInfo($userid);
	//var_dump($DolibarrInfo);
	// Only a few fields (password,user description,contact agreement) are allowed for modification at this stage
	($_POST['contact_agreement']=='on')?$contact_agreement=1:$contact_agreement=0;
	$auth->UpdateLocalAccount($userid,$_POST['user_password'],NULL,NULL,NULL,NULL,$_POST['user_comment'],NULL,NULL,NULL,$contact_agreement,True,$_POST['user_address'],$_POST['user_zip'],$_POST['user_city']);

	// Eventually Sync address with Dolibarr
	//var_dump($userinfo['external_id']);
	if ($Dolibarr && $userinfo['external_id']) {
		if ($_POST['user_address']!=$userinfo['address'] or $_POST['user_zip']!=$userinfo['zip'] or $_POST['user_city']!=$userinfo['city']) {
			$data=array('address'=>$_POST['user_address'],'zip'=>$_POST['user_zip'],'city'=>$_POST['user_city']);
			$Dolibarr->UpdateMember($userinfo['external_id'],$data);
			$txt_doli.=' (including Dolibarr)';
			}
		}
	if (! $auth->errors)
		$infos.=_('Profile sucessfully updated').' '._($txt_doli);
	else
		$errors.=_('Error updating profile : '.$auth->errors);
	}

// Save eventually previously posted skills
if ($_POST['save-skills'] && $userid) {
	//echo "ici!";
	$auth->SaveSkillsHtml($userid,$_POST['skills'],$_POST['trainingrqst']);
	if (! $auth->errors)
		$infos.=_('Profile sucessfully updated');
	else
		$errors.=_('Error updating profile : '.$auth->errors);

	}

//var_dump($_POST);
?>
