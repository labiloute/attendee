<?php
class mysql {

	public $dbuser;
	public $dbpass;
	public $dbhost;
	public $dbname;
	public $cx;
	public $last_affected_rows;
	public $last_id;
	public $num_rows;

	function __construct($dbhost,$dbuser,$dbpass,$dbname)
		{
		$this->dbhost=$dbhost;
		$this->dbuser=$dbuser;
		$this->dbpass=$dbpass;
		$this->dbname=$dbname;

		$this->cx = new mysqli($this->dbhost,$this->dbuser,$this->dbpass,$this->dbname);
		if ($this->cx->connect_errno){
			$this->cx=False;
			$this->connect_errno=$this->cx->connect_errno;
			$this->connect_error=$this->cx->connect_error;
			//return False;
			}
		}

	function query_array($sql) {
		if ($this->cx) {
			$result = $this->cx->query($sql) or die("Erreur de requête Mysql type array : ".$this->cx->error."<br /> <strong>Debug</strong> :<br />".nl2br($sql));
			//(isset($result->num_rows))?$this->num_rows = $result->num_rows:$this->num_rows=False;
			$this->last_affected_rows = $this->cx->affected_rows;
			$this->num_rows = $result->num_rows;
			return $result;
		}
		else return False;
	}
	function query_assoc($sql) {
		if ($this->cx) {
			$result = $this->cx->query($sql) or die("Erreur de requête Mysql type assoc : ".$this->cx->error);
			//(isset($result->num_rows))?$this->num_rows = $result->num_rows:$this->num_rows=False;
			$this->last_affected_rows = $this->cx->affected_rows;
			$this->num_rows = $result->num_rows;
			return $result;
		}
		else return False;
	}
	function query_simple($sql) {
		if ($this->cx) {
			$result = $this->cx->query($sql) or $this->last_error="Erreur de requête simple Mysql : ".$this->cx->error;
			$this->last_affected_rows = $this->cx->affected_rows;
			//echo $this->last_affected_rows;
			//echo "Affected : ".$this->last_affected_rows.' (for query '.$sql.')<br />';
			//(isset($result->num_rows))?$this->num_rows = $result->num_rows:$this->num_rows=False;
			$this->num_rows = $result->num_rows;
			$this->last_id = $this->cx->insert_id;
			return $result;
		}
		else return False;
	}
	function escape_string($string) {
		return $this->cx->real_escape_string($string);
	}

	function close() {
		if ($this->cx) {
			$this->cx->close();
		}
		else return False;
	}

}
?>
