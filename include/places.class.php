<?php
class Place {

	public $CONFIG;
	public $my;
	public $infos;
	public $warnings;
	public $errors;

	function __construct($CONFIG,$my) {

	//Store settings
	$this->CONFIG = $CONFIG;
	$this->my = $my;

	// Logs
	$this->infos=NULL;
	$this->warnings=NULL;
	$this->errors=NULL;
	}

	//***************************
	// 
	//***************************
	function GetPlaceText($id) {
	
	$sql = 'SELECT * FROM places WHERE id=\''.$id.'\' LIMIT 1';

	$q=$this->my->query_simple($sql);
	$r=$q->fetch_assoc();
	if ($r['text'] && $r['text']!='') return $r['text'];
	else return False;

	}

	function GetPlacesList() {

	$sql = 'SELECT id,IF(disabled=1,CONCAT("[DIS] ",title),title) as title,disabled FROM places ORDER BY disabled ASC, title ASC LIMIT 100';

	$q=$this->my->query_simple($sql);
	//$r=$q->fetch_assoc();
	return $q;
	}
	

}# End of class
?>
