<?php
echo '<!DOCTYPE html>
<html>
    <head>

	';

	// Display custom favico if exists otherwise fallback to default
	if (file_exists($CONFIG['base_folder'].'/favico.ico'))
		echo '<link rel="icon" type="image/x-icon" href="'.$CONFIG['base_folder'].'/favico.ico'.'">'.PHP_EOL;
	else
		echo '<link rel="icon" type="image/x-icon" href="images/favico.ico" />'.PHP_EOL;


	// Start of Meta tags
	echo '

        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<!-- Bootstrap -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<link rel="stylesheet" href="css/bootstrap.min.css">
    	<link rel="stylesheet" href="js/jquery-te/jquery-te-1.4.0.css">
	<!-- / Bootstrap -->
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	'.PHP_EOL;

	// Calendar view stylesheet
	if ($_GET['page']=='calendar') {
		echo "
		<link href='fullcalendar/fullcalendar.min.css' rel='stylesheet' />
		<link href='fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		".PHP_EOL;
	}

	/* Todolist view stylesheet */
	elseif ($_GET['page']=='todolist')
		echo '<link href="css/todolist.css?v=3" rel="stylesheet" />'.PHP_EOL;

	/* Css used for leaflet maps */
	elseif ($_GET['page']=='usermap' or $_GET['page']=='search') {
		echo'<link rel="stylesheet" href="leaflet/leaflet.css" crossorigin="" />'.PHP_EOL;
		echo'<link rel="stylesheet" type="text/css" href="leaflet.markercluster/dist/MarkerCluster.css" />'.PHP_EOL;
		echo'<link rel="stylesheet" type="text/css" href="leaflet.markercluster/dist/MarkerCluster.Default.css" />'.PHP_EOL;
		}
		
		
	/* Css used for displaying letter */
	elseif ($_GET['page']=='letters-display' || $_GET['page']=='letters') {
		// Custom css file for project if exists. Fallback to default otherwise
		//echo $CONFIG['base_folder'].'/css/ck-content-styles.css';
		//var_dump(file_exists($CONFIG['base_folder'].'/css/ck-content-styles.css'));
		if ($CONFIG['ckeditor-styles-policy']=='merge' or $CONFIG['ckeditor-styles-policy']=='custom') {
			if (file_exists($CONFIG['base_folder'].'/css/ck-content-styles.css'))
				echo '<link href="'.$CONFIG['base_folder'].'/css/ck-content-styles.css" rel="stylesheet" type="text/css">'.PHP_EOL;
			}
		if ($CONFIG['ckeditor-styles-policy']=='merge' or $CONFIG['ckeditor-styles-policy']=='default' or !$CONFIG['ckeditor-styles-policy'])
			echo '<link rel="stylesheet" href="css/ck-content-styles.css" crossorigin="" />'.PHP_EOL;
		}
	?>

	<?php
	// Custom css file for project if exists. Fallback to default otherwise
	if (file_exists($CONFIG['base_folder'].'/css/style.css'))
		echo '<link href="'.$CONFIG['base_folder'].'/css/style.css'.'?v=2" rel="stylesheet" media="all" type="text/css">'.PHP_EOL;
	else
		echo '<link href="css/style.css?v=2" rel="stylesheet" media="all" type="text/css">'.PHP_EOL;


	// Common stylesheets
	echo '
	<link rel="stylesheet" href="css/icono.min.css">
	<link rel="stylesheet" href="css/jquery.ui.datepicker.css" />
	<link type="text/css" rel="stylesheet" href="css/jquery-ui-timepicker-addon.css" />
	<!-- PureCSS -->
	<link rel="stylesheet" href="css/pure-min.css">
	<!-- / PureCSS -->
	<!-- Add to calendar button -->
	<link rel="stylesheet" href="addtocalendarbutton/assets/css/atcb.min.css">
	<!-- Add to calendar button -->
        <title>'._('software-title').'</title>
    </head>
    <body>
'.PHP_EOL;
?>
