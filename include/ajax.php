<?php
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
$config='config/'.$_SERVER['HTTP_HOST'].'.php';
$config_default='config/config.default.php';
session_start();
if (file_exists('../'.$config))
	include_once('../'.$config);
else {
	include_once('../'.$config_default);
        $_CONFIG['mono_instance'] = True; // Notify script that it runs in mono instance mode (Docker integration)
        }
if (file_exists('../vendor/autoload.php')) include_once('../vendor/autoload.php'); // Eventually include symfony dependancy (IntlDateFormatter)
include_once('../config/env.php');// Override config with $_ENV if it exists
include_once('mysql.php');

// Classes
include_once('places.class.php');
include_once('attendee.class.php');
include_once('event.class.php');

// Functions
function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
        }
    });
    return $array;
}

// Force locale if given in form
if ($_POST['force_locale']) $CONFIG['force_locale']=$_POST['force_locale'];
$verbose=False;// Brefore locale.php (disable warnings from locale.php)
$localepath='../locale';// Brefore locale.php (disable warnings from locale.php)
include_once('locale.php');

if ($_POST['config']) {
	$variable=$_POST['config'];
	echo $CONFIG[$variable].$_POST['force_locale'];
	exit;
}

if ($_POST['translation']) {
	$phrase=$_POST['translation'];
	echo _($phrase);
	exit;
}


// Connect to mysql
$my = new mysql($CONFIG['dbhost'],$CONFIG['dbuser'],$CONFIG['dbpass'],$CONFIG['dbname']);
$my->query_simple("SET NAMES 'utf8'");/* PREPARE UTF8 */
$my->query_simple("SET NAMES 'utf8'");

// Init Classes
$Places = new Place($CONFIG,$my);

// Query for template or specific session
if (isset($_POST['template_id']) or isset($_POST['event_id'])) {

	// Create event object
	$event=new Event($CONFIG,$my);
	// Display Event form
	echo $event->ScheduleForm($_POST['event_id'],$_POST['template_id'],$Places);

	} ?>


<?php if (isset($_POST['attendeeemail'])) {
$sql_past='
	SELECT Count(*) as nb,attendees.state as state
	FROM attendees
	LEFT JOIN sessions on sessions.id=attendees.session_id
	WHERE 1
	AND DAY(sessions.start) < DAY(NOW())
	AND email=\''.$_POST['attendeeemail'].'\'
	GROUP BY state LIMIT 100';

	echo $sql_past;
	}
?>



<?php
// Query for template or specific session
if (isset($_POST['place_id'])) {
	$sql = "SELECT * FROM places WHERE id='".$_POST['place_id']."' LIMIT 1";

	// Execute and fetch
	$r=$my->query_assoc($sql);
	$r=$r->fetch_assoc();
?>
	<!--<input type="text" value="<?php echo $_POST['place_id']; ?>" id="place_id" name="place_id" style="display:none;" /></span>-->
	<?php if ($r['disabled']=='1') echo '<span class="places-wrapper title"><span class="disabled-place">Disabled</span></span>'; ?>
	<span class="places-wrapper title"><?php echo _('Title'); ?></span>
	<span class="places-wrapper content"><input type="text" value="<?php echo $r['title']; ?>" id="place-title" name="place-title" required /></span>
	<span class="places-wrapper title"><?php echo _('Description'); ?></span>
	<span class="places-wrapper content"><textarea id="place-description" name="place-description" ><?php echo $r['description']; ?></textarea></span>


<?php
	if (is_numeric($_POST['place_id'])) 
		{
?>
		<span class="places-wrapper">
			<input type="submit" id="modify-place" name="modify-place" value="<?php echo _('Modify place'); ?>" />
			<?php if ($r['disabled']=="0") { ?>
				<input type="submit" id="disable-place" name="disable-place" value="<?php echo _('Disable place'); ?>" />
			<?php 
			}
			else {
			?>
				<input type="submit" id="enable-place" name="enable-place" value="<?php echo _('Enable place'); ?>" />
			<?php } ?>	
		</span>
	<?php 
	} 
	else { 
	?>
		<span class="places-wrapper">
			<input type="submit" id="create-place" name="create-place" value="<?php echo _('Create place'); ?>" />
		</span>
	<?php } ?>
<?php } ?>







<?php if (isset($_POST['attendeeemail'])) {
$sql_past='
	SELECT Count(*) as nb,attendees.state as state
	FROM attendees
	LEFT JOIN sessions on sessions.id=attendees.session_id
	WHERE 1
	AND DAY(sessions.start) < DAY(NOW())
	AND email=\''.$_POST['attendeeemail'].'\'
	GROUP BY state LIMIT 100';

	echo $sql_past;
	}
?>








<?php
if (isset($_POST['todolistaction'])) {
	include_once('../include/todolist.php');// Include TodoList Class
	$todo = new ToDo($row,$my);
	$todolistid = (int)$_POST['todolistid'];
	($_POST['todolistcat'])?$todolistcat=$_POST['todolistcat']:$todolistcat=NULL;
	($_POST['todolisttext'])?$todolisttext=$_POST['todolisttext']:$todolisttext=NULL;
	($_POST['todolistpositions'])?$todolistpositions=$_POST['todolistpositions']:$todolistpositions=NULL;
	try{

		switch($_POST['todolistaction'])
		{
			
			case 'delete':
				$todo->delete($todolistid,$todolistcat);
				break;
			
			case 'rearrange':
				$todo->rearrange($todolistpositions,$todolistcat);
				break;
			
			case 'edit':
				$todo->edit($todolistid,$todolisttext,$todolistcat);
				break;
			
			case 'new':
				//echo "ici";
				// Hé ho !
				$todo->createNew($todolisttext,$todolistcat);
				break;
		}

	}
	catch(Exception $e){
		echo $e->getMessage();
		die("0");
	}
}
?>
<?php
if (isset($_POST['searchcat'])) {

	if ($_GET['debug']) {$_POST['searchcat']=$_GET['debug']; $_GET['callback'] = '***calbacksalt***';}
	//echo $_POST['searchcat'];
	
	$sql = 'SELECT category FROM todolist WHERE LOWER(category) LIKE LOWER(\'%'.$_POST['searchcat'].'%\') GROUP BY category LIMIT 100';

	//echo $sql;
	$r = $my->query_array($sql);

	$catlist=[];
	while ($cat=$r->fetch_array(MYSQLI_NUM))
	   $catlist[]=$cat[0];

	$r=utf8_converter($catlist);

	echo $_GET['callback'].'('.json_encode($r).')';
}


// UP !
if (isset($_POST['love']) && isset($_SESSION['username'])) {
	$sql = "INSERT IGNORE INTO todolistup (todo_id,username) VALUES('".$_POST['love']."','".$_SESSION['username']."')";
	$r = $my->query_simple($sql);
	if ($my->last_affected_rows==1) echo 'OK'; else echo 'NOK';
}
// DOWN !
if (isset($_POST['unlove']) && isset($_SESSION['username'])) {
	$sql = "DELETE FROM todolistup WHERE todo_id='".$_POST['unlove']."' AND username='".$_SESSION['username']."' LIMIT 1";
	$r = $my->query_simple($sql);
	if ($my->last_affected_rows==1) echo 'OK'; else echo 'NOK';
}


// PAGE SEARCH USER

if ($_POST['searchskill']) {
	// Creation objet Attendee
	$A = new Attendee($CONFIG,$my);

	//if ($_GET['debug']) {$_POST['searchcat']=$_GET['debug']; $_GET['callback'] = '***calbacksalt***';}
	//echo $_POST['searchcat'];

	$skills = $A->GetSkills(False,False,$_POST['searchskill']);

	$list=array();

	//$tmp = str(var_dump($skills));
	if (is_array($skills)) {
		foreach ($skills as $skill) {
			$list[]=array('value'=>$skill['id'],'label'=>$skill['title']);
			}
		}
	elseif (is_object($skills)) {
		while ($row = $skills->fetch_array()) {
			$list[]=array('value'=>$row['id'],'label'=>$row['title']);
			}
		}
	// Else we debug return of query
	else {
		$list[]=array('value'=>0,'label'=>$skills);
		//$list[]=array('value'=>1,'label'=>$tmp);
		}
	

	$r=utf8_converter($list);

	echo $_GET['callback'].'('.json_encode($r).')';

	}

// \ PAGE SEARCH USER

// Get json styles to populate ckeditor style. Styles MUST be defined in css style as well.
if ($_POST['getckeditorjson']) {
	$final_table=array();
	$custom_table=array();
	$default_table=array();
	$custom_styles='../'.$CONFIG['base_folder'].'/json/ckeditor_styles.json';
	$default_styles='../json/default_ckeditor_styles.json';
	if (file_exists($custom_styles)) {
		$custom_styles_content=file_get_contents($custom_styles);
		$custom_table[]=json_decode($custom_styles_content,true);
		}
	if (file_exists($default_styles)) {
		$default_styles_content=file_get_contents($default_styles);
		//$json=json_encode($default_styles);
		$default_table[]=json_decode($default_styles_content,true);
		}
	//var_dump($custom_table);
	
	//echo json_encode($default_table[0]);
	if ($CONFIG['ckeditor-styles-policy']=='merge' && file_exists($custom_styles))
		$final_table=array_merge($custom_table[0],$default_table[0]);
	else if ($CONFIG['ckeditor-styles-policy']=='custom' && file_exists($custom_styles))
		$final_table=$custom_table[0];
	else
		$final_table=$default_table[0];

	echo "\"".str_replace("\"", "\\\"",json_encode($final_table))."\"";
	//else $styles=$default_styles;
	//$json = json_($styles);
	//echo json_encode($final_table);
	}

?>
<?php
// Close connection to mysql
$my->close();
?>
