<?php

class Animator
{
	/**
	* Constructor
	*/
	function __construct($CONFIG,$my)
	{
        	$this->CONFIG=$CONFIG;
		$this->my=$my;
		$this->error=False;
	}


	function notify($Event,$attendee_email,$Mail,$type='registration') {

		$event=$Event->getfields(array('id','title','username','notice','start'));

		//echo $attendee_email.'<br />';
		if ($event['notice'] == '1') {
			$animator_mail=$this->get_email($event['username']);
		
			if ($type=='registration') {
				$title=_('An attendee confirmed his participation for one of your event');
				$message=sprintf(_('animator-email-confirmation'),$attendee_email,$event['title'],$event['start']);
			}
			else if ($type=='cancelation') {
				$title=_('An attendee canceled his participation for one of your event');
				$message=sprintf(_('animator-email-cancelation'),$attendee_email,$event['title'],$event['start']);
				}
			$Mail->Send($animator_mail,$title,$message);
			if ($Mail) return True;
			else return False;
			}
		else return True;
		}

	/* 
		Get email of users
	*/
	function get_email($username) {
		$sql="SELECT mail FROM users WHERE username='".$username."' LIMIT 1";
		$q=$this->my->query_simple($sql);
		$r=$q->fetch_assoc();
		if ($r['mail'] && $r['mail']!='') return $r['mail'];
		else return False;
		}


	function format_date($date) {

		}

	}

?>
