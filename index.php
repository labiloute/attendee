<?php
session_start();
//header('Content-Type: text/html; charset=utf-8'); //test
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
// Config file is furnished by command line if run by CLI
if (php_sapi_name() == 'cli') $config='config/'.$argv[1].'.php';
// else it is picked up from URL
else $config='config/'.$_SERVER['HTTP_HOST'].'.php';
// Else fallback to default
$config_default='config/config.default.php';
if (file_exists($config))
	include_once($config);
else {
	include_once($config_default);
	$_CONFIG['mono_instance'] = True; // Notify script that it runs in mono instance mode (Docker integration)
	}

//if (file_exists('vendor/autoload.php')) include_once('vendor/autoload.php'); // Eventually include symfony dependancy (IntlDateFormatter)

include_once('include/dependancies.php'); // Include some external dependancies
include_once('config/env.php');// Override config with $_ENV if it exists
include_once('include/mysql.php');
include_once('include/mail.php');
include_once('include/ldapad.php');
include_once('include/users.class.php');
include_once('include/animator.class.php');
include_once('include/event.class.php');
include_once('include/places.class.php');
include_once('include/attendee.class.php');
include_once('include/letters.class.php');
$verbose = False;
include_once('include/locale.php');

// Optionnal modules
if (file_exists('include/dolibarr.class.php')) {
	include_once('include/dolibarr.class.php');
	$Dolibarr=new Dolibarr($CONFIG);
	}

// Init
$infos = NULL;
$warnings = NULL;
$errors = NULL;

// Connect to mysql
$my = new mysql($CONFIG['dbhost'],$CONFIG['dbuser'],$CONFIG['dbpass'],$CONFIG['dbname']);
//if (!$my && php_sapi_name() == 'cli') echo "[WARNING] Mysql connection cannot be established";
$my->query_simple("SET NAMES 'utf8'");


// Connect to LDAP is asked
if ($CONFIG['ldap_host'] && $CONFIG['ldap_host'] != "") {$ldap = new Ldap($CONFIG);}

// Auth handle method (mixed auth method) - Contains all user information, members, subscribers, etc
$auth = new LocalUser($CONFIG,$my,$ldap);

// Display Header
if (php_sapi_name() != 'cli') {
	include_once('include/header.php');
	}

// Enable swiftmailer
$mail = new Mail($CONFIG);
if (!$mail) $errors .= $mail->msg;

// Login form and menu
if (php_sapi_name() != 'cli') {
	include_once('include/login.php');
	include_once('include/menu.php');
	}

// Handle forms (need login first)
include_once('include/forms.php');

// Reload User Info if Forms changed something
if ($userid) {
        $userinfo = $auth->GetUserInfo($userid);
        $useremail = $userinfo['mail'];
        $external_id = $userinfo['external_id'];
        }

if (! $my->cx && php_sapi_name() != 'cli') {
	echo _('Configuration Howto');
	}
else if (! $my->cx && php_sapi_name() == 'cli') echo "Error connecting to mysql : ".$my->connect_error.PHP_EOL;

// Run scripts (only if called via command line)
if (php_sapi_name() == 'cli') {
	include_once('scripts/index.php');
}
else {
// Display data to user accessing via a browser
	include_once('template/help.php');
	if (!isset($_GET['page']))
		include_once('template/sessionlist.php');
	else if ($_GET['page']=='quickbook')
		include_once('template/quickbook.php');
	else if ($_GET['page']=='schedule')
		include_once('template/schedule.php');
	else if ($_GET['page']=='places')
		include_once('template/places.php');
	else if ($_GET['page']=='stats')
		include_once('template/stats.php');
	else if ($_GET['page']=='stats_users')
		include_once('template/stats_users.php');
	else if ($_GET['page']=='trainings')
		include_once('template/trainings.php');
	else if ($_GET['page']=='calendar')
		include_once('template/calendar.php');
	else if ($_GET['page']=='userspace')
		include_once('template/userspace.php');
	else if ($_GET['page']=='usermap')
		include_once('template/usermap.php');
//	else if ($_GET['page']=='attendee')
//		include_once('template/attendee.php');
	else if ($_GET['page']=='todolist')
		include_once('template/todolist.php');
	else if ($_GET['page']=='dolistock')
		include_once('template/dolistock.php');
	else if ($_GET['page']=='newmember')
		include_once('template/newmember.php');
	else if ($_GET['page']=='search')
		include_once('template/search.php');
	else if ($_GET['page']=='letters')
		include_once('template/letters.php');
	else if ($_GET['page']=='letters-groups')
		include_once('template/letters-groups.php');
	else if ($_GET['page']=='letters-addresses')
		include_once('template/letters-addresses.php');
	else if ($_GET['page']=='letters-display')
		include_once('template/letters-display.php');
        else if ($_GET['page']=='ldapadmin')
                include_once('template/ldapadmin.php');
	}


// Display Info/Warning/Errors
if (php_sapi_name() != 'cli') {
	// Button used to hide message
	$hidemessagebutton='<div class="hidemessagebutton">'._('Please click to hide this message').'</div>';
	// In case of an error, set css and emoji
	if ($errors) {
		$css_message='errors';$emoji='&#128546;';
		if (! in_array('errors',$CONFIG['ackbutton'])) $hidemessagebutton=NULL;
		}
	// Of it is a warning set related css and emoji
	elseif ($warnings) {
		$css_message='warnings';$emoji='&#129402;';
		if (! in_array('warnings',$CONFIG['ackbutton'])) $hidemessagebutton=NULL;
		}
	// Else, it is an info. Display of button is a setting.
	else {
		$css_message='infos';$emoji='&#128526;';
		if (! in_array('infos',$CONFIG['ackbutton'])) $hidemessagebutton=NULL;
		}
	echo '<div class="message '.$css_message.'">
			<div class="emoji">'.$emoji.'</div>
			<div class="errors">'.$errors.'</div>
			<div class="warnings">'.$warnings.'</div>
			<div class="infos">'.$infos.'</div>
			'.$hidemessagebutton.'
		</div>';
	}


// Close connection to mysql
$my->close();

// Display Footer
if (php_sapi_name() != 'cli') {
	include_once('include/footer.php');
}
?>
