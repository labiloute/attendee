// Const
function dateDiffInDays(a, b) {
	// Discard the time and time-zone information.
	var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
	return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}


var $_GET = {};
var locale = navigator.language || navigator.userLanguage; 

//console.log(locale);
document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }

    $_GET[decode(arguments[1])] = decode(arguments[2]);
});



function addParam(url, param, value) {
   // 2022-01-24 Function unused so far
   var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/g;
   var match, str = []; a.href = url; param = encodeURIComponent(param);
   while (match = regex.exec(a.search))
       if (param != match[1]) str.push(match[1]+(match[2]?"="+match[2]:""));
   str.push(param+(value?"="+ encodeURIComponent(value):""));
   a.search = str.join("&");
   return a.href;
}

// Start Jquery
$(document).ready(function()
	{



	// DatePicker FR CONFIG
	$.datepicker.regional['fr'] = {clearText: 'Effacer', clearStatus: '',
		closeText: 'Fermer', closeStatus: 'Fermer sans modifier',
		prevText: '&lt;Préc', prevStatus: 'Voir le mois précédent',
		nextText: 'Suiv&gt;', nextStatus: 'Voir le mois suivant',
		currentText: 'Maintenant', currentStatus: 'Voir le mois courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		monthStatus: 'Voir un autre mois', yearStatus: 'Voir un autre année',
		weekHeader: 'Sm', weekStatus: '',
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		dayStatus: 'Utiliser DD comme premier jour de la semaine', dateStatus: 'Choisir le DD, MM d',
		dateFormat: 'dd-mm-yy', 
		firstDay: 1, 
		initStatus: 'Choisir la date', isRTL: false
	};

	// Enable tabs on attendee template page
         $(function() {
            $( "#attendee-tabs-1" ).tabs();
         });

	// Toggle Plus/Minus sign when collapsed or not
	$(".session-plus-wrapper").on('click', function() {
		var expanded=$(this).attr('aria-expanded');
		//alert(expanded);
		if (expanded=='true' || expanded=='undefined') {
			$(this).children("i").removeClass("icono-check");
			$(this).children("i").addClass("icono-plus");
			}
		else {
			$(this).children("i").removeClass("icono-plus");
			$(this).children("i").addClass("icono-check");			
		}

	});

	// Attendee count of sessions display
	$('.attendee-email').on('click', function() {
		attendeeemail=$(this).html();
		DisplayUserSessions(attendeeemail);
	});

	// Change filter
	$('#session-title-select').on('change', function() {
		document.forms['sessions-title-filter'].submit();
	});


	// Display if it contains something
	if ($('div.message .errors').html() != ''){$('div.message').show();}
	else if ($('div.message .warnings').html() != ''){$('div.message').show();}
	else if ($('div.message .infos').html() != ''){
		$('div.message').show();
		if ($('div.hidemessagebutton').length == 0) {
			setTimeout(function() {$('div.message').hide('blind')}, 10000);
			}
		}
	// Hide fast if clicked
	$('div.message').on('click', function() { $('div.message').hide(); });
	//$('div#warnings').on('click', function() { $('div#warnings').hide(); });
	//$('div#errors').on('click', function() { $('div#errors').hide(); });

	// Email validation function
	function isEmail(email)
	{
	    var re = /\S+@\S+\.\S+/;
	    return re.test(email);
	}

	// Unused ?
	function storeconfig(data) {
		config=data;
	}


	function DisplayMessage(msg,type) {
		//console.log(variable);
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				translation: msg,
				force_locale : locale
				},
			success: function(data) {
				$("#" + type + " .content").html(data);
				DisplayInfo(type);
				console.log("Success querying translation : " + data);
				},
			error: function(data) {
				console.log("Error querying translation : " + data);
				$("#" + type + " .content").html(msg);
				DisplayInfo(type);
				}
		      	});
	}

	function GetTranslation(msg) {
		//console.log(variable);
		return $.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				translation: msg,
				force_locale : locale
				},
			success: function(data) {
				return data;
				},
			error: function(data) {
				console.log("Error querying translation : " + data);
				}
		      	});
		}


	//
        function DisplayUserSessions(attendeeemail) {
                $.ajax({
                        url : 'include/ajax.php',
                        type: 'POST',
                        data: {
                                attendeeemail: attendeeemail,
                                force_locale : navigator.language
                                },
                        success: function(data) {
                                $("#info .content").html(data);
				DisplayInfo('info');
                                console.log("Success querying user sessions infos : " + data);
                                },
                        error: function(data) {
                                console.log("Error querying user sessions : " + data);
                                }
                        });




        }


	// Check email and submit only if address is ok
	$(".register").submit(function(e) {
		var $form = $(this);
		var session_id = $form.attr('session_id');
		//alert(session_id);
		email = $('input[session_id=' + session_id + ']').val();
		//alert(isEmail(email));

		if (isEmail(email)==true) {
			//alert('OK');
			$(this).submit();
			$('input[session_id=' + session_id + ']').remClass('error');
			} 
		else {
			e.preventDefault();
			$('input[session_id=' + session_id + ']').addClass('error');
			DisplayMessage('wrong-email-address','errors');
			}
		});

		// Login window
		$('a.login-window').click(function() {
		//Getting the variable's value from a link 
		    var loginBox = $(this).attr('href');

		    //Fade in the Popup
		    $(loginBox).fadeIn(300);
		    //Set the center alignment padding + border see css style
		    var popMargTop = ($(loginBox).height() + 24) / 2;
		    var popMargLeft = ($(loginBox).width() + 24) / 2;
		    $(loginBox).css({
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		    });
		    // Add the mask to body
		    $('body').append('<div id="mask"></div>');
		    $('#mask').fadeIn(300);

		    return false;
		});

		// When clicking on the button close or the mask layer the popup closed
		$('a.close, #mask').on('click', function() {
			//alert("coucou");
		  $('#mask , .login-popup').fadeOut(300 , function() {
		    $('#mask').remove();
		});
		return false;
	});
	// End of Login window


	// ***************
	// PLACES PAGE
	// ***************
	$('#places-title-select').on('change', function() {
		id=$(this).val();
		LoadPlacesData(id);
	});


	function LoadPlacesData(place_id) {
		//console.log(id);
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				place_id: place_id
				},
			success: function(data) {
				$("#places-data").html(data);
				console.log("Success querying place data !");
				// Load Wysiwyg editor - http://jqueryte.com/documentation
				//$("#place-description").jqte();
				ClassicEditor.create( document.querySelector( '#place-description' ) );
				},
			error: function(data) {
				console.log("Error querying template : " + data);
				$("#places-data").html(data);
				}
		      	});

	}


	// ***************
	// SCHEDULE PAGE
	// ***************
	$('#schedule-title-select').on('change', function() {
		id=$(this).val();
		if (id=='')id=0;
		LoadTemplateData(id,null);
	});

	$(document.body).on('change', '#schedule input[name*=schedule-price]', function() {
		val=$(this).val();
		if (val=='0' || val=='-1') {
			console.log('Greying out price field');
			$('#schedule input#schedule-price').addClass('grey');
			}
		else {
			console.log('Ungreying out price field');
			$('#schedule input#schedule-price').removeClass('grey');
			$('#schedule input[name*=schedule-price]').prop('checked', false);
			}
		$('#schedule-price').val(val);
		
	});

	function LoadTemplateData(template_id,event_id) {
		//console.log(template_id);
		//console.log(event_id);
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				template_id: template_id,
				event_id: event_id,
				},
			success: function(data) {
				$("#template-data").html(data);

				if (template_id=="") {

					$("#template-buttons-wrapper :submit").attr("id","template-create");
					$("#template-buttons-wrapper :submit").attr("name","template-create");
					$("#template-buttons-wrapper :submit").attr("value","Create template");
					}
				else {

					$("#template-buttons-wrapper :submit").attr("id","template-update");
					$("#template-buttons-wrapper :submit").attr("name","template-update");
					$("#template-buttons-wrapper :submit").attr("value","Update template");
				}
				console.log("Success querying template !");
				// Load Wysiwyg editor - http://jqueryte.com/documentation
				//$("#schedule-comment").jqte();
				ClassicEditor.create( document.querySelector( '#schedule-comment'),{
					//plugins: [ Essentials, Paragraph, Bold, Italic, Alignment ]
					//extraPlugins: 'sourcedialog',
					//config.extraPlugins = 'sourcedialog';
					})
					.then( editor => {
						    console.log( 'Editor was initialized', editor );
						    myEditor = editor;
						} )
					.catch( error => {
						console.log( error );
					});
				},
			error: function(data) {
				console.log("Error querying template : " + data);
				$("#template-data").html(data);

				
				}
		      	});

		}

	// Date/time picker on Inputbox
	$( "#schedule-start" ).datetimepicker($.datepicker.regional['fr']);
	$( "#schedule-end" ).datetimepicker($.datepicker.regional['fr']);

	// Check that end datetime is higher than start datetime - DO NOT WORK YET - (Will be checked server-side instead)
	/*
	$(document.body).on('change', '#schedule input#schedule-start', function(e) {
		
		start=e.date;
		end=$( "#schedule-end" ).val();
		console.log(start);
		console.log(end);
		if (start!="" && end !="") {
			var d1 = new Date(start);
			var d2 = new Date(end);
			
			if (d2.getTime()>d1.getTime()) console.log("OK");
			else console.log("NOK : "+d2.getTime()+" "+d1.getTime()+"");
			}
		});
	*/

if (typeof calendar !== 'undefined') {
	var wHeight = $(window).innerHeight() * 0.85;
	$('#calendar').fullCalendar({
	height:wHeight ,
	events : calendar,
	eventRender: function(event, element) {
        	element.attr('title', event.animator);
    	}

	});
}

// Try to load empty template if selected page is "schedule"
var tpl_sel=$('#schedule-title-select').val();

console.log("[0] If a session ID is provided, we will load session : "+$_GET['event_id']);
if ($('#schedule-title-select').length || tpl_sel=="") {
	if (tpl_sel!="") {
		console.log("Loading template "+tpl_sel);
		LoadTemplateData(tpl_sel,null);
		}
	else
		{
		console.log("Loading empty template");
		LoadTemplateData(0,null);
		}
	}
else if ( $('#schedule-title-select').length==0 && $_GET['event_id']!=null && $_GET['page']=='schedule') {
	console.log("[1] Loading session "+$_GET['event_id']);
	LoadTemplateData(null,$_GET['event_id']);
	}
else if ( $('#schedule-modify').length==0 && $_GET['event_id']!=null && $_GET['page']=='schedule') {
	console.log("[2] Loading session "+$_GET['event_id']);
	LoadTemplateData(null,$_GET['event_id']);
	}
else if ($('#schedule-modify').length>=0) {
	var event_id=$('#schedule-modify').attr('event_id');
	console.log("[3] Getting event id from modification form : "+event_id);
	if (event_id!=0 && event_id!==undefined) {
		LoadTemplateData(null,event_id);
		}
	}

// Display Place if selector is selected
if ( $('#places-title-select').length>0) {
		place_id=$('#places-title-select').val()
		console.log("Loading place "+place_id);
		LoadPlacesData(place_id);
	}

// If there is only one session displayed, unfold the details automatically
var sessionsdisplayed = $('.session-title-container').length
//console.log("Number of sessions displayed : "+sessions_displayed);
if (sessionsdisplayed == 1) {
	$(".session-plus-wrapper").trigger('click');
	}

	/************ TODOLIST CODE ****************/
	/* The following code is executed once the DOM is loaded */
	defaulttxt="New element. Double clic to edit.";
	$(".todoList").sortable({
		axis		: 'y',				// Only vertical movements allowed
		//containment	: 'window',			// Constrained by the window
		update		: function(){		// The function is called after the todos are rearranged
		
			// The toArray method returns an array with the ids of the todos
			var arr = $(".todoList").sortable('toArray');
			    $(".todoList").disableSelection();
			
			// Striping the todo- prefix of the ids:
			
			arr = $.map(arr,function(val,key){
				return val.replace('todo-','');
			});
			
			// Saving with AJAX
			$.ajax({
					url : 'include/ajax.php',
					type: 'POST',
					data: {
						todolistaction:'rearrange',
						todolistpositions:arr,
						todolistcat:category
						},
					success: function(data) {
						console.log("Success rearranging todo entries !");
						},
					error: function(data) {
						console.log("Error rearranging todo entries : " + data);			
						}
				      	});
			//console.log(arr);
		},
		
		/* Opera fix: */
		
		stop: function(e,ui) {
			ui.item.css({'top':'0','left':'0'});
		}
	});
	
	// A global variable, holding a jQuery object 
	// containing the current todo item:
	var currentTODO;
	
	// Configuring the delete confirmation dialog
	$("#todolist-dialog-confirm").dialog({
		resizable: false,
		height:130,
		modal: true,
		autoOpen:false,
		buttons: {
			'Supprimer': function() {
				$.ajax({
					url : 'include/ajax.php',
					type: 'POST',
					data: {
						todolistaction:'delete',
						todolistid:currentTODO.data('id'),
						todolistcat:category
						},
					success: function(data) {
						currentTODO.fadeOut('fast');
						//$(data).appendTo('.todoList');
						console.log("Success deleting todo entry !");
						},
					error: function(data) {
						console.log("Error deleting Todo : " + data);			
						}
				      	});

				$(this).dialog('close');
			},
			'Annuler': function() {
				$(this).dialog('close');
			}
		}
	});

	// When a double click occurs, just simulate a click on the edit button:
	$('.todoList').on('dblclick','.todo',function(){
		$(this).find('a.edit').click();
	});
	
	// If any link in the todo is clicked, assign
	// the todo item to the currentTODO variable for later use.
	$('.todoList').on('click','.todo a',function(e){
		currentTODO = $(this).closest('.todo');
		//alert(currentTODO.attr('id'));
		currentTODO.data('id',currentTODO.attr('id').replace('todo-',''));
		e.preventDefault();
	});

	// Listening for a click on a delete button:
	$('.todoList').on('click','.todo a.delete',function(){
		$("#todolist-dialog-confirm").dialog('open');
	});
	
	// Listening for a click on a edit button
	$('.todoList').on('click','.todo a.edit',function(){
		var container = currentTODO.find('.text');
		// Display only when Ajax call to get translation has finished
		$.when(GetTranslation('Change category')).done(function(text) {

			//alert(text);
			var catbox = '<div class="catbox">'+ text + ' : <input type="text" class="category" value="'+category+'" /></div>';
			if (container.text() == defaulttxt){ container.text(''); } // Clear default text
			if(!currentTODO.data('origText'))
			{
				// Saving the current value of the ToDo so we can
				// restore it later if the user discards the changes:
				currentTODO.data('origText',container.text());
			}
			else
			{
				// This will block the edit button if the edit box is already open:
				return false;
			}
			//alert(container.text());
			$('<input type="text" class="textinput">').val(container.text()).appendTo(container.empty());
			// put cursor into inputbox		
			$('.todo input').focus();
			// Appending the save and cancel links:
			container.append(
				catbox+
				'<div class="editTodo">'+
					'<a class="saveChanges" href="#">Sauv.</a> ou <a class="discardChanges" href="#">Annuler</a>'+
				'</div>'
			);

		});
		
	});
	

	// Save when pressed enter
	$(".todoList").on('keypress','.todo',function(e) {
    		if(e.which == 13) {
	        //console.log('You pressed enter!');
			$('.todo a.saveChanges').click();
		}
	});


	// The Add New ToDo button:
	$('#todolist-addButton').on('click',function(e){

		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				todolistaction:'new',
				todolisttext:defaulttxt,
				rand:Math.random(),
				todolistcat:category
				},
			success: function(data) {
				$(data).appendTo('.todoList');
				console.log("Success creating todo entry !");
				},
			error: function(data) {
				console.log("Error creating Todo : " + data);			
				}
		      	});
		
		e.preventDefault();
	});


	// The cancel edit link:
	$('.todoList').on('click','.todo a.discardChanges',function(){
		currentTODO.find('.text')
					.text(currentTODO.data('origText'))
					.end()
					.removeData('origText');
	});
	
	// The save changes link:
	
	$('.todoList').on('click','.todo a.saveChanges',function(){
		//alert('Caca ');
		var text = currentTODO.find("input[type=text].textinput").val();
		var category = currentTODO.find("input[type=text].category").val();
		$.post("include/ajax.php",{todolistaction:'edit',todolistid:currentTODO.data('id'),todolisttext:text,todolistcat:category});
		
		currentTODO.removeData('origText')
					.find(".text")
					.text(text);
	});

	// The love button
	$('.todoList').on('click','.todo a.love',function(){
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				love:currentTODO.data('id')
				},
			success: function(data) {
				//alert("DATA :"+data+"FIN DATA");
				if (data == 'OK') {
					currentTODO.find(".actions a.love").addClass( "unlove" ).removeClass( "love" );
					lovecount = Number(currentTODO.find(".actions .lovecount").text());
					currentTODO.find(".actions .lovecount").text(lovecount + 1);
					console.log("Success loving this entry !");
					}
				},
			error: function(data) {
				console.log("Error loving this : " + data);			
				}
		      	});
		
	});

	// The love button
	$('.todoList').on('click','.todo a.unlove',function(){
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			data: {
				unlove:currentTODO.data('id')
				},
			success: function(data) {
				if (data == 'OK') {
					currentTODO.find(".actions a.unlove").addClass( "love" ).removeClass( "unlove" );
					lovecount = Number(currentTODO.find(".actions .lovecount").text());
					currentTODO.find(".actions .lovecount").text(lovecount - 1);
					console.log("Success Un-loving this entry !");
					}
				},
			error: function(data) {
				console.log("Error Un-loving this : " + data);			
				}
		      	});
		
	});


	$(document.body).on('focus', 'input.category' ,function(){
		$(this).autocomplete({
		source: function( request, response ) {
		//console.log(request.term)
		$.ajax({
			url : 'include/ajax.php',
			dataType: "jsonp",
			data: {
				searchcat: request.term,
				json: true
				},
			success: function( data ) {
				if (data!=null) {
					console.log(data);
					response(data);
					}
				},
			error: function(data) {
				console.log("Error Ajax JSON getting tag : " + data);
				},
			type: 'POST'
			});
		},
		autoFocus: true,
		minLength: 3     
		});
	});
	/*
	END OF TODOLIST SPECIFIC CODE
	*/

	/*
	START OF SEARCH USER SPECIFIC CODE
	*/

	// The love button
	$(document.body).on('click','span.skill-remove',function(){
		var skillid=$(this).attr("remid");
		console.log("Removing skill id "+skillid);
		$("#"+skillid).remove();
	});

	$(document.body).on('focus', 'input#autocomplete-skills' ,function(){
		$(this).autocomplete({
		source: function( request, response ) {
		console.log("Terme recherché :"+request.term);
		field=$(this);
		$.ajax({
			url : 'include/ajax.php',
			dataType: "jsonp",
			data: {
				searchskill: request.term,
				json: true
				},
			success: function( data ) {
				if (data!=null) {
					console.log(data);
					response(data);
					//field.val("");
					}
				},
			error: function(data) {
				console.log("Error Ajax JSON getting tag : " + data);
				},
			type: 'POST'
			});
		},
		autoFocus: true,
		minLength: 3,
		select: function( event, ui ) {
			console.log("Element sélectionné : ("+ui.item.value+") "+ui.item.label);
			selection='<div title="'+ui.item.label+'" class="selectedskill" id="skill-'+ui.item.value+'">';
			selection+='<span class="skill-remove" remid="skill-'+ui.item.value+'">';
			selection+='		&#10060;';
			selection+='<input class="selected-skillid" name="searchedskills[]" value="'+ui.item.value+'"/>';
			selection+='		'+ui.item.label+'';
			selection+='</div>';
			if ($("div#skill-"+ui.item.value).length == 0) {
				// Element does not exists, we append it
				$('div#selected-skills-id-list').append(selection);
				}
			$(this).val("");
			return false;
			},
		});
	});

	/*
	END OF SEARCH USER SPECIFIC CODE
	*/
	

	/*
	LETTER PAGE
	*/	
	// Try to load empty template if selected page is "schedule"
	//var tpl_sel=$('#letter-content').val();

	/*
	$(document.body).on('click','#toggleletterpreview',function(){
		console.log('Toggling !');
		//console.log(editor);
		myeditor.destroy();
		});
	*/


	$(document.body).on('click','#rmpagecolor',function(e){
		e.preventDefault();
		console.log('Clearing Page color !');
		$('#letter-pagecolor').val('#ffffff');
		});


	$(document.body).on('click','#rmbgcolor',function(e){
		e.preventDefault();
		console.log('Clearing Background color !');
		$('#letter-bgcolor').val('#ffffff');
		//console.log(editor);
		//LoadLetter();
		//console.log("Letter content "+$('#letter-content').length);
		});

	$(document.body).on('click','#lettersdisplayeditor',function(){
		//console.log(editor);
		LoadLetter();
		$(this).hide();
		//console.log("Letter content "+$('#letter-content').length);
		});

	// Load letter content if content is really small
	console.log("Longueur : "+$('#letter-source').text().length);
	if ($('#letter-source').length && $('#letter-source').text().length<=20) {
		console.log("Loading WYSIWYG editor !");
		LoadLetter();
		}
	//console.log("Loading letter "+$_GET['letter_id']);
	/*
	if ($('#letter-content').length || tpl_sel=="") {
		LoadLetter();
		}
	*/

	/* Load letter in Editor (CKEDITOR 5) */
	function LoadLetter(letter_id) {

		pagewidth=$('#letter-width').val();
		
		if (pagewidth != "") {
			$('#lettercontentwrapper').css('width', pagewidth);
			$('#lettercontentwrapper').css('margin', '0 auto');
			}
			
		data='{ "definitions" : [ "name": "Paragraph" , "element": "p" , "classes": [ "paragraph" ] } , { "name" : "Paragraph Title" , "element" : "p" , "classes" : [ "paragraph-title" ] } ]';
		data = '{"result":true, "count":42}';
		data='[ { "name" : "Paragraph", "element" : "p" , "classes" : [ "paragraph" ] } ]';
		//data = data.replace(/'/g, '"')
		var ckdefinitions=JSON.parse(data);
		//console.log("Valeur du truc : "+ckdefinitions[0].name);

		// Query content
		$.ajax({
			url : 'include/ajax.php',
			type: 'POST',
			async: false,
			dataType: 'json',
			data: {
				getckeditorjson: true,
				},
			success: function(data) {
				//$("#" + type + " .content").html(data);
				//DisplayInfo(type);
				console.log("Success querying Ckeditor Json content : " + data);
				ckdefinitions=JSON.parse(data);
				},
			error: function(data) {
				console.log("Error querying Ckeditor Json content : " + data);
				}
		      	});

			//console.log("Json definitions "+jsondefinitions.count);
			//testeditor = ClassicEditor.create( document.querySelector( '#letter-content'),{

		// Build editor with content
		MainEditor = ClassicEditor.create( document.querySelector( '#letter-source'),{
			//plugins: [ Essentials, Paragraph, Bold, Italic, Alignment ]
			//extraPlugins: 'sourcedialog',
			//config.extraPlugins = 'sourcedialog';
			//config.allowedContent:true,
			//editor.config.allowedContent = true;
			//extraAllowedContent: 'div(*)',
			//allowedContent: true,
			style: {
				definitions: ckdefinitions
				}
				})
			.then(editor => {
				myeditor = editor;
	  			})
			.catch( error => {
				console.log( error );
				});
				
		if ( $('div#letter-content').css('display') != 'none' && $('div#letter-content').css("visibility") != "hidden"){
			console.log('Hidding letter preview !');
			$('div#letter-content').hide();
			}
		}
		/*
		END OF LETTER PAGE
		*/	
	});

