��    j      l  �   �      	  
   	  =   	     Z	     f	     �	     �	     �	     �	  	   �	     �	      
     
     
     #
     0
     D
     U
     e
     }
     �
     �
     �
     �
     �
     �
  "   �
  !   �
          -     G     T     [  #   p     �  	   �     �     �     �     �     �               !     1  4   :     o     ~     �     �     �     �     �     �     �     �     �             !        ;     J  *   f     �     �     �     �     �     �  +   �          "     0     =     K     ^     z     �      �     �     �     �     �     �     �          ,     /     2     9     O     d     y     �     �     �     �     �     �     �     	          5     8     G     S  *  g  
   �  =   �     �  "   �     
     *  "   3     V     t     �     �     �     �     �  �   �  2   g     �  c   �          #     3     8     ?     L  #   X  I   |      �     �                 -     4  "   I     l  	   �  (   �  "   �     �     �     �           %      5     V  =   _     �     �     �     �     �     �                    '     6  X   =     �  *   �     �  "   �  �   �     |  /   �     �     �     �     �  +   �          %     3     @  7   N  P   �  8   �  8     3   I     }     �     �     �     �  S   �  )         J     M     P     W     m     �     �     �     �     �          !      2     S  &   a  &   �     �     �     �     �             Z   a   Q   =                    .   P             g   Y      [   _   N      h   %                     "              1       O   `      E   I   -   )   d       W   6   #   T       C          A                  8              $   +       ,   2                     @   i   j       7   '   &   X              	       J         B   M   (   V         D       ;   /   4   5   0   3       9              b   H       >               *      G   c       F       ]               S   U   ^   
   R   <      \          !      f       e   L             ?   K       :    All events An attendee confirmed his participation for one of your event Animated by Animation successfully booked Animation successfully released Animator Attendee deletion OK Attendee deletion failed Attendees Authentication successfull Availability Back to Calendar view Cancel event Configuration Howto Confirmation NOK Confirmation OK Confirmation email sent Count Create template Date Delete Delete-event Description Dolibarr-member-unknown Dolibarr-two-or-more-members-in-db Enter your email here to register Error booking animation Error releasing animation Event number Filter Free as in free beer Give this permalink to your friends Invalid authentication List view Modification NOK Modification OK Modify event Modify-event More information No animator yet No attendee yet Notify animator Password Please use the following tool to reset your password Practical info Price Profile sucessfully updated Register Release animation Restore event Save Save profile Schedule event Schedule-event Select Session canceled Sign in Subscription-is-valid-in-Dolibarr Take animation The event has been canceled The event you subscribed has been canceled Title Unable to find attendee Uncategorized Update template Use a template Username Your account is linked to an LDAP directory Your description Your password Your profile Your sessions already-registered animator-email-confirmation email-confirmation email-title end-of-subscription-mail-content end-of-subscription-mail-title free from link max-attendees-reached noanimator-mail-content noanimator-mail-title on or places schedule-creation-nok schedule-creation-ok schedule-deletion-ok schedule-end schedule-max-attendee schedule-modification-ok schedule-place schedule-start schedule-user-username software-title state-CONFIRMED state-WAIT4CONFIRM template-modification-ok to units in stock valid until wrong-email-address Project-Id-Version: Attendee
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-12-01 19:15+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n > 1);
Language: en
 All events An attendee confirmed his participation for one of your event Animated by Successfully booked this animation Successfully released animation Animator Successfully deleted participation Participation deletion failed Participants Connection successfull Availability Back to Calendar view Cancel event Your Attendee instance is not properly configured
Please have a look to the <a href="https://framagit.org/labiloute/attendee/wikis/home">Wiki</a> to know more Sorry, something went wrong with your confirmation Thanks for your confirmation ! An email has just been sent to you. Please click on the link provided to confirm your participation Count Create template Date Delete Delete event Description Member unknown in Dolibarr database There is more than one Dolibarr member registred with this email address. Type your email here to register Unable to book animation Error while releasing animation Event number Filter Free as in free beer Give this permaink to your friends Invalid authentication List view Something went wrong saving modification Successfully saved modification(s) Modify event Modify event More information No animator yet ! Take it mate ! No attendee yet Notify animator of event changes Password Please use the following external tool to reset your password Practical info Charge participation Profile sucessfully updated Register Release animation Restore event Save Save profile Schedule event Schedule event Select Sorry. This session/event is actually <b>canceled</b>. Please check details to know more Sign in Subscription is valid in Dolibarr database Take animation [%s] An event %s has been canceled Hi !
Sorry but an event you subscribed has been canceled.
Please contact team if required.
More info on following link.
Cheers.
 Title Unable to find you !. Did you already confirm ? Uncategorized Update template Use a template Username Your account is linked to an LDAP directory Your description Your password Your profile Your sessions Error. Maybe you are already registered on this session The attendee %s just confirmed his participation to your event %s starting on %s Please confirm your participation by clicking on this %s [%s] Please confirm your participation to event %s on %s Hello %s %s 
Please send money to subscribe again ! [YOU] Your subscription is over free from link Max number of attendees reached Hi folks ! Some sessions need your attention, because they have no animator yet :

 Session without animator in the next days on or places Event creation failed Schedule creation successfull Successfully deleted event Date and time for end of event Maximum number of attendees Successfully modified event Place Date and time for event start Session animator Attendee | manage your attendees Incription OK Waiting for confirmation from attendee Successfully saved changes to template to units in stock subscription valid until Please check email address 