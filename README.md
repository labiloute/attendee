# Attendee

## Prerequisites

- MySQL / MariaDB server with a dedicated database created for attendee
- PHP with following extensions enabled :
  - mysql
  - gettext
  - GD
  - intl

## Install

Clone the repository :
```
git clone https://framagit.org/labiloute/attendee.git
```

Create a MySQL / MariaDB database

Copy the sample config file

```
cd attendee
cp config/config_default.php config/{serverName}.php
```

where {serverName} is the name of your server, as used in HTTP requests (eg. localhost)

Fill all parameters in config/{serverName}.php (see comments inside the file for more details)

Run attendee.sql on the database.


