<?php
if (php_sapi_name() != 'cli') {
	echo "Must be run via cli. Sorry";
	exit;
}
// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

$date_formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE);
$time_formatter = new IntlDateFormatter($locale, IntlDateFormatter::NONE, IntlDateFormatter::MEDIUM);

// Get user emails
$sql = 'SELECT mail FROM users WHERE users.mail IS NOT NULL AND users.mail!=\'\' AND disabled IS NULL LIMIT 500';

$res_user=$my->query_assoc($sql);
//var_dump($res_user);
$nb_users = $my->num_rows;
//echo "$nb_users people have an email in userlist".PHP_EOL;


foreach ($argv as $key=>$arg) {
	echo "[INFO] Argument $key has value $arg".PHP_EOL;
}

$emails = array();
$emails_list = '';

if ($CONFIG['notify-noanimator-users'] && $nb_users>0) {
	while ($user=$res_user->fetch_assoc()) {
		$emails[]=$user['mail'];
		$emails_list.=$user['mail'].';';
	}
}

// If there is in config file an additionnal email to send the notification, we add it
if (isset($CONFIG['notify-noanimator-list'])) {
	$emails=array_merge($emails,$CONFIG['notify-noanimator-list']); // Or if option is not set, merge user emails and email stored in DB
}

// ***************************************
// Notify potential animator that sessions are empty
// ***************************************
if (in_array('all',$argv) or in_array('noanimator',$argv)) {
	$sql = '
	SELECT * FROM `sessions` WHERE
	date(start) < date(NOW()) + INTERVAL '.$CONFIG['notify-noanimator'].' DAY
	AND date(start) > date(NOW())
	AND username IS NULL
	AND canceled=\'0\'
	';
	//echo $sql;
	$dates = $my->query_assoc($sql);
	$nb_dates = $my->num_rows;

	if ($nb_dates > 0) {
		echo "$nb_dates sessions without animator in the next ".$CONFIG['notify-noanimator']." days".PHP_EOL;
		$noanimator = '';
		while ($row=$dates->fetch_assoc()) {
			$f_date=$date_formatter->format(strtotime($row['start']));
			$f_date.=' '._('from').' ';
			$f_date.=$time_formatter->format(strtotime($row['start'])); 
			if ($row['end'] != NULL) {
				$f_date.=" "._('to')." ";
				$f_date.=$time_formatter->format(strtotime($row['end']));
			}
			$noanimator.=''.$f_date.' : '.$CONFIG['base_url'].'?event_id='.$row['id'].PHP_EOL;
		}
	// Send email
	$mail->Send($emails,_('noanimator-mail-title'),_('noanimator-mail-content').$noanimator);
	}
	else {
		echo "No session without animator for now. Passing...".PHP_EOL;
	}
}

// *****************************************
// Disable a session if no animator has been set
// *****************************************
if (in_array('all',$argv) or in_array('auto_cancel_session',$argv)) {
	$sql = '
	SELECT * FROM `sessions` WHERE
	date(start) = date(NOW())
	AND (username IS NULL
	OR username=\'\')
	AND canceled=\'0\'
	LIMIT 100
	';
	//echo $sql;
	$dates = $my->query_assoc($sql);
	$nb_dates = $my->num_rows;

	//echo $nb_dates;
	if ($nb_dates > 0) {
//		echo "$nb_dates".PHP_EOL;
		$noanimator = '';
		while ($row=$dates->fetch_assoc()) {
			$sql2 = 'SELECT * from attendees WHERE session_id = \''.$row['id'].'\' ';
			$attendees = $my->query_assoc($sql2);
			$nb_attendees = $my->num_rows;
			echo "The session ".$row['title']." with id ".$row['id']." has $nb_attendees subscribers. We sent an email to each attendee to notify them that the event is canceled. And then we cancel it.".PHP_EOL;
			while ($row2=$attendees->fetch_assoc()) {
				echo "Sending a notice to ".$row2['email'].PHP_EOL	;
				$email=array($row2['email']);
				$link= $CONFIG['website-link'].'/attendee/?event_id='.$row['id'].''; //TODO - _SERVER variable is unusable. Is there something clever that can be done ?
				$title=sprintf(_('The event has been canceled'),$CONFIG['website-name'],$CONFIG['website-name']);
				$message=sprintf(_('The event you subscribed has been canceled'),$CONFIG['website-name']);
				$mail->Send($email,$title,$message.$link);//_('noanimator-mail-content')
				}

			// Canceling event-session
			echo "Canceling session with id ".$row['id'].PHP_EOL;
			$sql3 = 'UPDATE sessions SET canceled=\'1\' WHERE id=\''.$row['id'].'\' LIMIT 1';
			$my->query_simple($sql3);
			}
			//$noanimator.=''.$f_date.' : https://'.$argv[1].'/attendee/?event_id='.$row['id'].PHP_EOL;
		}
	else {
		echo "There is no session to cancel today...".PHP_EOL;
	}
}


// ***************************************
// Send notification for end on Dolibarr Validity
// ***************************************
if (in_array('all',$argv) or in_array('expired_subscription',$argv)) {
	echo "[INFO] You can set up date using arv 3 and 4 with dates formatted like 20201231".PHP_EOL;
	echo "[INFO] Exemple : php index.php <instance> expired_subscription 2020-01-01 2020-01-31".PHP_EOL;
	$start_date=date('Y-m-d');
	$end_date=date('Y-m-d');
	include_once('include/dolibarr.class.php');
	$Dolibarr=new Dolibarr($CONFIG);
	//
	if ($argv[3] && $argv[4]) {

		echo "Start date : $argv[3]".PHP_EOL;
		echo "To End date : $argv[4]".PHP_EOL;
		$date_en = $argv[3];
		$end_date = $argv[4];
	}
	else $date_en=$start_date;

	while (True) {
		$date = date("Ymd", strtotime($date_en));
		echo "[INFO] Using date : ".$date_en." converted to ".$date.PHP_EOL;
		// Get list of terminated subscriptions
		$list=$Dolibarr->get_list_of_term_subscriptions(False,$date);

		if ($list) {
			foreach($list as $user) {
				//var_dump($user);
				echo "[INFO] Subscription for ".$user['firstname']." ".$user['lastname']." (".$user['email'].") (subscriber number ".$user['id']." has to be renewed since ".$date_en." !".PHP_EOL;
				if ($CONFIG['dolibarr']['mail_copy']) {
					$dest=array($user['email'],$CONFIG['dolibarr']['mail_copy']);
					}
				else 	{
					echo '[WARNING] configuration variable \'$CONFIG[\'dolibarr\'][\'mail_copy\']\' is not set';
					$dest=array($user['email']);
					}
				// Send email
				if ($CONFIG['msg']['end-of-subscription-mail-content']) {
					$content=str_replace("[FIRSTNAME]",$user['firstname'],$CONFIG['msg']['end-of-subscription-mail-content']);
					$content=str_replace("[LASTNAME]",$user['lastname'],$content);
					//$content=sprintf($CONFIG['msg']['end-of-subscription-mail-content'],,$user['lastname']);
					}
				else 
					$content=_('end-of-subscription-mail-content');

				$mail->Send($dest,sprintf(_('end-of-subscription-mail-title'),$CONFIG['website-name']),$content);
				}
			}
		else
			echo "Nobody has a subscription that ends on ".$date_en.PHP_EOL;

		// Break if end date reached or if start and end date has not been given
		if ($date_en == $end_date) break;
		if (! $argv[3] or ! $argv[4]) break;
		$date_en = date("Y-m-d",strtotime("+1 day", strtotime($date_en)));
		}

	}


// ***************************************
// Put Stock content in cache
// ***************************************
if (in_array('all',$argv) or in_array('dolistock',$argv)) {
	include_once('dolistock.php');
	}
	
// ***************************************
// Send letters to list
// ***************************************
if (in_array('all',$argv) or in_array('letters',$argv)) {
	include_once('letters.php');
	}

?>
