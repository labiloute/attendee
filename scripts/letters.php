<?php
$Letters=new Letters($CONFIG,$my);

// Set shipping limit
($CONFIG['letters-shipping-limit'])?$limit=$CONFIG['letters-shipping-limit']:$limit=False;

// Get shippings
$shippings=$Letters->GetShippings(False,'NULL',True,$limit);

// Notice about limit
echo "[DEBUG] We will send a maximum of ".$Letters->limit." mail this time.".PHP_EOL;

// Open Ckeditor CSS File
//$css = file_get_contents('css/ck-content-styles.css');
// Remove ckeditor class
//$css = str_replace(".ck.ck-content", "", $css);

// Put CSS content into head and script tags
//$header = "<head>".PHP_EOL."<style>".PHP_EOL.$css.PHP_EOL."</style>".PHP_EOL."</head>".PHP_EOL.PHP_EOL;

$header=$Letters->MailHeader();




// Foreach
while ($row = $shippings->fetch_assoc()) {
	//var_dump($row);
	if (!$row['address']) continue;
	// Construct mail content based on query
	$full_content=$Letters->ConstructMail($row,$header);
	
	// Give information to cli
	echo '['.$row['letter_id'].'] Shipping letter "'.$row['letter_title'].'" to "'.$row['address'].'" : ';

	// Shipping
	$shipping=$mail->Send($row['address'],$row['letter_title'],$full_content);

	// Display result
	if ($shipping) {echo "[OK]".PHP_EOL;$status=1;} else {echo "[ERROR] ".$mail->msg.PHP_EOL;$status=0;}
	
	// Keep a trace of shipping
	$Letters->SetShippingsDone($row['letter_id'],$row['address_id'],$status);
	
	// Eventually Disable query if no mail left
	$pending=$Letters->GetPending($row['letter_id']);
	if ($pending==0) {
		echo "[".$row['letter_id']."] Disabling letter since there is no pending mail to send.".PHP_EOL;
		$Letters->DisableLetter($row['letter_id']);
		}

	//$Letters->DisableFinishedShippings($row['letter_id']);
	}
//echo $txt;

?>
