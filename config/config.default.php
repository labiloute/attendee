<?php

// DB Config - Overriden by Env var if needed
$CONFIG['dbhost'] = "localhost";
$CONFIG['dbuser'] = "root";
$CONFIG['dbpass'] = "pwd";
$CONFIG['dbname'] = "attendee";

// Base config
$CONFIG['base_folder'] = 'data'; // The folder containing all your data - One folder per instance !
$CONFIG['base_url'] = 'https://yourwebsite.org/attendee/'; // Your web site base URL (sent in confirmation emails, for instance) - with ending slash "/"
$CONFIG['renew_url'] = 'https://yourwebsite.org/renew'; // Url to renew subscription - if empty, will link to internal renew page

// LDAP config
$CONFIG['ldap_host'] = "";// Leave empty or comment to disable LDAP
$CONFIG['ldap_admin_dn'] = 'cn=admin,dc=foo,dc=bar';// leave empty for anonymous bind
$CONFIG['ldap_admin_pwd'] = '';
$CONFIG['ldap_port'] = 389;
$CONFIG['ldap_domain'] = '';
$CONFIG['ldap_base_dn'] = 'ou=users,dc=foo,dc=bar';
$CONFIG['ldap_filter'] = '';
$CONFIG['ldap_external_pwd_tool'] = ''; // path to an external password tool
$CONFIG['ldap_auth_attribute']=''; // Use this ldap attribute to store Role Based Access Control as defined in RBAC doc : https://ldapwiki.com/wiki/RBAC. Exemple 'businesscategory'.
$CONFIG['ldap_map_ADMIN']='';// or map this Ldap group to local ADMIN group (better use RBAC)
$CONFIG['ldap_map_MANAGER']='';// and Map this Ldap group to local MANAGER group (better use RBAC)
$CONFIG['ldap_map_ANIMATOR']='';// and Map this Ldap group to local ANIMATOR group (better use RBAC)
$CONFIG['ldap_allowed_description_metadata']=array('TITLE','DATA','USERS'); // Fields allowed in LDAP Organisational Unit description (specific to Marmule)
$CONFIG['ldap_disabled_field']='mail'; // Field used to set a disabled user
$CONFIG['ldap_disabled_pattern']='DISABLED-'; // Pattern added to the field to set a user as disabled
$CONFIG['ldap_user_identifier']='uid'; // samaccountname (Active Directory) or uid (Simple LDAP), or something else !
$CONFIG['ldap_allowed_rbac_values']=array('USER','MANAGER','ADMIN'); // Allowed values for RBAC access control like : SERVICE:VALUE


// Instance Logo and settings
$CONFIG['logo'] = 'images/attendee_logo.png'; // Relative to base_folder
$CONFIG['website-link']= 'https://yourwebsite.org'; // link to go back to your website
$CONFIG['website-name']= 'Your Website'; // Website display Name
$CONFIG['display-footer']=True;

// Locale settings
$CONFIG['force_locale'] = 'en'; // Used to force local
$CONFIG['timezone']='Europe/Paris';
$CONFIG['addtocalendarbutton']=array('iCal','Yahoo');
$CONFIG['sessiondateformat'] = 'EEEE dd MMM yyyy' ; // Used to display date on a specific format, based on documentation of "IntlDateFormatter" class
$CONFIG['sessiontimeformat'] = 'HH:mm' ; // Used to display date on a specific format, based on documentation of "IntlDateFormatter" class

// SwiftMailer path & Config
$CONFIG['enable_mail_confirmation'] = True;//Send a confirmation link to attendee
$CONFIG['swiftmailer_path'] = 'swiftmailer/lib/swift_required.php';// Path to swiftMailer
$CONFIG['swiftmailer_host'] = 'smtp.yourprovider.com';// SMTP host
$CONFIG['swiftmailer_port'] = 25;// SMTP port
$CONFIG['swiftmailer_username'] = 'username';// SMTP username
$CONFIG['swiftmailer_password'] = 'pass';// SMTP pass
$CONFIG['swiftmailer_encryption'] = False;// SMTP encryption - possible values :  False, 'ssl' or 'tls'
$CONFIG['swiftmailer_sender_name'] = 'My Sender'; // SMTP sender name
$CONFIG['swiftmailer_sender_email'] = 'myuser@foobar.com'; //

// Calendar view settings
$CONFIG['calendar_history'] = 3; // History to keep in calendar view (in month)
$CONFIG['calendar_upcoming'] = 3; // Upcoming events to display in calendar view (in month)
$CONFIG['addtocalendarbutton']=array('Apple','Google','iCal','Microsoft365','MicrosoftTeams','Outlook.com','Yahoo');

// TodoList
$CONFIG['enable-todolist'] = True;

// Locale
$CONFIG['currency_symbol']='€';
$CONFIG['rewrite_zero_to_free']=True;// Zero euro session = free as in free beer
$CONFIG['rewrite_minus_one_to']='Open to all members'; // Minus one meens "Open to all members"
$CONFIG['timezone']='Europe/Paris';

// Quick booking
$CONFIG['enable-quickbook']=True;

// Session List registering
$CONFIG['emailplaceholder']=_('username').'@'._('provider').'.'.$CONFIG['force_locale'];

// Different Attendee States
$CONFIG['states'] = array('WAIT4CONFIRM','CONFIRMED','FREE','PAID','ABSENT','CANCELED');
$CONFIG['state_confirmed']='CONFIRMED';
$CONFIG['state_canceled']='CANCELED';

// Notification config
$CONFIG['notify-noanimator'] = 3; // Notify N days before that no animator is set
$CONFIG['notify-noanimator-list']=False; // Add those adresses to notification list, to the email addresses found in ldap (if set). Should be an array
$CONFIG['notify-noanimator-users'] = True; // Notify users stored in database (when email field is set)
$CONFIG['ackbutton']=array('warnings','infos','errors'); // Display button to ack notifications (array of one of more of these : errors|warnings|infos)

// Ips allowed to see and use the newmember page
$CONFIG['newmember-security'] = True;
$CONFIG['newmember-networks'] = array(0=>'127.0.0.1',1=>'192.168.0.',2=>'192.168.1.',3=>'10.1.1.',4=>'10.1.0.');


// Letters config
$CONFIG['letters-shipping-limit']=20;
$CONFIG['ckeditor-styles-policy']='merge'; // ckeditor policy : 'merge' for merging default and custom, 'custom' for custom only, 'default' or False form default config only

// Misc practical info
$CONFIG['practical-info']='
Fill here to add practical info to all sessions
It can be on multiple line, obviously.
';
$CONFIG['members-validity']=365; // Validity of a member subscription (days)

// Stock module
$CONFIG['stock']['display_without_image'] = True; // Display or not a stock item that has no image associated
$CONFIG['stock']['display_stock_null'] = True; // Display or not a stock item that has a stock to zero
$CONFIG['stock']['uncategorized'] = 'Uncategorized'; // Label used for uncategorized items - can be _('Uncategorized') too
$CONFIG['stockcatweight'] = array('Computers'=>50,'Laptop'=>40,'Screens'=>30); // Set a category weight (used when Refreshing stock from source)
$CONFIG['stock']['information'] = 'If you are interested by our stock, please feel free to contact us : <a href="mailto:'.$CONFIG['swiftmailer_sender_email'].'">'.$CONFIG['swiftmailer_sender_email'].'</a>';// Stock disclaimer


// Geocoding with Nominatim
$CONFIG['nominatim_url'] = 'http://nominatim.openstreetmap.org/search?';
$CONFIG['usermap_logo'] = $CONFIG['logo']; // Path to logo that will be displayed in usermap

//******************/
// Dolibarr APIs ***/
//******************/
$CONFIG['dolibarr']['apikey']='';
$CONFIG['dolibarr']['url'] = '';
$CONFIG['dolibarr']['api'] = $CONFIG['dolibarr']['url'].'/api/index.php/';
$CONFIG['dolibarr']['mail_copy']=$CONFIG['swiftmailer_sender_email']; // Send a carbon copy of the email to this adress on every subscriber mail when not valid anymore
$CONFIG['dolibarr']['login_require_valid_sub'] = False; // Force local user to have a valid Dolibarr membership before login in
$CONFIG['dolibarr']['login_ldap_require_valid_sub'] = False; // Force ldap user to have a valid Dolibarr account before login in
$CONFIG['dolibarr']['image_field'] = 'note'; // This Dolibarr field that stock the image of the product
$CONFIG['dolibarr']['category_field'] = 'note_private'; // This Dolibarr field that stock product category
$CONFIG['dolibarrpaiementmethod']=array(3=>'Cash',4=>'Check');
$CONFIG['dolibarrpaiementtype']=array(3=>'CASH',4=>'CHECK');
$CONFIG['dolibarr']['paiementlabel']='Subscription 12 month';
$CONFIG['dolibarr']['subscriptionamount']=20;

//*************************/
// Custom HTML Messages ***/
//*************************/

// Css, when supported - form exemple, seems to not be supported by Gmail. In this cas, please insert style directly in html elements.
$CONFIG['msg']['css']='
<style type="text/css" media="screen">
a.boutton {text-color:white;font-weight:bold;}
a.boutton:link { color:white; text-decoration: none;}
a.boutton:visited { color:white; text-decoration: none; }
a.boutton:hover { color:white; text-decoration: none;}
a.boutton:active { color:white; text-decoration: underline;}
div.confirmregistration {background-color:green;border:1px solid #004d00;padding:0.5em 1em;width:30%;margin:0 auto;text-align:center;border-radius:1em;}
div.confirmregistration:hover {background-color :#004d00;}
div.confirmregistration:hover {background-color :#004d00;}
div.cancel {text-align:center;background-color:orange;border:1px solid #d88b00;padding:0.5em 1em;width:30%;margin:0 auto;border-radius:1em;}
div.cancel:hover {background-color:#d88b00;}
div.cancel a.bouton {font-weight:normal;}
</style>
';
$CONFIG['msg']['signature']=_('Team').' <a href="'.$CONFIG['website-link'].'">'.$CONFIG['website-name'].'</a>';


// Will be substitued : [EVENTNAME] [EVENTDATE] [CONFIRMLINK] [CANCELLINK]
$CONFIG['msg']['event-subscription-confirmation-test']='
<style type="text/css" media="screen">
* {font-family:"Trocchi";}
a.button:link { color:white; text-decoration: none;}
a.button:visited { color:white; text-decoration: none;}
a.button:hover { color:white; text-decoration: none;}
a.button:active { color:white; text-decoration: underline;}
.strong {color:#7c221a;}
</style>

<body style="background-color:#f5f5f5;" />

<div style="width:80%;margin: 0 auto;background-color:white;border:1px solid grey;padding:1em;">

<p>Hi,</p>

You subscribed to an event "[EVENTNAME]" which will be on [EVENTDATE].

Please confirm or cancel using following links :
<br />
<br />
<div class="confirm" style="background-color:#097700;border:2px solid #79be21;color:white;padding:0.5em 1em;margin:0 auto;width:50%;text-align:center;border-radius:0.3em;">
        <a class="button" href="[CONFIRMLINK]">
                Confirm
        </a>
</div>

<br />

<div class="cancel" style="background-color:#b2564e;border:2px solid #be2126;color:white;padding:0.5em 1em;margin:0 auto;width:50%;text-align:center;border-radius:0.3em;">
	<a class="button" href="[CANCELLINK]">
		Cancel
	</a>
</div>
<br /><br />
We will be pleased to see you there !<br /><br />
Team '.$CONFIG['website-name'].'
</div>
</body>
';



$CONFIG['msg']['email-confirmation-password-reset']='
<h2 style="color:grey;>%s</h2>
Hello !<br />
You requested to reset your password (if you didn\'t, simply delete this email).<br />
To reset your password, please on the link below to finish the modification process.<br />
<br />
';

$CONFIG['msg']['email-confirmation-password-reset-buttons'] ='<div class="confirmregistration"><a class="boutton" href="%s">%s</a></div>';


$CONFIG['msg']['email-confirmation-userspace']='
<h2 style="color:grey;">%s</h2>
Hello !
You requested creation of userspace for our organisation. Many thanks.<br />
If you didn\'t requested anything, please delete this email.<br /><br />
Please click on the link below in order to complete your userspace creation process :<br />';


$CONFIG['msg']['email-confirmation-userspace-buttons']='<div class="confirmregistration"><a class="boutton" href="%s">%s</a></div>';


$CONFIG['msg']['start-of-subscription-disclaimer']='
<h3>Please read this carefully !</h3>
<strong>Usage of this premises</strong><br />
Here is your disclaimer content<br /><br />
';

$CONFIG['msg']['start-of-subscription-explanation']='
Here is a HTML that describe why it is important du subscribe to your organization.
';

// [FIRSTNAME] and [LASTNAME] can be substituted
$CONFIG['msg']['end-of-subscription-mail-content']='
Here is the content of the email received by your subscribers when subscription comes to its end.
Please note that [FIRSTNAME] and [LASTNAME] can be substituted
';


//$CONFIG['msg']

?>
