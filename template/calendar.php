<!--
<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>
-->
<?php
$sql = 'SELECT 
sessions.id as session_id,
templates.id as template_id,
templates.title as template_title,
templates.price as template_price,
templates.comment as template_comment,
templates.image as template_image,

sessions.start as start,
sessions.end as end,
sessions.image as session_image,
sessions.comment as session_comment,
sessions.title as session_title,
sessions.max_attendee as session_max_attendee,
sessions.price as session_price,
sessions.canceled as session_canceled,

/*Count(attendees.id) as attendees_count,*/
Count(confirmed.id) as attendees_count,

users_session.username as session_animator_username,
users_session.user_last_name as session_animator_last_name,
users_session.user_first_name as session_animator_first_name,
users_session.user_comment as session_animator_comment

FROM `sessions`

LEFT JOIN templates ON sessions.template_id = templates.id
LEFT JOIN users as users_session ON sessions.username=users_session.username
/*LEFT JOIN attendees on attendees.session_id=sessions.id*/
LEFT JOIN (SELECT * FROM attendees WHERE attendees.state=\''.$CONFIG['state_confirmed'].'\') as confirmed on confirmed.session_id=sessions.id

WHERE 1
/* Evite de compter les personnes dont le statut est annulé/en attente/absent TODO */
/*AND (attendees.state IS NULL or attendees.state=\''.$CONFIG['state_confirmed'].'\')*/
'.$sql_filter.'
GROUP BY sessions.id
ORDER BY sessions.start ASC';

//echo $sql;
$res=$my->query_assoc($sql);

$nb_sessions = $my->num_rows;

// Create calendar array
$calendar=array();

// Create default settings
$calendar['header'] = array(
        'left'=>'prev,next today',
        'center'=>'title',
        'right'=>'month,agendaWeek,agendaDay,listWeek'
	);
$calendar['defaultDate']=date('Y-m-d'); // Default is now
$calendar['navLinks']=True;
$calendar['editable']=False;
$calendar['eventLimit']=true;
//$calendar['aspectRatio']='1';
$calendar['height']='parent';
//$calendar['Timezone']=array('timezone'=>'Europe/Paris');

// Create events list
$calendar['events'] = array();


// Fill up events list
while ($r = $res->fetch_assoc()) {
	($r['session_title'])?$session_title=$r['session_title']:$session_title=$r['template_title'];

	// TODO if somthing fail with datetime
	//$datetime = new DateTime('2010-12-30 23:21:46');
	//echo $datetime->format(DateTime::ATOM); // Updated ISO8601

	//echo $r['session_animator_username'];
	if ($r['session_canceled'] == '1') { $bg='red';	$animator = _('Session canceled');}
	else if ($r['session_animator_username']==NULL) {
		$bg='orange';
		$animator = _('No animator yet');
		}
	else if ($r['attendees_count']==$r['session_max_attendee']) {
		$bg='grey';
		$animator = _('Session full');
		}
	else {
		$bg='green';
		$animator = _('Animated by').' '.$r['session_animator_username'];
		}
	$calendar['events'][] = array(
			'start'=> $r['start'],
			'end'=> $r['end'],
			'title'=> $session_title,
			'url'=> '?event_id='.$r['session_id'],
			'backgroundColor'=> $bg,
			'animator'=> $animator
			);
	}

$json_calendar = json_encode($calendar);

//-debug-echo $json_calendar;

echo '<script type="text/javascript">

	var calendar = '.$json_calendar.';

</script>';

?>
 	<div class="calendar-recommandations">Cliquez sur un évenement pour en savoir plus...</div>
<div class="calendar-wrapper">	
	<div id='calendar'></div>
</div>
