<?php
$Event=new Event($CONFIG,$my);
//echo $Event->Html_Session_Filter();
?>

<?php
// This function give to time a reading comfort : it translate 9:30:00 to 9h30, depending on locale
function format_time($locale,$time) {
	if ($locale=='fr' or $locale='fr_FR' or $locale='fr_FR.utf-8') {
		if (substr($time,-3)==':00') return substr($time,0,2).'h';
		else return substr($time,0,2).'h'.substr($time,-2);
		}
	else return $time;
	}

?>


<?php

if ($_GET['event_id']) {
	$event_id_action='event_id='.$_GET['event_id'];
	}


// Define filters for this page
$sql_filter='AND sessions.quickbook=\'1\' AND users_session.username IS NOT NULL'; // Filter on quick booking checkbox, and only events with animator.
$sql_date_filter='AND DATE(sessions.start) > NOW() - INTERVAL 1 DAY'; // Only future events

?>

<?php

// Get events
$res=$Event->get_Events($sql_filter,$sql_date_filter);

// Only if query succeed
if ($res) {

$nb_sessions = $my->num_rows;
if (isset($CONFIG['quickbook']['information'])) $quickbook_information=$CONFIG['quickbook']['information'];
else $quickbook_information=_('Here are the next time slots available');

?>


<div id="template-information"><?php echo $quickbook_information; ?></div>
<!-- For each session -->
<div id="quicksessions">
<?php $i=0; ?>
<?php while ($row = $res->fetch_assoc()) {
	// Select good values
	($row['session_title'])?$session_title=$row['session_title']:$session_title=$row['template_title'];
	($row['session_image'])?$session_image=$row['session_image']:$session_image=$row['template_image'];
	//(is_file($CONFIG['base_folder'].'/images/'.$session_image))?$session_image=$CONFIG['base_folder'].'/images/'.$session_image:$session_image='images/default_event.png';// Fallback to default logo if image file does not exists
	//($row['session_comment'])?$session_comment=$row['session_comment']:$session_comment=$row['template_comment'];
	//($row['session_price']!=NULL)?$session_price=$row['session_price']:$session_price=$row['template_price'];

	($row['session_animator_first_name'])?$session_animator_first_name=$row['session_animator_first_name']:$session_animator_first_name=NULL;
	($row['session_animator_last_name'])?$session_animator_last_name=$row['session_animator_last_name']:$session_animator_last_name=NULL;
	($row['session_animator_comment'])?$session_animator_comment=$row['session_animator_comment']:$session_animator_comment=NULL;
	($row['session_place'])?$session_place=nl2br($row['session_place']):$session_place = _('Place has not been defined yet');

	// Other rewrites
	if ($CONFIG['rewrite_zero_to_free'] && $session_price==0) {$session_price=_('Free as in free beer');}
	else if ($session_price=='-1') {$session_price=$CONFIG['rewrite_minus_one_to'];}
	else $session_price=$session_price.$CONFIG['currency_symbol'];

	// Query attendees (confirmed and unconfirmed)
	$sql_attendees = 'SELECT * from attendees WHERE session_id=\''.$row['session_id'].'\' LIMIT 100';
	//echo $sql_attendees;
	$res_attendees=$my->query_assoc($sql_attendees);
	$nb_attendees_confirmed=0;
	$nb_attendees_other=0;
	while ($r=$res_attendees->fetch_assoc()) {
		if ($r['state']==$CONFIG['state_confirmed'])$nb_attendees_confirmed++;
		else $nb_attendees_other++;
		}
	($nb_attendees_confirmed==0)?$txt_attendees = _('No confirmed attendee yet'):$txt_attendees=_('Attendees');
	$txt_attendees.=' ('.$nb_attendees_other.' '._('waiting for confirmation').')';
	$free_places = $row['session_max_attendee'] - $nb_attendees_confirmed;

	// Go further only if there is room space left
	if ($free_places<1) continue;
	?>

	<?php ($_GET['event_id']==$row['session_id'] && $_GET['event_id']!=NULL)?$full=' full':$full=''; ?>
	<div class="quicksession">
	<!-- Define locale -->

	<?php
	$dt = new DateTime;
	// Pick up date format in config
	($CONFIG['sessiondateformat'])?$session_date_format=$CONFIG['sessiondateformat']:$session_date_format=NULL;
	($CONFIG['sessiontimeformat'])?$session_time_format=$CONFIG['sessiontimeformat']:$session_time_format=NULL;
	$date_formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, NULL, NULL, $session_date_format);
	$time_formatter = new IntlDateFormatter($locale, IntlDateFormatter::NONE, IntlDateFormatter::MEDIUM, NULL, NULL,  $session_time_format);
	//$formatter->setPattern('E d.M.yyyy'); // to specify the pattern
	?>

	<!--// Display session desc -->
		<!--<div class="quicksession-title-container">
			<span class="session-title-wrapper"><a href="?event_id=<?php echo $row['session_id']; ?>"><?php echo $session_title ?></a></span>
		</div>-->

		<!--<div class="session-image-container" style="background-image:url('<?php echo $session_image;?>');height:20em;  background-repeat: no-repeat;background-size: cover;background-position: center;">-->
			<a href="?event_id=<?php echo $row['session_id']; ?>">
			<div class="quicksession-element">
				<span class="quicksession-datetime">
			<?php
				echo $date_formatter->format(strtotime($row['start']));
				//echo '<br />';
				echo ' '._('from').' ';
				echo format_time($locale,$time_formatter->format(strtotime($row['start'])));
				if ($row['end'] != NULL) {
					echo " "._('to')." ";
					echo format_time($locale,$time_formatter->format(strtotime($row['end'])));
					}
				echo "</span>";
				echo '<span class="quicksession-infos">';
				echo "".$free_places.' '._('places').' '._('free').' - '._('animated by').' '.$session_animator_first_name.' '.$session_animator_last_name.'';
				echo "</span>";
				$i++;// Count how many session displayed
			?>
			</div>
	</div><!-- Session data -->
	</a>
	<?php 
	}  // Each session loop
	?>
	</div><!-- / All session container -->
	<?php
	} // If $res

	if ($i==0) {
	echo '	<div class="quicksession">'._('Sorry, there is actually no session available (of this kind)').'</div>';
	}
	?>
