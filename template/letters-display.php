<?php
// This page is freely available
//if (!$_SESSION['username']) {echo '<div class="forbidden">'._('Forbidden').'</div>';return;}
?>
<?php
// INIT
$Letters=new Letters($CONFIG,$my);

//var_dump($_POST['letter-enabled']);

?>

<?php

// FORMS

// Table letters
if ($_GET['display_id'])$display_id=$_GET['display_id'];
if ($_GET['unsubscribe'])$unsubscribe=$_GET['unsubscribe'];

// Exit if nothing requested
if (!$display_id && !$unsubscribe) exit;

//echo $unsubscribe;

$letter=$Letters->GetLetter(False,$display_id);


//var_dump($letter);
$letter_title=$letter['letter_title'];
$letter_width=$letter['letter_width'];
$letter_bgcolor=$letter['letter_bgcolor'];
$letter_pagecolor=$letter['letter_pagecolor'];

if ($letter_width) $style_width='width:'.$letter_width.'px;';
if ($letter_bgcolor) $style_bgcolor='background-color:'.$letter_bgcolor.';';
if ($letter_pagecolor) $style_pagecolor='background-color:'.$letter_pagecolor.';';

?>

<?php if ($unsubscribe) { ?>
<div class="unsubscribe">
	<?php 
		$unsubscribe=$Letters->Unsubscribe($unsubscribe);
		if ($unsubscribe) echo $unsubscribe; else echo _("An error occured during unsubscribing your address. Sorry for inconvenience.");
		//var_dump($unsubscribe);	
	?>
<div>

<?php } ?>

<div class="" id="letterwrapperdisplay">
	<div style="<?php echo $style_bgcolor; ?>">
		<div id="letter-display" class="ck ck-content" style="margin: 0 auto;padding:1em;<?php echo $style_width.$style_pagecolor; ?>">
			<?php echo $letter['letter_content']; ?>
		</div>
	</div>
	</form>
</div>
