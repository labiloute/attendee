<!-- HTML -->

<?php
// Security basics
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}
?>

<?php
(!$_POST['filter_year'] or $_POST['filter_year']=='')?$filter_year=date('Y'):$filter_year=$_POST['filter_year'];
//$_POST['filter_year']='2020';
//echo $filter_year;
?>


<?php


$date_formatter = \IntlDateFormatter::create(
  \Locale::getDefault(),
  \IntlDateFormatter::NONE,
  \IntlDateFormatter::NONE,
  \date_default_timezone_get(),
  \IntlDateFormatter::GREGORIAN,
  'MMMM'
);

?>

<span class="global-stats-title"><?php echo _('Users stats'); ?></span>

<!--
<span class="global-stats-form-wrapper">
	<form id="global-stats-form" name="global-stats-form" action="?page=stats" method="POST">
		<select id="filter_year" name="filter_year">';
		<?php echo _('Year') ?>
		<?php for ($i=date("Y");$i>=date("Y")-10;$i--) { ?>
			<?php if ($filter_year==$i) $sel='selected=selected'; else $sel=""; ?>
			<option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
		<?php } ?>
		</select>
		<input type="submit" id="global-stats-refresh" name="global-stats-refresh" value="<?php echo _('Refresh'); ?>" />
	</form>
</span>
-->
<?php

//**********************************/
// Focus on sessions and attendees */
//**********************************/
$sql = 'SELECT 
	*
	,DATEDIFF(NOW(), last_pwd_update) as last_pwd_update_ago
	,DATEDIFF(NOW(), last_login) as last_login_ago
	,IF(ldap=1,"Yes","No") as ldap
	,IF(contact_agreement=1,"Yes","No") as contact_agreement
	FROM users order by last_login DESC LIMIT 10000';

$res1=$my->query_array($sql);
$nb1 = $my->num_rows;

?>


<?php


?>

	<span class="global-stats-subtitle"><?php echo  _('All users'); ?></span>
	<table class="global-stats-table" id="users-stats-table">
	<tr>
		<th><?php echo _('Username'); ?></th>
		<th><?php echo _('Email'); ?></th>
		<th><?php echo _('First name'); ?></th>
		<th><?php echo _('Last name'); ?></th>
		<th><?php echo _('Profile'); ?></th>
		<th><?php echo _('Skills set'); ?></th>
		<th><?php echo _('Training requests set'); ?></th>
		<th><?php echo _('Last login').' ('._('days').')'; ?></th>
		<th><?php echo _('Last password update').' ('._('days').')'; ?></th>
		<th><?php echo _('Contact agreement'); ?></th>
		<th><?php echo _('External ID'); ?></th>
		<th><?php echo _('Confirmed'); ?></th>
		<th><?php echo _('Ldap'); ?></th>
		<th><?php echo _('Enabled'); ?></th>
	</tr>

	<?php $monthly_s=0;$monthly_a=0;$prev_month=1;$prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970));?>
	<?php $yearly_s=0;$yearly_a=0;?>
	<?php
	if ($nb1==0) {
		echo '<tr><td colspan="4">'._('No data').'</td></tr>';
	}
	else {
		 while ($u=$res1->fetch_assoc()) { ?>
		<?php 
		($u['last_login'])?$last_login=$u['last_login_ago']:$last_login=_('Never');
		($u['last_pwd_update'])?$last_pwd_update=$u['last_pwd_update_ago']:$last_pwd_update=_('Never');
		//($u['confirmkey'] && $u['last_password_update'])$confirmed_css='green'
		$ldap=$u['ldap'];
		$ldap=_($ldap);
		$contact_agreement=$u['contact_agreement'];
		$contact_agreement=_($contact_agreement);
		($disabled)?$disabled_css='red':$disabled_css='green';
		($u['trainingrqstid'])?$trainingrqstid_css='green':$trainingrqstid_css='red';
		($u['skillsid'])?$skillsid_css='green':$skillsid_css='red';
		?>
			<tr>
				<!--<td colspan="2"  class="total"><?php echo _('Total month')." ".$prev_month_long; ?></td>-->
				<td class=""><?php echo $u['username']; ?></td>
				<td class=""><?php echo $u['mail']; ?></td>
				<td class=""><?php echo $u['user_first_name']; ?></td>
				<td class=""><?php echo $u['user_last_name']; ?></td>
				<td class=""><?php echo $u['profile']; ?></td>
				<td class="<?php echo $skillsid_css; ?>"></td>
				<td class="<?php echo $trainingrqstid_css; ?>"></td>
				<td class=""><?php echo $last_login; ?> </td>
				<td class=""><?php echo $last_pwd_update; ?></td>
				<td class=""><?php echo $contact_agreement; ?></td>
				<td class=""><?php echo $u['external_id']; ?></td>
				<td class=""><?php echo $u['confirmed']; ?></td>
				<td class=""><?php echo $ldap; ?></td>
				<td class="<?php echo $disabled_css; ?>"><?php echo $u['disabled']; ?></td>
			</tr>
			<!--<?php $prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970)); ?>-->
		<?php } ?>
	<?php } ?>

	<?php echo '</table>'; ?>
