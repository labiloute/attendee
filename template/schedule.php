<!-- HTML -->

<?php
// New event button
$new_event_button='
<div class="schedule-new-button-wrapper">
<a href="?page=schedule" class="button">
	'._('New-event').'
</a>
</div>';


//Base Security
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}

?>

<!-- TEMPLATE SELECTOR (Query) -->
<?php

	$sql = 'SELECT 
	templates.id as id,
	templates.title as title,
	templates.max_attendee as max_attendee,
	templates.price as price,
	templates.username as username
	FROM `templates`
	GROUP BY templates.title
	';

	//echo $sql;
	$res=$my->query_assoc($sql);
	$nb_types = $my->num_rows;
?>
<!--  / TEMPLATE SELECTOR (Query) -->

<!--  User SELECTOR (Query) -->
<?php 
	$sql = 'SELECT 
	users.id,
	users.username,
	users.user_first_name,
	users.user_last_name,
	users.user_comment
	FROM `users`
	ORDER BY user_last_name ASC
	';

	//echo $sql;
	$u=$my->query_assoc($sql);
	$nb_u = $my->num_rows;

?>

<!--  / User SELECTOR (Query) -->

<!--  Session SELECTOR (Query) -->
<?php 
	if ($_GET['event_id']) {
	$sql = 'SELECT * FROM sessions WHERE id="'.$_GET['event_id'].'" LIMIT 1';

	//echo $sql;
	$s=$my->query_assoc($sql);
	$nb_s = $my->num_rows;
	$res_s = $s->fetch_assoc();

	}
?>

<!--  / User SELECTOR (Query) -->


<!-- TEMPLATE ID SELECTOR (Html) -->
<?php ($_GET['event_id'])?$action='&event_id='.$_GET['event_id']:NULL; ?>
<?php if ($_GET['event_id']) echo $new_event_button; ?>

<form enctype="multipart/form-data" id="schedule" name="schedule" action="?page=schedule<?php if (isset($action)) echo $action; ?>" method="POST">

<?php if (!$_GET['event_id']) { ?>

<span class="schedule-wrapper title"><?php echo _('Use a template'); ?></span>
<span class="schedule-wrapper content"><select id="schedule-title-select" name="schedule-title-select">
<?php
echo '<option value="">-- '._('Select').' --</option>';
$i=0;
while ($row = $res->fetch_assoc()) {
	($row['id']==$_POST['schedule-title-select'])?$sel_session_title_select='selected="selected"':$sel_session_title_select=NULL;
	echo '<option value="'.$row['id'].'" '.$sel_session_title_select.' price="'.$row['price'].'" max_attendee="'.$row['max_attendee'].'" username="'.$row['username'].'">'.$row['title'].'</option>';
	$i++;
	}
?>
</select></span>
<!-- / TEMPLATE ID SELECTOR (Html) -->
<?php } ?>

<?php
// Conditions for form values
// First read query results
($res_s['start'])?$schedule_start=date('d-m-Y H:s',strtotime($res_s['start'])):NULL;
($res_s['end'])?$schedule_end=date('d-m-Y H:s',strtotime($res_s['end'])):NULL;
// Priority to freshly posted values
($_POST['schedule-start'])?$schedule_start=$_POST['schedule-start']:NULL;
($_POST['schedule-end'])?$schedule_end=$_POST['schedule-end']:NULL;

?>

<span class="schedule-wrapper title"><?php echo _('schedule-start'); ?></span>
<span class="schedule-wrapper content"><input type="text" id="schedule-start" name="schedule-start" value="<?php echo $schedule_start;?>"/></span>
<span class="schedule-wrapper title"><?php echo _('schedule-end'); ?></span>
<span class="schedule-wrapper content"><input type="text" id="schedule-end" name="schedule-end" value="<?php echo $schedule_end;?>"/></span>

<div id="template-data">

</div>
<?php if (!$_GET['event_id']) { ?>
	<span class="schedule-wrapper content"><input type="submit" id="schedule-submit" name="schedule-submit" value="<?php echo _('Schedule-event'); ?>" />

		<span id="template-buttons-wrapper">
			<input type="submit" id="" name="" value="" class="button" />
		</span>
	</span>
<?php }
else if ($_GET['event_id']) { 
?>
	<span class="schedule-wrapper content"><input type="submit" id="schedule-modify" event_id="<?php echo $_GET['event_id']; ?>" name="schedule-modify" value="<?php echo _('Modify-event'); ?>" />
	<input type="submit" id="schedule-delete" name="schedule-delete" class="button" value="<?php echo _('Delete-event'); ?>" />
<?php } ?>
</form>

<?php if ($_GET['event_id']) {echo $new_event_button;} ?>

