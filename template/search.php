<!-- HTML -->
<?php
// Security basics
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}
?>

<?php
$attendee_email = $_GET['attendee'];
?>



<span class="attendee-stats-title"><?php echo _('Skills'); ?></span>


<?php
//Dolibarr module inclusion
if (file_exists('include/dolibarr.class.php')) {
	include_once('include/dolibarr.class.php');
	$Dolibarr=new Dolibarr($CONFIG);
	}
?>

<?php
// Creation oj object Attendee
//$A = new Attendee($CONFIG,$my);

?>

<div class="artinder-container">
<img src="images/artinder.png" class="artinder-logo" />
<div class="artinder-subtitle"><?php echo _('Lookup the member that suit you'); ?></div>
<?php

// If search is empty, fill with current requested training id
//var_dump($userinfo['trainingrqstid']);
if ($_POST['fill-with-requests']) {
	if ($userinfo['trainingrqstid']=='' or ! $userinfo['trainingrqstid']) {
		$redirect_trainingrqst=_('Please fill your').' <a href="?page=userspace#attendee-tabs-4">'._('training requests').'</a>';
		$searchedskills=False;
		}
	else {
		$searchedskills=explode(';',$userinfo['trainingrqstid']);
		$searchedskills = array_filter($searchedskills);
		}
	}
//else if (! $_POST['searchedskills'] or $_POST['searchedskills']=='') {
//	}
else if ($_POST['searchedskills']){
	$searchedskills=$_POST['searchedskills'];
	$searchedskills = array_filter($searchedskills);
	}
else $searchedskills=False;

//var_dump($searchedskills);
// Get skill search form
echo $auth->HtmlSearchForm($_GET['page'],$searchedskills);

//var_dump($skillslistarray);

// Eventually redirect to Userspace :
echo $redirect_trainingrqst;

// If some skills given by form
if ($searchedskills) {
	// Put requested skills in an array of value=>labels
	$skillslist = $auth->GetSkills($searchedskills);
	$skillslistarray = [];
	while($row = mysqli_fetch_array($skillslist)) $skillslistarray[$row['id']] = $row['title'];
	/**** Put values in various array and ordering them *****/
	$users = $auth->GetUserHavingSkills($searchedskills);
	// array of pair user=>count of matching values (so we can order them in descending depending of number of matches)
	$usermatches=array();
	// Array of pair user=>ids of matching skills
	$usermatcheslist=array();
	// For each user found
	while ($u=$users->fetch_array()) {
		// We explode his skills in a array
		$userskills=explode(';',$u['skillsid']);
		// Init match count
		$match=0;
		// For each ids of researched skills
		foreach($searchedskills as $postedskill) {
			// If the skill resquested is in user skill array, we append it to our different arrays
			if (in_array($postedskill,$userskills)) {
				// Add one point to user/matching array
				$match++;
				// Add one id to user/matching list
				$usermatchlist[$u['mail']][]=$postedskill;
				}
			}
		// At the end, we add count of matches values to our array of user
		$usermatches[$u['mail']]=$match;
		}
	// Sort ascending according to value, to present them in descending order to people : https://www.w3schools.com/PHP/php_arrays_sort.asp
	arsort($usermatches);
	/***** \ ORDERING ********/

	//var_dump($usermatches);
	//var_dump($usermatchlist);

	// Displaying all that
	echo '<div class="artinder-results-container">';
	if ($my->num_rows) {
		echo '<span>&#128150; '._('We found someone with at least one of the').' '.count($searchedskills).' '._('requested skills').'</span>';
		foreach ($usermatches as $userid=>$matchingcount) {
			echo '<div class="artinder-result">';
				echo $userid;
				foreach ($usermatchlist[$userid] as $skill) echo '<span title="'.$skillslistarray[$skill].'">&nbsp;&#128150;</span>'; // or &#127891; for graduation cap
			echo '</div>';
			}

		}
	else echo "<span>&#128148; "._("Sorry, I was unable to find any member matching")."</span>";
	echo '</div>';

	// Include a textarea with geoinfo included
	$geoinfo='<textarea id="geoinfo" style="display:none;">'.$auth->GetGeoInfo($searchedskills).'</textarea>';
	}

?>
<!-- Artinder-container -->
</div>
<?php
$map_class=$_GET['page'];
include_once('template/usermap.php');
?>

