<!-- HTML -->
<?php
//Base Security
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}
?>

<!-- PLACE SELECTOR (Query) -->
<?php 
// Init Classes
$Place = new Place($CONFIG,$my);
?>
<?php

	$sql = 'SELECT 
	id,
	title
	FROM `places`
	ORDER BY disabled ASC,title ASC
	';


	// Fetch places
	$places = $Place->GetPlacesList();


	//echo $sql;
	//$res=$my->query_assoc($sql);
	//$nb_types = $my->num_rows;
?>
<!--  / PLACE SELECTOR (Query) -->


<!-- TEMPLATE ID SELECTOR (Html) -->
<?php ($_GET['place_id'])?$action='&place_id='.$_GET['place_id']:NULL; ?>
<form enctype="multipart/form-data" id="places" name="places" action="?page=places<?php if (isset($action)) echo $action; ?>" method="POST">

<?php if (!$_GET['place_id']) { ?>

<span class="places-wrapper title"><?php echo _('Existing places'); ?></span>
<span class="places-wrapper content">
<select id="places-title-select" name="places-title-select">
<?php
echo '<option value="">-- '._('Select').' --</option>';
$i=0;
while ($row = $places->fetch_assoc()) {
	($row['id']==$_POST['places-title-select'])?$sel='selected="selected"':$sel=NULL;
	echo '<option value="'.$row['id'].'" '.$sel.'">'.$row['title'].'</option>';
	$i++;
	}
?>

<?php } ?>
</select>
</span>

<div id="places-data">

</div>

</form>

