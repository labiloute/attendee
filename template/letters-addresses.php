<?php
// Security basics
if (!$_SESSION['username']) {echo '<div class="forbidden">'._('Forbidden').'</div>';return;}
?>
<?php
// INIT
$Letters=new Letters($CONFIG,$my);

//var_dump($_POST);

?>

<?php

// FORMS
// Table letters
if ($_POST['rmlink']) $Letters->RemoveLink($_POST['rmlink']);
if ($_POST['rmaddress']) $Letters->RemoveAddress($_POST['rmaddress']);
if ($_POST['enableaddress']) $Letters->EnableAddress($_POST['enableaddress']);

if ($_POST['lettersave'] && $_GET['address_id'] && ! $_POST['enableaddress']) $_POST['disableaddress']=$_GET['address_id'];// Checkbox is unchecked
if ($_POST['disableaddress']) $Letters->Unsubscribe(False,$_POST['disableaddress']); // Disable address

//if ($_POST['createlink']) $Letters->CreateLink($_GET['group_id'],$_POST['createlink']);
if ($_POST['addaddress']) echo "Errors : ".$Letters->AddAddress($_POST['addaddress']);

if ($_POST['massdisableaddress']){
	$Letters->MassDisableAddresses($_POST['massdisableaddress']);
	if ($Letters->errors==0) {
		$infos.=_('Addresses disabled').' : '.$Letters->disabled_addresses.'<br />'
			._('Were already disabled').' : '.$Letters->already_disabled_addresses.'<br />'
			._('Unknown addresses').' : '.$Letters->unknown_addresses.'<br />';
		}
	else $errors.=$Letters->errors.('errors');
	}

//if ($_POST['newgroup']) echo "Errors : ".$Letters->AddGroup($_POST['newgroup']);
// Table Letters_groups
//if ($_POST['letters-groups']) $Letters->UpdateLettersToGroups($_GET['letter_id'],$_POST['letters-groups']);

?>
<?php
// DISPLAY
$letter=$Letters->GetLetter($_GET['letter_id']);
// If Enabled
($letter['letter_enabled']=='1')?$enabled_ckd="checked":$enabled_ckd="";

?>
<form method="post" action="">

<div id="letters-menu">
	<?php echo $Letters->Menu(); ?>
</div>
<div id="letters" class="letterslist">
	<?php echo $Letters->GetAddressesHtml($_GET['address_id']); ?>

</div>

<div class="letter-right-block address" id="letterwrapper">

	<div class="letter-settings" >
		<?php
		// Save button
		echo '<input type="submit" id="lettersave" title="'._('Save').'" value="&#9745;" />';
		
		if (! $_GET['address_id']) 
			echo '<div id="letter-row1">
				<span class="letter-textarea-title">'._('Mass import : ').'</span>
				<textarea name="addaddress" id="addaddress" style=""></textarea>
				</div>';
		if (! $_GET['address_id']) 
			echo '<div id="letter-row2">
				<span class="letter-textarea-title">
				'._('Mass disable : ').'
				</span>
				<textarea name="massdisableaddress" id="massdisableaddress" style=""></textarea>
				</div>';
		if ($_GET['address_id']) {
			$address_info = $Letters->getAddress($_GET['address_id']);
			$row = $address_info->fetch_assoc();
			//var_dump($row);
			if ($row['disabled']) {$enabled_ckd="";$bname='enableaddress';}else {$enabled_ckd="checked";$bname='disableaddress';}
			}
		?>

		<?php if ($_GET['address_id']) {
			echo '
			<label class="toggler-wrapper style-1">
				<input type="checkbox" value="'.$_GET['address_id'].'" '.$enabled_ckd.' name="'.$bname.'" id="'.$bname.'" />
				<!--<input type="checkbox" >-->
				<div class="toggler-slider">
				<div class="toggler-knob"></div>
				</div>
			</label>'.PHP_EOL;

			echo '<span class="letters-options">'._('Address belongs to').'</span>'.PHP_EOL;
			$groups=$Letters->GetGroupsOfAddressHtml($_GET['address_id']);
			if ($groups) echo $groups.PHP_EOL;
			else echo _('None');
			}
		?>
	</div>
</div>
</form>
