<span class="attendee-stats-title"><?php echo _('Stock') ?></span>

<div id="template-information"><?php echo $CONFIG['stock']['information']; ?></div>
<?php
if (file_exists('include/dolibarr.class.php')) {
        include_once('include/dolibarr.class.php');
        $Dolibarr=new Dolibarr($CONFIG,$my);
	// First get item list from Dolibarr Stock
        $stocked=$Dolibarr->getLocalStock();

	// Put everybody in an array
	$prod=array();
	while($product=$stocked->fetch_assoc()) {
		$prod[]=$product;
		}
	$i=1;
	$prev_cat="";
	// First round for displaying tabs
	foreach($prod as $p) {
		($p['category'])?$cat=$p['category']:$cat=_('Uncategorized');
		if ($i==1) {
			echo '<div id="attendee-tabs-'.$i.'"><ul>'.PHP_EOL; // The virtual tab that contains all others
			$i++;
			}
		if ($cat!=$prev_cat){
			echo '<li class="attendee-tabs"><a href = "#attendee-tabs-'.$i.'">'.$cat.'</a></li>'.PHP_EOL;
			$i++;
			$prev_cat=$cat;
			}			
		}
	echo '</ul>'.PHP_EOL;

	echo '<div class="products">'.PHP_EOL;

	$i=2;
	$j=0;
	$prev_cat="";
	// Second round for displaying tabs
	foreach ($prod as $p) {
		// If cat is null, rewrite to "Uncategorized"
		($p['category'])?$cat=$p['category']:$cat=_('Uncategorized');
		// If category changed
		if ($cat!=$prev_cat){
			// Close previous tab (except for first tab - which has index 2)
			if ($i>2) {
				echo "<!-- End of flex container div -->".PHP_EOL;
				echo '</div>'.PHP_EOL;
				echo "<!-- End of tab div -->".PHP_EOL;
				echo '</div>'.PHP_EOL;
				}
			// Open new tab
			echo '<div id ="attendee-tabs-'.$i.'">'.PHP_EOL;
			// Open a new container
			echo '<div class="products-flex-line">'.PHP_EOL;
			// Increment tab counter
			$i++;
			// Store new name of previous cat
			$prev_cat=$cat;
			// Init product counter
			$j=0;
			}

			//
		$modulo=$j%4;
		//echo $modulo;
		//($modulo==0)?$clear='clear-both':$clear='';
		// Every 4 tabs
		if ($modulo==0 && $j>0) {
			echo "<!-- Close previous flex container -->".PHP_EOL;
			echo '</div>'.PHP_EOL;
			echo "<!-- Open a new container -->".PHP_EOL;
			echo '<div class="products-flex-line">'.PHP_EOL;
			}
		//echo '<div class="product '.$clear.'">';
		echo "<!-- Start of product div -->".PHP_EOL;
		echo '<div class="product">'.PHP_EOL;
			echo '<div class="product-title-container" title="Référence : '.$p['ref'].'">'.$p['label'].'</div>'.PHP_EOL;
			echo '<div class="product-subtitle">'.round($p['price']).' '.$CONFIG['currency_symbol'].' - '.$p['stock'].' '._('units in stock').'</div>'.PHP_EOL;
			echo '<div class="product-availability"></div>'.PHP_EOL;
			echo '<div class="product-image"><img src="'.$p['image'].'" height="100" /></div>'.PHP_EOL;
			echo '<div class="product-desc">'.$p['description'].'</div>'.PHP_EOL;
		echo "<!-- End of product div -->".PHP_EOL;
		echo '</div>'.PHP_EOL;
		$j++;
		}
	
	echo "<!-- End of LAST flex container div -->".PHP_EOL;
	echo '</div>'.PHP_EOL;
	echo "<!-- End of tab div -->".PHP_EOL;
	echo '</div>'.PHP_EOL;
	}
	echo " <!-- End of tab 1 (main tab that include others) -->".PHP_EOL;
	echo '</div>'.PHP_EOL;
?>
