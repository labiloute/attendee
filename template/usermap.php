<?php
// Set default class if page is called independantly
if (!$map_class) $map_class='fullpage';
// Set default Geoinfo, which query for all subscribers having an adress and skills
if (!$geoinfo) $geoinfo='<textarea id="geoinfo" style="display:none;">'.$auth->GetGeoInfo().'</textarea>';
// Echo hidden textarea containing geo info
echo $geoinfo;
// Check leaflet path
(file_exists('leaflet/leaflet.js'))?$leaflet='leaflet/leaflet.js':$leaflet='../leaflet/leaflet.js';
(file_exists('leaflet.markercluster/dist/leaflet.markercluster.js'))?$markercluster='leaflet.markercluster/dist/leaflet.markercluster.js':$markercluster='../leaflet.markercluster/dist/leaflet.markercluster.js';
?>

<div id="map" class="<?php echo $map_class; ?>">
	    <!-- Ici s'affichera la carte -->
</div>


        <!-- Fichiers Javascript -->
        <script src="<?php echo $leaflet; ?>" crossorigin=""></script>
        <script type='text/javascript' src='<?php echo $markercluster; ?>'></script>
	<script type="text/javascript">
	    // On initialise la latitude et la longitude de Paris (centre de la carte)
	    var lat = 49.1367;
	    var lon = -0.0761;
	    var macarte = null;
            var markerClusters; // Servira à stocker les groupes de marqueurs
            // Nous initialisons une liste de marqueurs
            // debug
            var villes = {
                "Paris": { "lat": 48.852969, "lon": 2.349903 },
                "Brest": { "lat": 48.383, "lon": -4.500 },
                "Quimper": { "lat": 48.000, "lon": -4.100 },
                "Bayonne": { "lat": 43.500, "lon": -1.467 },
                "Louvigny": {"lat":49.1565383, "lon":-0.3937312 }
            };
            // Recupere la valeur du champ caché
            let users = document.getElementById('geoinfo').value;
            // Recupere les donnees de chaque utilisateur
            var villes=JSON.parse(users);
	    // Fonction d'initialisation de la carte
            function initMap() {
                // Nous définissons le dossier qui contiendra les marqueurs
                var iconBase = 'http://localhost/carte/icons/';
                var iconBase = '<?php echo $CONFIG['usermap_logo']; ?>';
                var zoom_level=9;
		// Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
                macarte = L.map('map').setView([lat, lon], zoom_level);
                markerClusters = L.markerClusterGroup(); // Nous initialisons les groupes de marqueurs
                // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
                L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                    // Il est toujours bien de laisser le lien vers la source des données
                    attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                    minZoom: 1,
                    maxZoom: 20
                }).addTo(macarte);
                // Nous parcourons la liste des villes
                for (ville in villes) {
	            // Nous définissons l'icône à utiliser pour le marqueur, sa taille affichée (iconSize), sa position (iconAnchor) et le décalage de son ancrage (popupAnchor)
                    var myIcon = L.icon({
                        //iconUrl: iconBase + "autres.png",
                        iconUrl: iconBase,
                        iconSize: [50, 50],
                        iconAnchor: [25, 50],
                        popupAnchor: [-3, -76],
                    });
                    var marker = L.marker([villes[ville].lat, villes[ville].lon], { icon: myIcon }); // pas de addTo(macarte), l'affichage sera géré par la bibliothèque des clusters
                    marker.bindPopup(villes[ville].label);
                    markerClusters.addLayer(marker); // Nous ajoutons le marqueur aux groupes
                }
                macarte.addLayer(markerClusters);
            }
	    window.onload = function(){
		// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
		initMap(); 
	    };
	</script>
