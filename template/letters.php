<?php
// Security basics
if (!$_SESSION['username']) {echo '<div class="forbidden">'._('Forbidden').'</div>';return;}
?>

<?php

// INIT
$Letters=new Letters($CONFIG,$my);

//var_dump($_POST);

?>

<?php

// FORMS

// Table letters
if ($_GET['letter_id'])$letter_id=$_GET['letter_id'];
if ($_POST['letter_id'])$letter_id=$_POST['letter_id'];

//echo "Letter id :".$letter_id;
// Delete a letter
if ($_POST['rmletter']) {$Letters->RemoveLetter($_POST['rmletter']); $letter_id=NULL;}

// Update or create (only if requested is not to delete)
if ($_POST && ! $_POST['rmletter']) $letter_id=$Letters->UpdateLetter($_POST,$letter_id);

// Table Letters_groups
if ($_POST['letters-groups'] && ! $_POST['rmletter']) $Letters->UpdateLettersToGroups($letter_id,$_POST['letters-groups']);

// Reset Shipping stats
if ($_POST['reset-success']) $Letters->ResetShippings($letter_id,1);
if ($_POST['reset-errors']) $Letters->ResetShippings($letter_id,0);

// Test mail
if ($_POST['letter-test']) {
	// Get letter content
	$letter=$Letters->GetLetters($letter_id);
	// Extract first row
	$row = $letter->fetch_assoc();
	// Construct header
	$header=$Letters->MailHeader();
	// Set test address to dict
	$row['address']=$_POST['letter-test'];
	// Construct full mail content
	$full_content=$Letters->ConstructMail($row,$header);
	// Shipping
	$shipping=$mail->Send($row['address'],$row['letter_title'],$full_content);
	// Display result
	if ($shipping) {
		$infos=_('Successfully shipping letter').' "'.$row['letter_title'].'" to "'.$row['address'].'"'.PHP_EOL;$status=1;} 
	else {$errors.= "[ERROR] Error sending email : ".$mail->msg.PHP_EOL;$status=0;}
	}

//echo "Letter id :".$letter_id;
?>
<?php
// DISPLAY
if ($letter_id) $letter=$Letters->GetLetter($letter_id);

// Prepare fields
($letter['letter_enabled']=='1')?$enabled_ckd="checked":$enabled_ckd="";
($_POST['letter-title'] && ! $_POST['rmletter'])?$letter_title=htmlspecialchars($_POST['letter-title'] ?? ''):$letter_title=htmlspecialchars($letter['letter_title'] ?? '');
($_POST['letter-width'] && ! $_POST['rmletter'])?$letter_width=$_POST['letter-width']:$letter_width=$letter['letter_width'];
($_POST['letter-bgcolor'] && ! $_POST['rmletter'])?$letter_bgcolor=$_POST['letter-bgcolor']:$letter_bgcolor=$letter['letter_bgcolor'];
($_POST['letter-pagecolor'] && ! $_POST['rmletter'])?$letter_pagecolor=$_POST['letter-pagecolor']:$letter_pagecolor=$letter['letter_pagecolor'];

if ($letter_width) $style_width='width:'.$letter_width.'px;';
if ($letter_bgcolor) $style_bgcolor='background-color:'.$letter_bgcolor.';';
if ($letter_pagecolor) $style_pagecolor='background-color:'.$letter_pagecolor.';';

?>
<div id="letters-menu">
	<?php echo $Letters->Menu(); ?>
</div>
<form method="post" action="">

<div id="letters" class="letterslist">
	<?php echo $Letters->GetLettersHtml($letter_id); ?>
</div>

<div class="letter-right-block letters" id="letterwrapper">

	<div class="letter-settings">
	
		<div id="letter-row1">
			<?php if (!$letter_title) $letter_title=_('New letter'); ?>
			<span class="letter-title"><input type="text" name="letter-title" id="letter-title" value="<?php echo $letter_title; ?>" required /></span>
			<?php if ($letter_id) { ?>
				<span class="lastshipping"><?php echo _('Last shipping (this letter)').' : '.$Letters->getLastShipping($letter_id); ?></span>
			<?php } ?>
			<span class="letter-stats"><?php echo $Letters->GetShippingStats($letter_id); ?></span>
		</div>		
		<!--<label for="letter-enabled"><?php echo _('Enabled'); ?></label>-->

		<div id="letter-row2">
			<span class="letter-toggle">
			<label class="toggler-wrapper style-1">
				<input type="checkbox" <?php echo $enabled_ckd; ?> name="letter-enabled" id="letter-enabled" />
				<!--<input type="checkbox" >-->
				<div class="toggler-slider">
					<div class="toggler-knob"></div>
				</div>
			</label>
			</span>
			&nbsp;
			<?php if ($letter_id) { ?>
			<a href="?page=letters-display&display_id=<?php echo $letter['letter_code']; ?>" title="<?php echo _('External link'); ?>" class="letters-options">&#8599;</a>
			<?php } ?>
			<span class="letters-options"><?php echo _('Width'); ?> : <input type="text" size="4" name="letter-width" id="letter-width" value="<?php echo $letter_width; ?>" />px</span>
			<span class="letters-options"><?php echo _('Background color'); ?> : <input type="color" name="letter-bgcolor" id="letter-bgcolor" value="<?php echo $letter_bgcolor; ?>" /> <button name="rmbgcolor" id="rmbgcolor" value="'.$row['link_id'].'">&#10060;</button></span>
			<span class="letters-options"><?php echo _('Page color'); ?> : <input type="color" name="letter-pagecolor" id="letter-pagecolor" value="<?php echo $letter_pagecolor; ?>" /> <button name="rmpagecolor" id="rmpagecolor"  value="'.$row['link_id'].'">&#10060;</button></span>
			
			<?php if ($letter_id) { ?>
			<span class="letters-options"><?php echo _('Test with'); ?> : <input type="text" name="letter-test" id="letter-test" value="" placeholder="<?php echo $CONFIG['emailplaceholder']; ?>" /></span>
			<?php } ?>
			
			<input type="submit" id="lettersave" title="<?php echo _('Save'); ?>" value="&#9745;" />
			<a href="#" id="lettersdisplayeditor" title="<?php echo _('Edit'); ?>">&#128275;</a>
			<input type="text" name="letter_id" id="letter_id" value="<?php echo $letter_id; ?>"  style="display:none;" />
			

		</div>

		<div id="letter-row3">
			<label for="letters-groups"></label>
			<select name="letters-groups[]" id="letters-groups" title="<?php echo _('Choose a group'); ?>" multiple>
				<?php echo $Letters->GetSelectGroups($letter_id); ?>
			</select>
		</div>

	</div>
 

	<div id="lettercontentbackground" style="<?php echo $style_bgcolor; ?>">
		<div id="lettercontentwrapper">

			<textarea id="letter-source" name="letter-source" style="display:none; margin: 0 auto;<?php echo $style_width.$style_pagecolor; ?>" >
				<?php echo $letter['letter_content']; ?>
			</textarea>

			<div id="letter-content" class="ck ck-content" style=" margin: 0 auto;<?php echo $style_width.$style_pagecolor; ?>">
				<?php echo $letter['letter_content']; ?>
			</div>
		</div>
	</div>
</div>
</form>
