<?php
// Security basics
if (!$_SESSION['username']) {
        echo '<div class="forbidden">'._('Forbidden').'</div>';
        return;
}
$ldap->connect();

// Call getOU without argument to set baseDN as in config - otherwise specify it now
$ldapou=$ldap->getOU(False,True);

//var_dump($ldapou);

var_dump($_POST);

// Get selected Organisational Unit
if ($_POST['ouselect'])$ouselect=$_POST['ouselect'];
else if ($_GET['ouselect'])$ouselect=$_GET['ouselect'];

// Eventually enable user
if ($_POST['enableuser']) {
        $ldap->bind($CONFIG['ldap_admin_dn'],$CONFIG['ldap_admin_pwd']);
        $userselect=$_POST['enableuser'];
        $userattr=$ldap->getAttribute($userselect,$CONFIG['ldap_disabled_field']);
        //var_dump($userattr);
        $modattr=str_replace($CONFIG['ldap_disabled_pattern'],'',$userattr);
        $wldap=$ldap->SetValue($userselect,$CONFIG['ldap_disabled_field'],0,$modattr);
        //var_dump($wldap);
        }

// Eventually disable user
if ($_POST['disableuser']) {
        $ldap->bind($CONFIG['ldap_admin_dn'],$CONFIG['ldap_admin_pwd']);
        $userselect=$_POST['disableuser'];
        $userattr=$ldap->getAttribute($userselect,$CONFIG['ldap_disabled_field']);
        //var_dump($userattr);
        $modattr=$CONFIG['ldap_disabled_pattern'].$userattr;
        $wldap=$ldap->SetValue($userselect,$CONFIG['ldap_disabled_field'],0,$modattr);
        //var_dump($wldap);
        }


// Select first OU form
echo '<form action="?page='.$_GET['page'].'" method="POST">';
echo '<select name="ouselect" id="ouselect">';
foreach ($ldapou as $ou) {
        ($ou==$ouselect)?$sel='selected="selected"':$sel='';
        echo '<option value="'.$ou.'" '.$sel.' >'.$ou.'</option>';
        }
echo '</select>';
echo '<input type="submit" />';
echo '</form>';

// ******************** ADD USER

$createform='<form method="POST" action="?'.$_GET['page'].'">'.PHP_EOL;
$createform.='<input type="text" id="createuserfirstname" name="userfirstname" />'.PHP_EOL;
$createform.='<input type="text" id="createuserlastname" name="userlastname" />'.PHP_EOL;
$createform.='<input type="text" id="createuseremail" name="useremail" />'.PHP_EOL;
$createform.= '<select name="createrbacselect" id="createrbacselect">';
$createform.='<option value="'._('Not allowed').'" >'._('Not allowed').'</option>';
foreach ($CONFIG['ldap_allowed_rbac_values'] as $rbacvalue) {
        ($businessdata[1]==$rbacvalue)? $selrbac='selected="selected"':$selrbac='';
        $createform.='<option value="'.$rbacvalue.'" '.$selrbac.'>'.$rbacvalue.'</option>';
        }
$createform.='</select>';
echo $createform;


// ******************** ENABLED USERS

// List users from OU
if ($ouselect) {
        $searchou='ou=users,ou='.$ouselect.','.$CONFIG['ldap_base_dn'];
        //echo $searchou;
        $ouusers=$ldap->getUsers($searchou,$disabled=False);
        //var_dump($ouusers);
        $oulimits=$ldap->getOUDescription($ouselect);
        //var_dump($oulimits);

        ($oulimits['TITLE']=='')?$oulimits['TITLE']=$ouselect:NULL;
        echo '<h1>'.$oulimits['TITLE'].' - '.$oulimits['DATA'].'Go of DATA and '.$oulimits['USERS'].' users</h1>';
        foreach ($oulimits['KEY'] as $keyindex=>$keypair) {
                echo "URL du service ".$keyindex.' : '.$keypair.'<br />';
                }

        echo '<table>';
        foreach ($ouusers as $ouuser) {
                echo '<tr>';
                $userservicelist=array();
                foreach ($ouuser['businesscategory'] as $businesscategory) {
                        $businessdata=explode(':',$businesscategory);
                        $userservicelist[]=$businessdata[0];
                        }
                //var_dump(in_array('cloud.artifaille.fr',$userservicelist));
                if ($ouuser['cn'][0]=='') continue;
                //if (substr($ouuser['mail'][0],0,9)=='DISABLED-') continue;
                //echo $ouuser['cn'][0].' ('.$ouuser['mail'][0].') ';
                echo '<td>'.$ouuser['givenname'][0].'</td>';
                echo '<td>'.$ouuser['sn'][0].'</td>';
                echo '<td>'.$ouuser['mail'][0].'</td>';
                foreach ($oulimits['KEY'] as $key=>$servicekey) {
                        echo '<td>';
                        // The ORG service is listed in the USER specific field
                        //$key.' : ';
                        //echo 'VAL'.$servicekey.'|';
                        //var_dump(in_array($servicekey,$userservicelist));
                        echo '<select name="rbacselect[]" id="rbacselect-'.$ouuser['uid'][0].'">';
                        echo '<option value="'._('Not allowed').'" >'._('Not allowed').'</option>';
                        foreach ($CONFIG['ldap_allowed_rbac_values'] as $rbacvalue) {
                                ($businessdata[1]==$rbacvalue)? $selrbac='selected="selected"':$selrbac='';
                                echo '<option value="'.$rbacvalue.'" '.$selrbac.'>'.$rbacvalue.'</option>';
                                }
                        echo '</select>';
                        echo '</td>';
                        }
                echo '<td>';
                echo '<form type="submit" method="POST" action="?page='.$_GET['page'].'&ouselect='.$ouselect.'" /><input type="submit" name="disableuser" id="disableuser" value="'.$ouuser['uid'][0].'" /></form>';
                echo '</td>';
                echo '</tr>';
                }
        echo '</table>';


// ****************************** DISABLED USERS

        echo "<br /><br />DISABLED :<br />";
        // Union with Disabled Users
        $ouusers=$ldap->getUsers($searchou,$disabled='ONLY');
        foreach ($ouusers as $ouuser) {
                $userservicelist=array();
                foreach ($ouuser['businesscategory'] as $businesscategory) {
                        $businessdata=explode(':',$businesscategory);
                        $userservicelist[]=$businessdata[0];
                        }
                //var_dump(in_array('cloud.artifaille.fr',$userservicelist));
                if ($ouuser['cn'][0]=='') continue;
                //if (substr($ouuser['mail'][0],0,9)=='DISABLED-') continue;
                echo $ouuser['cn'][0].' ('.$ouuser['mail'][0].') ';
                foreach ($oulimits['KEY'] as $key=>$servicekey) {
                        // The ORG service is listed in the USER specific field
                        echo $key.' : ';
                        //echo 'VAL'.$servicekey.'|';
                        //var_dump(in_array($servicekey,$userservicelist));
                        if (in_array($servicekey,$userservicelist)) {
                                echo '<input type="checkbox" checked="checked" />';
                                }
                        else
                                echo '<input type="checkbox" />';

                        echo '<form type="submit" method="POST" action="?page='.$_GET['page'].'&ouselect='.$ouselect.'" /><input type="submit" name="enableuser" id="enableuser" value="'.$ouuser['uid'][0].'" /></form>';
                        }
                echo '<br />';
                }
        }
?>

