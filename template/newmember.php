<!-- HTML -->
<?php

if (! $auth->CheckAllowedNetworks() && $CONFIG['newmember-security']==True && $_SESSION['profile']!='ADMIN' && $_SESSION['profile']!='MANAGER') {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
	}
$dt = new DateTime;
$date_formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE);
?>
<span class="attendee-stats-title"><?php echo _('Subscription') ?></span>
<?php
if (file_exists('include/dolibarr.class.php')) {
        include_once('include/dolibarr.class.php');
        $Dolibarr=new Dolibarr($CONFIG);

?>

<?php if (! $_POST['member-email'] && ! $_GET['subscribe-create'] && !$_POST['renew-member-id']) { ?>
<video style="display:block;margin:0 auto;max-width: 100%;" controls="" height="400" width="auto" ><source src="<?php echo $CONFIG['base_folder']."/videos/".$CONFIG['base_folder']."_welcome_video.mp4"; ?>" type="video/mp4" />Unable to load welcome video.</video>


<div id="member-search">
<div id="newmember-welcome-message">
<?php printf(_('newmember-welcome-message'),$CONFIG['website-name']); ?>
</div>
	<form class="pure-form pure-form-aligned" enctype="multipart/form-data" id="membercheck" name="membercheck" action="?page=newmember" method="POST">
		<fieldset>
			<div class="pure-control-group">
				<!--<label for="member-email"><?php echo _('Member email'); ?></label>-->
				<input placeholder="<?php echo _('Type here your email'); ?>" type="text"  id="member-email" name="member-email"/>
		        </div>
			<div class="pure-controls">
				<!--<label for="aligned-cb" class="pure-checkbox">
				<input type="checkbox" id="aligned-cb" /> I&#x27;ve read the terms and conditions</label>-->
				<button type="submit" class="pure-button pure-button-primary"><?php echo _('Test'); ?></button>
        		</div>
	</form>
</div>


<?php } ?>

<?php
	//var_dump($_POST);
	// STEP 2 - Email Check
	if ($_POST['member-email'] && ! $_GET['create-member'] && ! $_POST['renew-member-id']) {
		$members=$Dolibarr->count_members($_POST['member-email']);
		//var_dump($members);
		//$is_valid=$Dolibarr->is_valid($subscription);
		echo '<br />';
		if ($members==0) {
			echo '<div id="member-result">';
				echo "<h4>"._("Email").' '.$_POST['member-email'].' '._('is unknown from our database').'.</h4>';
				echo '<form method="POST" action="?page=newmember&subscribe-create='.$_POST['member-email'].'" class="pure-form pure-form-aligned">';
				echo '<div class="pure-controls">';
				echo '	<button type="submit" class="pure-button pure-button-primary">'._('Subscribe').'</button>';
				echo '</div>';
				echo '</form>';
				echo "</div>";

			}
		else if ($members==1)
			{
			$subscription=$Dolibarr->get_last_subscription_date($_POST['member-email']);
			if ($Dolibarr->is_valid($subscription)) {
				echo '<div id="member-result">';
				echo "<h4>"._("Subscription for").' '.$_POST['member-email'].' '._('is valid until').' '.$date_formatter->format(strtotime(date('Y-m-d',$subscription))).'</h4>';
				echo '<strong>'._('Welcome on board !').'</strong>';
				echo "</div>";
				}
			else { 
				echo '<div id="member-result">';
				echo "<h4>"._("Subscription for").' '.$_POST['member-email'].' <strong>'._('is not valid anymore since').'</strong> '.$date_formatter->format(strtotime(date('Y-m-d',$subscription))).'</h4>';
				echo "<br />"._("Subscription explanation");
				echo '<form method="POST" action="?page=newmember" class="pure-form pure-form-aligned">';
				echo '<div class="pure-control-group">';
				echo '	<input type="text"  value="'.$Dolibarr->last_user_id.'" id="renew-member-id" name="renew-member-id" readonly hidden="hidden" />';
				echo '	<input type="text"  value="'.$_POST['member-email'].'" id="renew-member-email" name="renew-member-email" readonly hidden="hidden" />';
				echo '</div>';
				echo '<div class="pure-controls">';
				foreach ($CONFIG['dolibarrpaiementmethod'] as $key=>$method) {
					echo '	<button type="submit" id="renew-method-'.$key.'" name="renew-method" value="'.$key.'" class="pure-button pure-button-primary">'._('Renew your subscription').' '.$method.'</button>';
					}
				echo '</div>';
				echo '</form>';
				echo "</div>";
				}
			}
		else if ($members>1) {
			echo _("More than one member found. Please contact support.");
			}
		else
			{
			echo _("Error querying database. Please contact support.")."<br />";
			echo "Error returned :<br />";
			echo $Dolibarr->error."<br />";
			}
		//echo "There are ".$members." member(s registered in Dolibarr with this email<br />";
		}

	// STEP 3 - Creation form, only if member still do not exists
	if ($_GET['subscribe-create'] && $members==0 && ! $_POST['renew-member-id']) { ?>
		<div id="member-subscribe">
		<?php echo _('newmember-privacy'); ?>
		<form class="pure-form pure-form-aligned" enctype="multipart/form-data" id="memberform" name="memberform" action="?page=newmember&create-member=1" method="POST">
		    <fieldset>
			<div class="pure-control-group">
				<input type="text"  value="<?php echo $_GET['subscribe-create']; ?>" id="member-email" name="member-email" placeholder="<?php echo _('Email'); ?>" readonly />
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-firstname" name="member-firstname" placeholder="<?php echo _('First name'); ?>" />
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-lastname" name="member-lastname"  placeholder="<?php echo _('Last name'); ?>"/>
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-address" name="member-address"  placeholder="<?php echo _('Street name'); ?>"/>
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-zip" name="member-zip"  placeholder="<?php echo _('Zip code'); ?>"/>
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-town" name="member-town"  placeholder="<?php echo _('Town name'); ?>"/>
			</div>
			<div class="pure-control-group">
				<input type="text"  id="member-phone" name="member-phone"  placeholder="<?php echo _('Phone'); ?>"/>
			</div>
			
			<?php 
			foreach ($CONFIG['dolibarrpaiementmethod'] as $key=>$method) {
				echo '<button type="submit" id="paiment-method-'.$key.'" name="paiment-method" value="'.$key.'" class="pure-button pure-button-primary">'._('I pay').' '.$method.'</button>&nbsp;';
					}
			?>
		<!--	<button type="submit" class="pure-button pure-button-primary"><?php echo _('Subscribe'); ?></button>-->
		</form>
		<div class="disclaimer">
			<?php 
				if ($CONFIG['msg']['start-of-subscription-disclaimer']) echo $CONFIG['msg']['start-of-subscription-disclaimer'];
				else echo _('start-of-subscription-disclaimer');
			 ?>
		</div>
		</div>
		<?php
		}
		?>

	<?php
	// STEP 4a - Creation of member && subscription
	if ($_GET['create-member']) {
			echo '<div id="member-subscribe">';
			$memberid=$Dolibarr->CreateMember($_POST);
			$memberemail=$_POST['member-email'];
			$bankid=$_POST['paiment-method'];
			if ( ! $memberid) {
				echo '<div class="error">';
					echo _("Error reported from Dolibarr during creation process");
				echo '</div>';
				echo '<form method="POST" action="?page=newmember" class="pure-form pure-form-aligned">';
				echo '<div class="pure-controls">';
				echo '	<button type="submit" class="pure-button pure-button-primary">'._('Start again').'</button>';
				echo '</div>';
				echo '</form>';
				}

			if (is_numeric($memberid))
				{
				//$subscription=$Dolibarr->CreateSubscription($memberid);
				// Get member information (to get user First and Last name)
				$memberinfo=$Dolibarr->GetMemberInfo($memberemail);
				// Create a bank line (une écriture)
				$banklineid=$Dolibarr->CreateBankLine($bankid);
				// Create a new subscription (une adhésion)
				$subscriptionid=$Dolibarr->CreateSubscription($memberid,$bankid,$banklineid);
				// Update subscription to set fd_bank field to the correct bank line -> create the link between subscription and bank line)
				$updateid=$Dolibarr->UpdateSubscription($subscriptionid,$banklineid);
				// Create link the other way around, i.e. from bank line to member
				$newbanklink = $Dolibarr->CreateBankLink($bankid,$banklineid,$memberid,$memberinfo['firstname']." ".$memberinfo['lastname']);
				if ($subscriptionid && $banklineid && $updateid && $newbanklink) {
					//echo _('Subscription successfully created');
					echo '<div class="disclaimer">';
					if ($CONFIG['msg']['start-of-subscription-explanation']) echo $CONFIG['msg']['start-of-subscription-explanation'];
					else echo _('start-of-subscription-explanation');
					echo '</div>';
					}
				else {
					// Display errors
					echo "Error : <br />";
					echo "MemberInfo : ".print_r($memberinfo)."<br />";
					echo "Identifiant de compte bancaire : ".$bankid."<br />";
					echo "BankLineId :".$banklineid."<br />";
					echo "Subscriptionid :".$subscriptionid."<br />";
					echo "UpdateID :".$updateid."<br />";
					echo "NewBankLink : ".$newbanklink."<br />";
					}
				}
			//else echo "Error on member id";
			echo '</div>';
		}
	?>

	<?php
	// STEP 4b - Creation of subscription only
	if ($_POST['renew-member-id'] && $_POST['renew-member-email']) {
			echo '<div id="member-renew">';
			$memberid=$_POST['renew-member-id'];
			$memberemail=$_POST['renew-member-email'];
			$bankid=$_POST['renew-method'];

			if (is_numeric($memberid))
				{
				// Get member information (to get user First and Last name)
				$memberinfo=$Dolibarr->GetMemberInfo($memberemail);
				// Create a bank line (une écriture)
				$banklineid=$Dolibarr->CreateBankLine($bankid);
				// Create a new subscription (une adhésion)
				$subscriptionid=$Dolibarr->CreateSubscription($memberid,$bankid,$banklineid);
				// Update subscription to set fd_bank field to the correct bank line -> create the link between subscription and bank line)
				$updateid=$Dolibarr->UpdateSubscription($subscriptionid,$banklineid);
				// Create link the other way around, i.e. from bank line to member
				$newbanklink = $Dolibarr->CreateBankLink($bankid,$banklineid,$memberid,$memberinfo['firstname']." ".$memberinfo['lastname']);
				// If everything went fine, ok !
				if ($subscriptionid && $banklineid && $updateid && $newbanklink) {
					//echo _('Subscription successfully created');
					echo '<div class="disclaimer">';
					if ($CONFIG['msg']['start-of-subscription-explanation']) echo $CONFIG['msg']['start-of-subscription-explanation'];
					else echo _("start-of-subscription-explanation");
					echo '</div>';
					}
				else { 
					// Display errors
					echo "Error : <br />";
					echo "MemberInfo : ".$memberinfo."<br />";
					echo "BankLineId :".$banklineid."<br />";
					echo "Subscriptionid :".$subscriptionid."<br />";
					echo "UpdateID :".$updateid."<br />";
					echo "NewBankLink : ".$newbanklink."<br />";
					}
				}
			echo '</div>';
		}
	?>

	<?php //var_dump($_POST); ?>
<?php } ?>





