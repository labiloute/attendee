<!-- HTML -->

<?php
//Base Security
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}
?>

<?php
//(!$_POST['filter_year'] or $_POST['filter_year']=='')?$filter_year=date('Y'):$filter_year=$_POST['filter_year'];
//$_POST['filter_year']='2020';
//echo $filter_year;
?>

<span class="global-stats-title"><?php echo _('Training stats'); ?></span>
<!--
<span class="global-stats-form-wrapper">
	<form id="global-stats-form" name="global-stats-form" action="?page=stats" method="POST">
		<select id="filter_year" name="filter_year">';
		<?php echo _('Year') ?>
		<?php for ($i=date("Y");$i>=date("Y")-10;$i--) { ?>
			<?php if ($filter_year==$i) $sel='selected=selected'; else $sel=""; ?>
			<option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
		<?php } ?>
		</select>
		<input type="submit" id="global-stats-refresh" name="global-stats-refresh" value="<?php echo _('Refresh'); ?>" />
	</form>
</span>
-->
<?php

$trainings=$auth->GetAllSkills();

//$res1=$my->query_array($sql);
//$nb1 = $my->num_rows;

?>


<?php

// Compile all IDs and count in an array
$counter = [];
while ($training=$trainings->fetch_array()) {
	$trainid=explode(";",$training['trainingrqstid'] ?? '');
	$skillid=explode(";",$training['skillsid'] ?? '');
	foreach ($trainid as $id) {
		if ($id!="" && $id!=NULL)
			$counter[$id]++;
		}
	foreach ($skillid as $id) {
		if ($id!="" && $id!=NULL)
			$counterskills[$id]++;
		}
	}

// Get all Skills label
$labels=$auth->GetSkills();
while ($label = $labels->fetch_assoc()) {
	$alllabels[$label['id']] = $label['title'];
	}

// We sort all queries by amount of requests
arsort($counter);
arsort($counterskills);

$total=0;
foreach ($counter as $key=>$training) {
	$total+=$training;
	}
$totalskills=0;
foreach ($counterskills as $key=>$skill) {
	$totalskills+=$skill;
	}

?>


<?php if (count($counter)>0) { ?>
	<table class="global-stats-table" id="global-stats-table-bystate">
	<tr>
		<th><?php echo _('Skill'); ?></th>
		<th><?php echo _('Number of requests'); ?></th>
	</tr>


	<?php foreach ($counter as $key=>$training) { ?>
	
		<?php if (array_key_exists($key,$alllabels)) { ?>
		<tr>	
			<td><?php echo $alllabels[$key];?></td>
			<td><?php echo $training;?></td>
		</tr>

		<?php } ?>

	<?php } ?>
	<tr>
		<td colspan="1" class="total"><?php echo _('Total'); ?></td>
		<td class="total"><?php echo $total; ?></td>
	</tr>
	</table>
<?php } else { ?>

	<div class="notification"><?php echo _('No data'); ?></div>

<?php } ?>



<?php if (count($counterskills)>0) { ?>
	<table class="global-stats-table" id="global-stats-table-bystate">
	<tr>
		<th><?php echo _('Skill'); ?></th>
		<th><?php echo _('Number of skills offered'); ?></th>
	</tr>


	<?php foreach ($counterskills as $key=>$skill) { ?>

		<?php if (array_key_exists($key,$alllabels)) { ?>
		<tr>	
			<td><?php echo $alllabels[$key];?></td>
			<td><?php echo $skill;?></td>
		</tr>

		<?php } ?>

	<?php } ?>
	<tr>
		<td colspan="1" class="total"><?php echo _('Total'); ?></td>
		<td class="total"><?php echo $totalskills; ?></td>
	</tr>
	</table>
<?php } else { ?>

	<div class="notification"><?php echo _('No data'); ?></div>

<?php } ?>




<br />




