<?php

// Temp
//var_dump($userinfo);
//$coordinates=$auth->geocode('username','2 rue au lièvre','14111','Louvigny');
//if ($coordinates) var_dump($coordinates);
//$DolibarrInfo = $Dolibarr->GetMemberInfo($userinfo['username']);
//var_dump($DolibarrInfo);

// Defining a callback function
function myFilter($var){
	return ($var !== NULL && $var !== FALSE && $var !== "");
}

//************************************************/
// If the user ask to set his password
//
// We display form form
//
//************************************************/

if ($_GET['userspace'] && $_GET['confirmkey'] && ($_GET['setpassword'] || $_GET['resetpassword'])) {
	//var_dump($auth->IsRegistered($_GET['userspace'],'users.id',$_GET['confirmkey'],False));
	if (! $auth->IsRegistered($_GET['userspace'],'users.id',$_GET['confirmkey'],False)) $errors.=_('Activation key is not valid anymore. Please reset your password again');
	else {
		if ($_GET['setpassword'])$buttonlabel=_('Confirm account');
		else if ($_GET['resetpassword'])$buttonlabel=_('Reset your password');
		echo '<div class="offline-notification">
			'._('Please choose a password').'<br />
			<form method="POST" action="'.$CONFIG['base_url'].'?page=userspace&setpwdid='.$_GET['userspace'].'&confirmkey='.$_GET['confirmkey'].'">
			<input type="password" name="password" id="password" value="" /><br /><br />
			'._('Confirm your password here').'<br /><input type="password" name="password2" id="password2" value="" /><br /><br />
			<input type="submit" name="set_password" value="'.$buttonlabel.'" />
			</form>';
		echo '</div>';
		}
	}

//************************************************/
// If the user is not logged in
//
// We display registering form
//************************************************/

else if( ! $_SESSION['username']) {
	($_POST['userspace-userid'])?$userspaceuserid=CleanSimpleField($_POST['userspace-userid']):$userspaceuserid=NULL;
	($_POST['userspace-userid-reset'])?$userspaceuseridreset=CleanSimpleField($_POST['userspace-userid-reset']):$userspaceuseridreset=NULL;
	echo '<div class="offline-notification">';
	echo _('If your already created your account');
	echo '<br />';
	echo _('Please log in clicking below');
	echo '<br />';
	echo $auth->GetLoginButton(True);
	echo '</div>';
	echo '<div class="offline-notification">';
	echo _('You are a valid member of organization').' '.$CONFIG['website-name'];
	echo '<br />';
	echo '
		<form id="userspace-create" method="POST">
			<input type="text" name="userspace-userid" id="userspace-userid" placeholder="'._('Your email').'" value="'.$userspaceuserid.'" />
			<input type="submit" name="userspace-create-request" value="'._('Create your userspace').'"/>
		</form>';
	echo '</div>';
	echo '<div class="offline-notification">';
	echo _('You forgot your password');
	echo '<br />';
	echo '
		<form id="userspace-reset" method="POST">
			<input type="text" name="userspace-userid-reset" id="userspace-userid-reset" placeholder="'._('Your email').'" value="'.$userspaceuseridreset.'" />
			<input type="submit" name="userspace-reset-request" value="'._('Reset your password').'"/>
		</form>';
	echo '</div>';
	}

//************************************************/
//
//
// If the user is logged on
//
//
//************************************************/

else if ($_SESSION['username']) {

	//******************/
	// Get user info
	//******************/
	($userinfo['contact_agreement']==1)?$ckd_contact_agreement='checked="checked"':$ckd_contact_agreement='';
	?>

	<?php

	if ($Dolibarr) {
		// Get last subscription date and check that it is still valid
		$last_subscription_date=$Dolibarr->get_last_subscription_date($useremail);
		$endofsubscription=$last_subscription_date;

		// Format display
		if ($last_subscription_date) {
			// If Dolibarr user ID (number) has changed, we update local external user id
			if ($Dolibarr->last_user_id && $Dolibarr->last_user_id!=$external_id) $auth->UpdateExternalId($userid,$Dolibarr->last_user_id);
			$external_id = $Dolibarr->last_user_id;
			$html_id='<span title="'.$useremail.' - ID : '.$external_id.'">&#127380;</span>';
			($CONFIG['renew_url'])?$renew_url = $CONFIG['renew_url']:$renew_url='?page=newmember';
			$html_renew = sprintf(_("To keep benefit of organisation, please renew your subscription"),$CONFIG['website-name'],$renew_url);
			if ($Dolibarr->is_valid($last_subscription_date)) {
				$html_subscription_state='<div class="dolibarr-subscription valid">&#9973; '._('Your subscription is valid').' ('._('valid until').' '.$Dolibarr->format_date($endofsubscription).') '.$html_id.'</div>';
				}
			else if ( ! $Dolibarr->is_valid($last_subscription_date)) {
				$html_subscription_state='<div class="dolibarr-subscription invalid">&#128680; '._('Your subscription is not valid').' ('._('valid until').' '.$Dolibarr->format_date($endofsubscription).') '.$html_id.'</div>';
				$html_subscription_state.='<div class="dolibarr-subscription renew" >'.$html_renew.'</div>';
				}
			else {
				$html_subscription_state='<div class="dolibarr error">'.$Dolibarr->error.'</div>';
				}
			}
		}
	?>

	<span class="attendee-stats-title"><?php echo _('Userspace of').' '.$userid; ?></span>
	<!-- Tab 1 displays tabs -->
	<div id = "attendee-tabs-1">
		 <ul>
		    <li class="attendee-tabs"><a href = "#attendee-tabs-2"><?php echo _('Your profile'); ?></a></li>
		    <li class="attendee-tabs"><a href = "#attendee-tabs-3"><?php echo _('Your sessions'); ?></a></li>
		    <li class="attendee-tabs"><a href = "#attendee-tabs-4"><?php echo _('Your skills / requests'); ?></a></li>
		 </ul>
		<!-- Tab 2 (first displayed to user) starts here -->
		<div id ="attendee-tabs-2">
			<div class="userspace-wrapper">
				<form id="profile" name="profile" action="?page=userspace" method="POST">
				<div class="subtitle"><div><?php echo _('Hello').' '.$_SESSION['username']; ?></div></div>
					<?php echo _('Email linked to your profile is').' '.$userinfo['mail'].'<br />'; ?>
					<?php echo $html_subscription_state; ?>

				<!--<span class="subtitle"><?php echo ('Contact agreement'); ?></span>-->
				<?php echo _('Check this box if you agree to be contacted directly by others member of the organization'); ?> <input type="checkbox" id="contact_agreement" name="contact_agreement" <?php echo $ckd_contact_agreement ?> />

				<div class="profile-greeter subtitle"><div><?php echo _('Change your password'); ?></div></div>
				<div class="profile-password">
				<?php if ($userinfo['ldap']=='1') {
						echo _('Your account is linked to an LDAP directory')."<br />";
						echo _('Please use the following tool to reset your password')." :";
						echo ' <a href="'.$CONFIG['ldap_external_pwd_tool'].'" target="new">'._('Click here').'</a><br />';
					}
				else {
					echo '<input type="password" id="user_password" name="user_password" value="" /><br />';
					}
				?>
				</div>
				<div class="profile-greeter subtitle"><div><?php echo _('Describe yourself'); ?></div></div>
				<div class="profile-description">
					<textarea id="user_comment" name="user_comment"><?php echo $userinfo['user_comment']; ?></textarea>
				</div>
				
				<?php if ($Dolibarr) { ?>
				<div class="profile-greeter subtitle"><div><?php echo _('Your address'); ?></div></div>
				<?php echo _('Address usage'); ?>
				<div class="profile-address">
					<?php echo _('Address'); ?> <input id="user_address" name="user_address" value="<?php echo $userinfo['address']; ?>" /><br />
					<?php echo _('Zip code'); ?> <input id="user_zip" name="user_zip" value="<?php echo $userinfo['zip']; ?>" /><br />
					<?php echo _('City'); ?> <input id="user_city" name="user_city" value="<?php echo $userinfo['city']; ?>" /><br />
				</div>
				<?php } ?>
				
				<div class="profile-submit">
					<input type="submit" id="profile-submit" name="profile-submit" value="<?php echo _('Save profile'); ?>" /><br />
				</div>
				</form>
			</div>
		<!-- Tab 2 (first displayed to user) ends here  -->
		</div>
		 <div id = "attendee-tabs-3">

	<?php
	// Repartition by type
	$sql = 'SELECT attendees.state, Count(attendees.state) as nb  
		FROM attendees 
		WHERE attendees.email="'.$useremail.'" 
		GROUP BY attendees.state
		LIMIT 100';
	$res_a=$my->query_array($sql);
	$nb_a = $my->num_rows;
	?>
	<?php if ($nb_a>0) { ?>
		<span class="attendee-stats-subtitle"><?php echo  _('By session state'); ?></span>
		<table class="attendee-stats-table" id="attendee-stats-table-bystate">
		<tr>
			<th><?php echo _('State'); ?></th>
			<th><?php echo _('Count'); ?></th>
		</tr>
		<?php while ($a=$res_a->fetch_assoc()) { ?>
			<tr>
			<td><?php echo $a['state']; ?></td>
			<td><?php echo $a['nb']; ?></td>
			</tr>
		<?php } ?>
		</table>
	<?php } ?>

	<br />

	<?php
	// Repartition by type
	$sql = '
	SELECT IFNULL(sessions.title,templates.title) as realtitle, attendees.state, Count(attendees.state) as nb
	FROM `attendees`
	LEFT JOIN sessions on sessions.id=attendees.session_id
	LEFT JOIN templates on sessions.template_id=templates.id
	WHERE attendees.email="'.$useremail.'"
	GROUP BY realtitle,state
	LIMIT 100
	';

	$res_a=$my->query_array($sql);
	$nb_a = $my->num_rows;
	?>
	<?php if ($nb_a>0) { ?>
		<span class="attendee-stats-subtitle"><?php echo  _('By session title'); ?></span>
		<table class="attendee-stats-table" id="attendee-stats-table-bytitle">
		<tr>
			<th><?php echo _('Title'); ?></th>
			<th><?php echo _('State'); ?></th>
			<th><?php echo _('Count'); ?></th>
		</tr>
		<?php while ($a=$res_a->fetch_assoc()) { ?>
			<tr>
				<td><?php echo $a['realtitle']; ?></td>
				<td><?php echo $a['state']; ?></td>
				<td><?php echo $a['nb']; ?></td>
			</tr>
		<?php } ?>
		</table>
	<?php } ?>
	<br />
	<?php
	// List all sessions with this attendee
	$sql = '
	SELECT sessions.start , IFNULL(sessions.title,templates.title) as realtitle, attendees.state
	FROM `attendees`
	LEFT JOIN sessions on sessions.id=attendees.session_id
	LEFT JOIN templates on sessions.template_id=templates.id
	WHERE attendees.email="'.$useremail.'"
	ORDER BY sessions.start DESC
	LIMIT 100
	';

	$res_a=$my->query_array($sql);
	$nb_a = $my->num_rows;
	?>
	<?php if ($nb_a>0) { ?>
		<span class="attendee-stats-subtitle"><?php echo  _('All sessions'); ?></span>
		<table class="attendee-stats-table" id="attendee-stats-table-allsessions">
		<tr>
			<th><?php echo _('Date'); ?></th>
			<th><?php echo _('Title'); ?></th>
			<th><?php echo _('State'); ?></th>
		</tr>
		<?php while ($a=$res_a->fetch_assoc()) { ?>
			<tr>
				<td><?php echo $a['start']; ?></td>
				<td><?php echo $a['realtitle']; ?></td>
				<td><?php echo $a['state']; ?></td>
			</tr>
		<?php } ?>
		</table>
	<?php } ?>
	</div>
	<div id="attendee-tabs-4">
	<?php
	
        // Starred Skills
        $starredskills = $auth->GetUserSkillsLabel($userid,$starred=True);
        $nb_starred=$my->num_rows;  
        $c_sk=0;
        if ($starredskills) {
                while ($sk=$starredskills->fetch_assoc()) {
                        if (file_exists($CONFIG['base_folder'].'/images/star-'.$sk['starred'].'.png')) $img=$CONFIG['base_folder'].'/images/star-'.$sk['starred'].'.png';
                        else if (file_exists($CONFIG['base_folder'].'/images/star.png')) $img=$CONFIG['base_folder'].'/images/star.png';
                        else if (file_exists('images/star-'.$sk['starred'].'.png')) $img='images/star-'.$sk['starred'].'.png';
                        else $img='images/star.png';
                        $txt_sk.='<span class="userspace-starred-skills"><img src="'.$img.'" title="'._('Great ! You reached the starred step  with id').' '.$sk['starred'].'" height="50"/>'.$sk['title'].'</span>';
                        $c_sk++;
                        }
                // If user has some starred skills, display section
                if ($c_sk>0) {
                        echo '<span class="attendee-stats-subtitle">'._('You already have').' '.$c_sk.' '._('starred steps').' !</span>';
                        echo '<div style="text-align:center;">';
                        echo $txt_sk;
                        echo '</div>';
                        }
                }
	// Get skills and training requests
	echo '<span class="attendee-stats-subtitle">'._('Skills and training requests').'</span>';
	// Provide user possibility to declare skills
        // Get skills from DB
        $skills = $auth->GetUserSkills($userid);
        if ($skills) {
                $sql_skills = $skills->fetch_array();
                // Extact skills from query result
                $skills= $sql_skills['skillsid'];
                $rqsts = $sql_skills['trainingrqstid'];
                // Set as an array
                $skills = explode(';',$skills);
                $skills=array_filter($skills,"myFilter");// Callback to keep eventually 0 from array
                $rqsts = explode(';',$rqsts);
                $rqsts=array_filter($rqsts,"myFilter");// Callback to keep eventually 0 from array
                }

	//var_dump($rqsts);
	// Display Skills
	echo $auth->GetSkillsForm(NULL,$_GET['page'].'&userid='.$userid,$skills,$rqsts);
	?>
		</div>
	<!-- TAB-3 -->
	</div>
	<?php
	} // End of logged-in userspace
?>
