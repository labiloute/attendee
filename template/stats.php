<!-- HTML -->

<?php
//Base Security
if (!$_SESSION['username']) {
	echo '<div class="forbidden">'._('Forbidden').'</div>';
	return;
}
?>

<?php
(!$_POST['filter_year'] or $_POST['filter_year']=='')?$filter_year=date('Y'):$filter_year=$_POST['filter_year'];
//$_POST['filter_year']='2020';
//echo $filter_year;
?>


<?php


$date_formatter = \IntlDateFormatter::create(
  \Locale::getDefault(),
  \IntlDateFormatter::NONE,
  \IntlDateFormatter::NONE,
  \date_default_timezone_get(),
  \IntlDateFormatter::GREGORIAN,
  'MMMM'
);

?>

<span class="global-stats-title"><?php echo _('Global stats'); ?></span>

<span class="global-stats-form-wrapper">
	<form id="global-stats-form" name="global-stats-form" action="?page=stats" method="POST">
		<select id="filter_year" name="filter_year">';
		<?php echo _('Year') ?>
		<?php for ($i=date("Y");$i>=date("Y")-10;$i--) { ?>
			<?php if ($filter_year==$i) $sel='selected=selected'; else $sel=""; ?>
			<option value="<?php echo $i; ?>" <?php echo $sel; ?>><?php echo $i; ?></option>
		<?php } ?>
		</select>
		<input type="submit" id="global-stats-refresh" name="global-stats-refresh" value="<?php echo _('Refresh'); ?>" />
	</form>
</span>
<?php

//**********************************/
// Focus on sessions and attendees */
//**********************************/
$sql = 'SELECT 
		src.month, 
		src.title, 
		SUM(src.nb_attendees) as nb_attendees, 
		COUNT(src.title) as nb_sessions 
	FROM
		(
		SELECT 
			sessions.id,
			MONTH(sessions.start) as month, 
			sessions.title, 
			attendees.email, 
			COUNT(attendees.email) as nb_attendees 
		FROM sessions
		LEFT JOIN attendees on (sessions.id=attendees.session_id and attendees.state!=\'ABSENT\')
		WHERE YEAR(sessions.start)=\''.$filter_year.'\'
		AND sessions.canceled=0
		#AND attendees.state!=\'ABSENT\'
		GROUP BY sessions.id
		ORDER BY month ASC
		LIMIT 1000
		)
		src
	GROUP BY month, title
	ORDER BY month ASC, title ASC
	LIMIT 200';

$res1=$my->query_array($sql);
$nb1 = $my->num_rows;

?>


<?php


?>

	<span class="global-stats-subtitle"><?php echo  _('All non-canceled sessions'); ?></span>
	<table class="global-stats-table" id="global-stats-table-bystate">
	<tr>
		<th><?php echo _('Month'); ?></th>
		<th><?php echo _('Title'); ?></th>
		<th><?php echo _('Count of sessions'); ?></th>
		<th><?php echo _('Count of attendees'); ?></th>
	</tr>

	<?php $monthly_s=0;$monthly_a=0;$prev_month=1;$prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970));?>
	<?php $yearly_s=0;$yearly_a=0;?>
	<?php
	if ($nb1==0) {
		echo '<tr><td colspan="4">'._('No data').'</td></tr>';
	}
	else {


		 while ($a=$res1->fetch_assoc()) { ?>
			<?php $cur_month=$a['month']; ?>
			<?php $cur_month_long=$date_formatter->format(mktime(0, 0, 0, $a['month'], 2, 1970)); ?>
			<?php if ($cur_month==$prev_month) { 
				$monthly_a+=$a['nb_attendees'];
				$monthly_s+=$a['nb_sessions'];
				$yearly_a+=$a['nb_attendees'];
				$yearly_s+=$a['nb_sessions'];
				} 
				else { ?>
				<tr>
				<td colspan="2"  class="total"><?php echo _('Total month')." ".$prev_month_long; ?></td>
				<td class="total"><?php echo $monthly_s; ?></td>
				<td class="total"><?php echo $monthly_a; ?></td>
				<?php $monthly_a=$a['nb_attendees'];$monthly_s=$a['nb_sessions']; ?>
				<?php $yearly_a+=$a['nb_attendees'];$yearly_s+=$a['nb_sessions']; ?>
				</tr>
			<?php } ?>
			<tr>
			<td><?php echo $cur_month_long; ?></td>
			<td><?php echo $a['title']; ?></td>
			<td><?php echo $a['nb_sessions']; ?></td>
			<td><?php echo $a['nb_attendees']; ?></td>
			</tr>

			<?php $prev_month=$a['month']; ?>
			<?php $prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970)); ?>
		<?php } ?>
		<tr>
			<td colspan="2" class="total"><?php echo _('Total month')." ".$prev_month_long; ?></td>
			<td class="total"><?php echo $monthly_s; ?></td>
			<td class="total"><?php echo $monthly_a; ?></td>
		</tr>
		<tr>
			<td colspan="2" class="totalyear"><?php echo _('Total year')." ".$filter_year; ?></td>
			<td class="totalyear"><?php echo $yearly_s; ?></td>
			<td class="totalyear"><?php echo $yearly_a; ?></td>
		</tr>
		</table>
	<?php } ?>

<?php
//**********************************/
// Focus on sessions animators */
//**********************************/
$sql = 'SELECT 
		src.month, 
		src.title, 
		SUM(src.nb_attendees) as nb_attendees, 
		COUNT(src.title) as nb_sessions 
	FROM
		(
		SELECT 
			sessions.id,
			MONTH(sessions.start) as month, 
			sessions.title, 
			attendees.email, 
			COUNT(attendees.email) as nb_attendees 
		FROM sessions
		LEFT JOIN attendees on sessions.id=attendees.session_id
		WHERE YEAR(sessions.start)=\''.$filter_year.'\'
		AND sessions.canceled=0
		AND attendees.state!=\'ABSENT\'
		GROUP BY sessions.id
		ORDER BY month ASC
		LIMIT 1000
		)
		src

	GROUP BY month, title
	ORDER BY month ASC, title ASC
	LIMIT 200';

$sql = '
SELECT 
MONTH(sessions.`start`) as month
,Count(username) as nb
,username
FROM sessions
WHERE 1
AND sessions.canceled = 0
AND YEAR(sessions.start)=\''.$filter_year.'\'
AND username IS NOT NULL
GROUP BY sessions.username,MONTH(sessions.`start`)
ORDER BY sessions.username,MONTH(sessions.`start`)

'
;

//echo $sql;
$res1=$my->query_array($sql);
$nb1 = $my->num_rows;

?>


<?php


?>


	<span class="global-stats-subtitle"><?php echo  _('All non-canceled sessions by animator'); ?></span>
	<table class="global-stats-table" id="global-stats-table-bystate">
	<tr>
		<th><?php echo _('Username'); ?></th>
		<?php for ($i=1;$i<=12;$i++) {
			$cur_month_long=$date_formatter->format(mktime(0, 0, 0, $i, 2, 1970));
			echo '<th>'.$cur_month_long.'</th>';
			}
		?>
		<th><?php echo _('Total'); ?></th>
	</tr>


	<?php $monthly_s=0;$monthly_a=0;$prev_month=1;$prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970));?>
	<?php $yearly_s=0;$yearly_a=0;?>
	<?php $prev_username=''; ?>
	<?php $k=0;$i=13;$total_a=0;$total_m=0;$total_month=array(); ?>
	<?php 
	if ($nb1==0) {
		echo '<tr><td colspan="14">'._('No data').'</td></tr>';
	}
	else {
		while ($s=$res1->fetch_assoc()) {
			$cur_username=$s['username'];
			$cur_month=$s['month'];

			if ($cur_username==$prev_username) {
				if ($i<$cur_month){
					for ($j=$i;$j<$cur_month;$j++) {
						echo '<td></td>';
						//$i++;
						}
					}
				echo '<td>'.$s['nb'].'</td>';
				$total_a+=$s['nb'];
				$total_month[$cur_month]+=$s['nb'];
				$i=$cur_month+1;
				}
			else {
				if ($k>0) {
					if ($i<=12){
						for ($j=$i;$j<=12;$j++) {
							echo '<td></td>';
							$i++;
							}
						}
					echo '<td class="totalyear">'.$total_a.'</td>';
					echo '</tr>';
					}
				$i=1;
				$total_a=0;
				echo '<tr><td>'.$s['username'].'</td>';
				if ($i<$cur_month) {
					for ($j=$i;$j<$cur_month;$j++) {
						echo '<td></td>';
						//$i++;
						}
					}
				echo '<td>'.$s['nb'].'</td>';
				$total_a+=$s['nb'];
				$total_month[$cur_month]+=$s['nb'];
				$prev_username=$s['username'];
				$i=$cur_month+1;
				}
			$prev_month_long=$date_formatter->format(mktime(0, 0, 0, $prev_month, 2, 1970));
			$k++;
			}
			// Display last total count and close
			if ($i<=12){
				for ($j=$i;$j<=12;$j++) {
					echo '<td></td>';
					$i++;
					}
				}
			echo '<td class="totalyear">'.$total_a.'</td>';
			echo '</tr>';
			echo '<tr>';
			echo '<td class="totalyear">'._('Totals').'</td>';
			$totals=0;
			for ($i=1;$i<=12;$i++) {
				echo '<td class="totalyear">'.$total_month[$i].'</td>';
				$totals+=$total_month[$i];
				}
			echo '<td class="totalyear">'.$totals.'</td>';
			echo '</tr>';
		}
	echo '</table>';





