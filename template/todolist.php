<?php
//Base Security
if (!$_SESSION['username'] or ! $CONFIG['enable-todolist']) {
	echo '<div class="forbidden">'._('Forbidden').'<br /><br />
	'._('Please notice that this feature may be disabled by configuration').'</div>';
	return;
}

?>

<?php
require "include/todolist.php";
//require "include/groups.php";
//require "config.php";
//require "js/todo.js.php";
// ACLS


//$category=$_GET['group'];
//$page_title="Notre ToDo List (".$category." ;))";


// Display Categories
$sql="SELECT category FROM `todolist` GROUP BY LOWER(category) ORDER BY category ASC LIMIT 100";
//echo $sql;
$q = $my->query_assoc($sql);

// Filling the $todos array with new ToDo objects:

$categories.='<a href="?page=todolist">'._('Uncategorized').'</a>';
while($row = $q->fetch_assoc()){
	$categories.='<a href="?page=todolist&category='.$row['category'].'">'.$row['category'].'</a>';
}


// Prepare filter for category
($_GET['category'])?$sql_cat='WHERE lower(category) =\''.$_GET['category'].'\'':$sql_cat='WHERE category IS NULL or category=\'\'';
// Select all the todos, ordered by position:
$sql="SELECT 
todolist.id
,todolist.position
,todolist.text
,todolist.dt_added
,todolist.category
,Count(todolistup.username) as love
,Count(IF(todolistup.username='".$_SESSION['username']."',1,NULL)) as loved

FROM `todolist` 
LEFT JOIN todolistup ON todolist.id = todolistup.todo_id

".$sql_cat."

GROUP BY todolist.id
ORDER BY `position` ASC 
LIMIT 1000";
//echo $sql;
$q = $my->query_assoc($sql);

$todos = array();

// Filling the $todos array with new ToDo objects:

while($row = $q->fetch_assoc()){
	$todos[] = new ToDo($row,$my);
}


?>
<!--
<h1>
<?php echo $page_title ?> 
</h1>
-->
<div class="todolistcategories">
	<?php echo $categories; ?>
</div>
<div id="todolistmain">

	<ul class="todoList">
		
        <?php
		
		// Looping and outputting the $todos array. The __toString() method
		// is used internally to convert the objects to strings:
		foreach($todos as $item){
			echo $item;
		}
		
		?>

    </ul>

<a id="todolist-addButton" class="green-button" href="#">Ajouter</a>

</div>

<!-- This div is used as the base for the confirmation jQuery UI POPUP. Hidden by CSS. -->
<div id="todolist-dialog-confirm" title="Supprimer ?">Êtes-vous sûr(e) de vouloir supprimer cette ligne?</div>


<p class="todolistnote">Glissez pour réordonner vos tâches</p>

<!-- Including our scripts -->

<?php
if ($_GET['category'])$category=$_GET['category'];
// Assignement of category in javascript code
echo '<script type="text/javascript">
category="'.$category.'";
</script>';
?>
