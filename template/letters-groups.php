<?php
// Security basics
if (!$_SESSION['username']) {echo '<div class="forbidden">'._('Forbidden').'</div>';return;}
?>
<?php
// INIT
$Letters=new Letters($CONFIG,$my);

//var_dump($_POST);

?>

<?php

// FORMS
// Table letters
if ($_POST['rmlink']) $Letters->RemoveLink($_POST['rmlink']);
if ($_POST['rmgroup']) $Letters->RemoveGroup($_POST['rmgroup']);
if ($_POST['createlink']) $Letters->CreateLink($_GET['group_id'],$_POST['createlink']);
if ($_POST['addaddress']) { $Letters->AddAddress($_POST['addaddress'],$_GET['group_id']);$infos.=_('Addresses inserted').' : '.$Letters->inserted_addresses.'<br />'._('Links done').' : '.$Letters->inserted_links; }
if ($_POST['newgroup']) $Letters->AddGroup($_POST['newgroup']);
if ($_POST['renamegroup']) echo $Letters->RenameGroup($_GET['group_id'],$_POST['renamegroup']);
// Table Letters_groups
//if ($_POST['letters-groups']) $Letters->UpdateLettersToGroups($_GET['letter_id'],$_POST['letters-groups']);

?>
<?php
// DISPLAY
//$letter=$Letters->GetLetter($_GET['letter_id']);
// If Enabled
($letter['letter_enabled']=='1')?$enabled_ckd="checked":$enabled_ckd="";

?>
<form method="post" action="">

<div id="letters-menu">
	<?php echo $Letters->Menu(); ?>
</div>
<div id="letters" class="letterslist">
	<?php echo $Letters->GetGroupsHtml($_GET['group_id']); ?>
	<input type="text" value="" name="newgroup" id="newgroup" /><input type="submit" />
</div>

<div class="letter-right-block groups" id="letterwrapper">
	<?php if ($_GET['group_id']) {
	$group=$Letters->GetGroups($_GET['group_id']);
	$group = $group->fetch_assoc();
	$members=$Letters->GetAddressesofGroupHtml($_GET['group_id']);
	$count=$Letters->num_rows;
	?>	
	<!-- Full width -->
	<div class="letter-settings" >
		<div id="letter-row1">
			<span class="letter-title"><input type="text" name="renamegroup" id="renamegroup" value="<?php echo $group['group_name']; ?>" /></span>
			<span class="letter-stats"><span class="total" title="<?php echo _('Members count'); ?>"><span class="value"><?php echo $count; ?></span></span></span>

		</div>
		<div id="letter-row2">
			<input type="submit" id="lettersave" title="<?php echo _('Save'); ?>" value="&#9745;" />
			<span class="letter-textarea-title"><?php echo _('Auto and mass insertion').'</span>'.'<textarea name="addaddress" id="addaddress" style=""></textarea>'; ?>
		</div>
	</div>



	<!-- Half page left -->
	<div class="letters-groups-settings-left" style="">
		<?php echo $members; ?>
	</div>
	
	<div class="letters-groups-settings-middle" style="">
		&#8596;
	</div>

	
	<!-- Half page right -->
	<div class="letters-groups-settings-right" style="">
		<?php echo $Letters->GetAddressesOutOfGroupHtml($_GET['group_id']); ?>
	</div>

	<?php } ?>
</div>
</form>
