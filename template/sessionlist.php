<?php
$Event=new Event($CONFIG,$my);
echo $Event->Html_Session_Filter();
?>

<?php
// This function give to time a reading comfort : it translate 9:30:00 to 9h30, depending on locale
function format_time($locale,$time) {
	if ($locale=='fr' or $locale='fr_FR' or $locale='fr_FR.utf-8') {
		if (substr($time,-3)==':00') return substr($time,0,2).'h';
		else return substr($time,0,2).'h'.substr($time,-2);
		}
	else return $time;
	}

?>


<?php

if ($_GET['event_id']) {
	$event_id_action='event_id='.$_GET['event_id'];
	}

?>

<?php

// Get events
$res=$Event->get_Events($sql_filter,$sql_date_filter);

// Only if query succeed
if ($res) {
$nb_sessions = $my->num_rows;

// Captcha
include("include/captcha.php");
$_SESSION['captcha_image_src']=captcha();

?>

<!-- For each session -->
<div id="sessions">
<?php while ($row = $res->fetch_assoc()) {
	// Select good values
	($row['session_title'])?$session_title=$row['session_title']:$session_title=$row['template_title'];
	($row['session_image'])?$session_image=$row['session_image']:$session_image=$row['template_image'];
	(is_file($CONFIG['base_folder'].'/images/'.$session_image))?$session_image=$CONFIG['base_folder'].'/images/'.$session_image:$session_image='images/default_event.png';// Fallback to default logo if image file does not exists
	($row['session_comment'])?$session_comment=$row['session_comment']:$session_comment=$row['template_comment'];
	($row['session_price']!=NULL)?$session_price=$row['session_price']:$session_price=$row['template_price'];

	($row['session_animator_first_name'])?$session_animator_first_name=$row['session_animator_first_name']:$session_animator_first_name=NULL;
	($row['session_animator_last_name'])?$session_animator_last_name=$row['session_animator_last_name']:$session_animator_last_name=NULL;
	($row['session_animator_comment'])?$session_animator_comment=$row['session_animator_comment']:$session_animator_comment=NULL;
	($row['session_place'])?$session_place=nl2br($row['session_place']):$session_place = _('Place has not been defined yet');

	// Other rewrites
	if ($CONFIG['rewrite_zero_to_free'] && $session_price==0) {$session_price=_('Free as in free beer');}
	else if ($session_price=='-1') {$session_price=$CONFIG['rewrite_minus_one_to'];}
	else $session_price=$session_price.$CONFIG['currency_symbol'];

	// Query attendees (confirmed and unconfirmed)
	$sql_attendees = 'SELECT * from attendees WHERE session_id=\''.$row['session_id'].'\' LIMIT 100';
	//echo $sql_attendees;
	$res_attendees=$my->query_assoc($sql_attendees);
	$nb_attendees_confirmed=0;
	$nb_attendees_canceled=0;
	$nb_attendees_other=0;
	while ($r=$res_attendees->fetch_assoc()) {
		//echo $confirmed;
		//if (in_array($r['state'],$confirmed))$nb_attendees_confirmed++;
		if ($r['state']==$CONFIG['state_confirmed'])$nb_attendees_confirmed++;
		else if ($r['state']==$CONFIG['state_canceled']) $nb_attendees_canceled++;
		else $nb_attendees_other++;
		}
	($nb_attendees_confirmed==0)?$txt_attendees = _('No confirmed attendee yet'):$txt_attendees=_('Attendees');
	$txt_attendees.='<br />('.$nb_attendees_other.' '._('waiting for confirmation').' - '.$nb_attendees_canceled.' '._('canceled').')';
	$free_places = $row['session_max_attendee'] - $nb_attendees_confirmed;

	?>

	<?php ($_GET['event_id']==$row['session_id'] && $_GET['event_id']!=NULL)?$full=' full':$full=''; ?>
	<div class="session<?php echo $full;?>">
	<!-- Define locale -->

	<?php
	$dt = new DateTime;
	// Pick up date format in config
	($CONFIG['sessiondateformat'])?$session_date_format=$CONFIG['sessiondateformat']:$session_date_format=NULL;
	($CONFIG['sessiontimeformat'])?$session_time_format=$CONFIG['sessiontimeformat']:$session_time_format=NULL;
	$date_formatter = new IntlDateFormatter($locale, IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, NULL, NULL, $session_date_format);
	$time_formatter = new IntlDateFormatter($locale, IntlDateFormatter::NONE, IntlDateFormatter::MEDIUM, NULL, NULL,  $session_time_format);
	//$formatter->setPattern('E d.M.yyyy'); // to specify the pattern
	?>

	<!--// Display session desc -->
		<div class="session-title-container">
			<span class="session-title-wrapper"><a href="?event_id=<?php echo $row['session_id']; ?>"><?php echo $session_title ?></a></span>
		</div>

		<div class="session-image-container" style="background-image:url('<?php echo $session_image;?>');height:20em;  background-repeat: no-repeat;background-size: cover;background-position: center;">
			<!--<a href="?event_id=<?php echo $row['session_id']; ?>"><img src="<?php echo $session_image; ?>" class="session_image" /></a>-->
			<div class="datetime"> 
			<?php
				echo $date_formatter->format(strtotime($row['start']));
				echo '<br />';
				echo ' '._('from').' ';
				echo format_time($locale,$time_formatter->format(strtotime($row['start'])));
				if ($row['end'] != NULL) {
					echo " "._('to')." ";
					echo format_time($locale,$time_formatter->format(strtotime($row['end'])));
					}
			?>
			</div>
		</div>

			<?php 
			echo '<div class="template-count-attendees">';
			if ($row['session_canceled']=='0')
				echo $free_places.' '._('places').' '._('free').' ('.$row['session_max_attendee'].' max)'; 
			else { echo _('Session canceled');}
			echo '</div>';
			?>

		<?php 
		// **********************************
		// FULL EVENT PAGE ONLY
		// **********************************
		if ($_GET['event_id']==$row['session_id'] && $_GET['event_id']!=NULL) {  ?>
		<div class="session-data-container">
		<div class="session-desc-meta-inf-content">
		<?php $ex_start=explode(' ',$row['start']);$start_date=$ex_start[0];$start_time=$ex_start[1]; ?>
		<?php $ex_end=explode(' ',$row['end']);$end_date=$ex_end[0];$end_time=$ex_end[1]; ?>
		<div class="atcb" style="display:none;clear:both;">
			  {
			"label":"<?php echo _('Add to calendar'); ?>",
			"name":"<?php echo $session_title; ?>",
			"startDate":"<?php echo $start_date; ?>",
			"endDate":"<?php echo $end_date; ?>",
			"startTime":"<?php echo substr($start_time,0,5); ?>",
			"endTime":"<?php echo substr($end_time,0,5); ?>",
			"options":[
			<?php 
				$i=0;
				foreach ($CONFIG['addtocalendarbutton'] as $atcb) {
					if ($i>0) echo ",";
					echo "\"".$atcb."\"";
					$i++;
					}
			?>
			],
			"timeZone":"<?php echo $CONFIG['timezone'];?>",
			"iCalFileName":"Attendee-event"
			  }
			</div>

			<div class="session-desc-meta-inf-title">
				<?php echo _('Description')?>
			</div>
			<div class="session-desc-meta-inf-content">
				<?php echo nl2br($session_comment); ?>
			</div>
			<?php 
			if ($session_animator_last_name && $session_animator_first_name) { ?>
				<div class="session-desc-meta-inf-title">
						<?php echo _('Animator')." : "; ?>
						<?php echo $session_animator_first_name." ".$session_animator_last_name[0]."."; ?>
				</div>
				<div class="session-desc-meta-inf-content">
					<?php echo nl2br($session_animator_comment);?>
				</div>
				<?php } ?>
			<div class="session-desc-meta-inf-title">
				<?php echo _('Price'); ?> : <?php echo $session_price ?>
			</div>
			<div class="session-desc-meta-inf-title">
				<?php echo _('Location'); ?>
			</div>
			<div class="session-desc-meta-inf-content">
				<?php echo $session_place ?>
			</div>

			<div class="session-desc-meta-inf-title">
				<?php echo _('Practical info'); ?>
			</div>
			<div class="session-desc-meta-inf-content">
				<?php echo $CONFIG['practical-info']; ?>
			</div>

			<?php

			if ($row['session_animator_username']==NULL && isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')) 
				{ ?>
				<form name="take-animation" class="sessionlist_button" id="take-animation-<?php echo $row['session_id']; ?>" action="?<?php echo $event_id_action; ?>" method="POST">
					<input class="sessionlist_button_hidden_input" name="take-animation-session_id" id="take-animation-session_id-<?php echo $row['session_id']; ?>" type="text" value="<?php echo $row['session_id']; ?>">
					<input class="sessionlist-button take" id="take-animation-button" name="take-animation-button" value="<?php echo _('Take animation'); ?>" type="submit" />
				</form>
			<?php }
			elseif ($row['session_animator_username']!=NULL && $row['session_animator_username']==$_SESSION['username'] && isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')) 
				{ ?>
				<form name="release-animation" class="sessionlist_button" id="release-animation-<?php echo $row['session_id']; ?>" action="?<?php echo $event_id_action; ?>" method="POST">
					<input class="sessionlist_button_hidden_input" name="release-animation-session_id" id="release-animation-session_id<?php echo $row['session_id']; ?>" type="text" value="<?php echo $row['session_id']; ?>">
					<input class="sessionlist-button release" id="release-animation-button" name="release-animation-button" value="<?php echo _('Release animation'); ?>" type="submit" />
				</form>
			<?php } ?>

			<?php if ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR' ) { ?>
				<form name="schedule-modify" class="sessionlist_button" id="schedule-modify-<?php echo $row['session_id']; ?>" action="?page=schedule&<?php echo $event_id_action; ?>" method="POST">
					<input class="sessionlist-button modify" id="schedule-modify-button" name="schedule-modify-button" value="<?php echo _('Modify event'); ?>" type="submit" />
				</form>	
				<?php if ($row['session_canceled']=='0') { ?>
					<form name="cancel-animation" class="sessionlist_button" id="cancel-animation-<?php echo $row['session_id']; ?>" action="?<?php echo $event_id_action; ?>" method="POST">
					<input class="sessionlist_button_hidden_input" name="cancel-animation-session_id" id="cancel-animation-session_id-<?php echo $row['session_id']; ?>" type="text" value="<?php echo $row['session_id']; ?>">
					<input class="sessionlist-button cancel" id="cancel-animation-button" name="cancel-animation-button" value="<?php echo _('Cancel event'); ?>" type="submit" />
					</form>
				<?php } else { ?>
					<form name="restore-animation" class="sessionlist_button" id="restore-animation-<?php echo $row['session_id']; ?>" action="?<?php echo $event_id_action; ?>" method="POST">
					<input class="sessionlist_button_hidden_input" name="restore-animation-session_id" id="restore-animation-session_id-<?php echo $row['session_id']; ?>" type="text" value="<?php echo $row['session_id']; ?>">
					<input id="restore-animation-button" class="sessionlist-button restore" name="restore-animation-button" value="<?php echo _('Restore animation'); ?>" type="submit" />
					</form>
					
				<?php } ?>
			<?php } ?><!-- Restricted -->



			</div>
			<div class="session-aside-container">

			<div class="session-desc-meta-inf-title"><?php echo _('Register'); ?></div>

				<?php
				if ($row['session_canceled']!='0') {
					echo _('Session canceled');
					}
				else if ($free_places > 0) {
					if ($_POST['email']) $registeremail = $_POST['email'];	
					else if ($_SESSION['username']) $registeremail = $userinfo['mail'];
					else {$registeremail='';$placeholderemail=$CONFIG['emailplaceholder'];}
					echo '<form id="register-'.$row['session_id'].'" session_id="'.$row['session_id'].'" class="register" method="POST" action="?event_id='.$row['session_id'].'&register='.$row['session_id'].$event.'">'; ?>
					&#9993; <?php echo _('Enter your email below to register'); ?><br />
					<input name="email" id="input-email-<?php echo $row['session_id'] ?>" session_id="<?php echo $row['session_id']; ?>" class="email" value="<?php echo $registeremail ?>" required="" autocomplete="off" placeholder="<?php echo $placeholderemail; ?>" />
					<?php if (! $_SESSION['username']) { ?>				
						<div class="sessionlist-captcha">
							<div class="captcha-label"><?php echo _('Please enter the result of the calculation below in the box'); ?></div>
							<img src="<?php echo $_SESSION['captcha_image_src']; ?>" class="sessionlist-captcha-img" />
							<input placeholder="<?php echo _('Result'); ?>" type="text" size="4" name="input-captcha-<?php echo $row['session_id'] ?>" id="input-captcha-<?php echo $row['session_id'] ?>" session_id="<?php echo $row['session_id']; ?>" class="sessionlist-captcha-input" />
						</div>
					<?php } ?>
					<input type="submit" class="submit-email" value="<?php echo _('Register'); ?>" />
				</form>
				<?php } 
				else if ($free_places == 0) {
						echo _('Session is full');
					}
				?>


			<div class="session-desc-meta-inf-title"><?php echo _('Already registered'); ?></div>

			<div class="count-attendees"><?php echo $txt_attendees; ?></div>

			<!-- // Display Attendees -->
			<?php $res_attendees->data_seek(0); ?>
			<?php while ($row_attendees = $res_attendees->fetch_assoc()) {
				$f_submit = NULL;

				// Create form button to check validity :
				$f_check = '<form class="" enctype="multipart/form-data" name="membercheck" action="?page=newmember" method="POST" style="display:inline;">
				<button type="submit" class="" name="member-email" value="'.$row_attendees['email'].'" title="'._('Check').'"/>&#10004;</button>
				</form>';

				// For Admins
				if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER')) {
					$f_form = '<form action="?newstate=newstate&'.$event_id_action.'" method="post" id="newstate-'.$row_attendees['id'].'" name="newstate-'.$row_attendees['id'].'" class="newstate" >';
					$f_attendee_id = '<input id="attendee_id-'.$row_attendees['id'].'" name="attendee_id" attendee_id="'.$row_attendees['id'].'" class="attendee_id" value="'.$row_attendees['id'].'" />';
					$f_state = '
						<select name="newstate">';

					foreach ($CONFIG['states'] as $state) {
						($row_attendees['state'] == $state)?$sel='selected="selected"':$sel=NULL;
						$f_state .= '<option value="'.$state.'" '.$sel.'>'._(''.$state.'').'</option>';
						}
					$f_state .= '</select>';
					$f_submit .= '<input type="submit" name="save-attendee-state" value="&#128427;" title="'._('Save').'" />';
					$f_submit .= '<input type="submit" name="delete-attendee" value="&#10060;" title="'._('Delete').'" />';
					$f_endform = '</form>';
					}
				?>

				<?php 
				// Hiding attendee email behind stars
				if (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR')) {
					$email = '<span class="attendee-email">'.$row_attendees['email'].'</span>';
					}
				else {
					$alen=strlen($row_attendees['email']);
					$asplit=explode('@',$row_attendees['email']);
					$afirst=$asplit[0][0];
					$alast=substr($asplit[0],-1);
					$alen1=strlen($asplit[0]);
					$alen2=strlen($asplit[0]);
					$hide='';
					for ($i=0;$i<$alen1-2;$i++) {
						$hide.='*';
						}
					$email = $afirst.$hide.$alast.'@'.$asplit[1];
					}
				?>

				<?php
				// Display only if not in a temporary state
				if (	$row_attendees['state']==$CONFIG['state_confirmed']
					or (isset($_SESSION['profile']) && ($_SESSION['profile']=='ADMIN' OR $_SESSION['profile']=='MANAGER' OR $_SESSION['profile']=='ANIMATOR'))) {
					?>
					<span title="<?php echo _('state-'.$row_attendees['state']).'" class="'.$row_attendees['state'].'">'.$email; ?>
					<?php echo $f_form ?? ''; ?>
					<?php echo $f_state ?? ''; ?>
					<?php echo $f_attendee_id ?? ''; ?>
					<?php echo $f_submit ?? ''; ?>
					<?php echo $f_endform ?? ''; ?>
					<?php echo $f_check ?? ''; ?>
					</span>
					<br />
				<?php } ?>
				<?php
				} // End of "Attendee loop"
				?>

				<div class="buttons-wrapper">
					<div class="session-share-wrapper" data-toggle="collapse" data-target="#share-<?php echo $row['session_id']; ?>" title="<?php echo _('Share'); ?>">
						<i class="icono-share" class="session-share-button"></i>
					</div>
				</div>

				<!-- Share div (hidden) -->
				<div id="share-<?php echo $row['session_id']; ?>" class="collapse share-button">
					<?php echo _('Give this permalink to your friends').' :<br />'; ?>
					<input class="share-input" value="<?php echo $CONFIG['base_url'].'index.php?event_id='.$row['session_id']; ?>">
				</div>

			</div>
		<?php } 
		// **********************************
		// END OF FULL EVENT PAGE ONLY
		// **********************************
		else echo '<div class="fulleventlink" title="'._('Click for more information.').'"><a class="moreinfobutton" href="?event_id='.$row['session_id'].'">+</a></div>'; ?>
	</div><!-- Session data -->
	<?php 
	}  // Each session loop
	?>
	</div><!-- / All session container -->
	<?php
	} // If $res
	?>

